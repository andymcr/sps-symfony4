<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220925151618 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE authentications CHANGE enabled enabled TINYINT(1) NOT NULL, CHANGE locked locked TINYINT(1) NOT NULL, CHANGE expired expired TINYINT(1) NOT NULL, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', CHANGE last_login last_login DATETIME NOT NULL, CHANGE password_changed_at password_changed_at DATETIME DEFAULT NULL, CHANGE credentials_expired credentials_expired TINYINT(1) DEFAULT NULL, CHANGE created created DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE autologins (id INT AUTO_INCREMENT NOT NULL, `key` VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, authentication_id INT DEFAULT NULL, user_agent VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, last_ip_address VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, last_login DATETIME NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE loginattempts (id INT AUTO_INCREMENT NOT NULL, login_id VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, ip_address VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, time DATETIME NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE oauth_clients (id VARCHAR(32) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, secret VARCHAR(32) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, name VARCHAR(32) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_forum (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, slug VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_post (id INT AUTO_INCREMENT NOT NULL, thread_id INT NOT NULL, content VARCHAR(4000) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, published SMALLINT DEFAULT 0 NOT NULL, user_id INT NOT NULL, cdate DATETIME NOT NULL, ip VARCHAR(16) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, moderateReason VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, voteUp INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_post_report (id INT AUTO_INCREMENT NOT NULL, post_id INT NOT NULL, user_id INT NOT NULL, cdate DATETIME NOT NULL, processed SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_post_vote (id INT AUTO_INCREMENT NOT NULL, post_id INT NOT NULL, thread_id INT NOT NULL, voteType INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_setting (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, value VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, type VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_subforum (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, forum_id INT NOT NULL, name VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, nb_thread INT DEFAULT NULL, slug VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, nb_post INT DEFAULT NULL, last_reply_date DATETIME NOT NULL, lastReplyUser INT DEFAULT NULL, allowed_roles VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_thread (id INT AUTO_INCREMENT NOT NULL, subforum_id INT NOT NULL, author_id INT NOT NULL, cdate DATETIME NOT NULL, nbReplies INT NOT NULL, lastReplyDate DATETIME NOT NULL, lastReplyUser INT DEFAULT NULL, resolved TINYINT(1) DEFAULT NULL, locked TINYINT(1) DEFAULT NULL, label VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, sublabel VARCHAR(100) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, slug VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, pin TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE authentications ADD person_id INT DEFAULT NULL, ADD username_canonical VARCHAR(255) NOT NULL, ADD salt VARCHAR(255) NOT NULL, ADD email_canonical VARCHAR(255) NOT NULL, ADD last_ip VARCHAR(255) DEFAULT \'0.0.0.0\', DROP email_verified, CHANGE enabled enabled TINYINT(1) DEFAULT 1, CHANGE locked locked TINYINT(1) DEFAULT 0, CHANGE expired expired TINYINT(1) DEFAULT 0, CHANGE password_changed_at password_changed_at DATETIME DEFAULT CURRENT_TIMESTAMP, CHANGE credentials_expired credentials_expired TINYINT(1) DEFAULT 0, CHANGE roles roles VARCHAR(255) DEFAULT \'a:0:{}\', CHANGE last_login last_login DATETIME DEFAULT \'0000-00-00 00:00:00\', CHANGE created created DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE author_points DROP FOREIGN KEY FK_C279B804F675F31B');
        $this->addSql('ALTER TABLE author_points DROP FOREIGN KEY FK_C279B804CC05B01B');
        $this->addSql('DROP INDEX UNIQ_C279B804F675F31B ON author_points');
        $this->addSql('ALTER TABLE author_points CHANGE points points INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE competition_awards DROP FOREIGN KEY FK_4AA9232DBA364942');
        $this->addSql('ALTER TABLE competition_awards DROP FOREIGN KEY FK_4AA9232DE38E6A94');
        $this->addSql('DROP INDEX IDX_4AA9232DBA364942 ON competition_awards');
        $this->addSql('DROP INDEX IDX_4AA9232DE38E6A94 ON competition_awards');
        $this->addSql('ALTER TABLE competition_sections DROP FOREIGN KEY FK_1AE778B7C97EF49F');
        $this->addSql('DROP INDEX IDX_1AE778B7C97EF49F ON competition_sections');
        $this->addSql('ALTER TABLE competition_sections CHANGE `order` `order` INT DEFAULT 0 NOT NULL, CHANGE maximum_entries maximum_entries INT DEFAULT 0');
        $this->addSql('ALTER TABLE competitioncategory DROP FOREIGN KEY FK_6DB13812727ACA70');
        $this->addSql('DROP INDEX IDX_6DB13812727ACA70 ON competitioncategory');
        $this->addSql('ALTER TABLE competitioncategory CHANGE id id INT NOT NULL, CHANGE short_name short_name VARCHAR(45) NOT NULL, CHANGE full_name full_name VARCHAR(120) DEFAULT NULL, CHANGE rank rank INT DEFAULT 0');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463DA2A63DB2');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D12469DE2');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D3DA5256D');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D71F7E88B');
        $this->addSql('DROP INDEX IDX_A7DD463DA2A63DB2 ON competitions');
        $this->addSql('DROP INDEX IDX_A7DD463D12469DE2 ON competitions');
        $this->addSql('DROP INDEX IDX_A7DD463D3DA5256D ON competitions');
        $this->addSql('DROP INDEX UNIQ_A7DD463D71F7E88B ON competitions');
        $this->addSql('ALTER TABLE competitions ADD complete TINYINT(1) DEFAULT 0 NOT NULL, CHANGE public_authors public_authors TINYINT(1) DEFAULT 0 NOT NULL, CHANGE public_results public_results TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE competitions_people DROP FOREIGN KEY FK_60493A12FCCFC170');
        $this->addSql('ALTER TABLE competitions_people DROP FOREIGN KEY FK_60493A1210A43C19');
        $this->addSql('DROP INDEX IDX_60493A12FCCFC170 ON competitions_people');
        $this->addSql('DROP INDEX IDX_60493A1210A43C19 ON competitions_people');
        $this->addSql('DROP INDEX `primary` ON competitions_people');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation DROP FOREIGN KEY FK_14FA7C28963579DB');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation DROP FOREIGN KEY FK_14FA7C28C9363781');
        $this->addSql('DROP INDEX IDX_14FA7C28963579DB ON competitionsection_pointsaccumulation');
        $this->addSql('DROP INDEX IDX_14FA7C28C9363781 ON competitionsection_pointsaccumulation');
        $this->addSql('DROP INDEX `primary` ON competitionsection_pointsaccumulation');
        $this->addSql('ALTER TABLE distinctions_people DROP FOREIGN KEY FK_6D60098556EEE8EA');
        $this->addSql('ALTER TABLE distinctions_people DROP FOREIGN KEY FK_6D600985952F297F');
        $this->addSql('DROP INDEX IDX_6D60098556EEE8EA ON distinctions_people');
        $this->addSql('DROP INDEX IDX_6D600985952F297F ON distinctions_people');
        $this->addSql('DROP INDEX `primary` ON distinctions_people');
        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A9A82C896');
        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A40A73EBA');
        $this->addSql('DROP INDEX IDX_5387574A9A82C896 ON events');
        $this->addSql('DROP INDEX IDX_5387574A40A73EBA ON events');
        $this->addSql('ALTER TABLE events CHANGE draft draft TINYINT(1) DEFAULT 0 NOT NULL, CHANGE cancelled cancelled TINYINT(1) DEFAULT 0 NOT NULL, CHANGE postponed postponed TINYINT(1) DEFAULT 0 NOT NULL, CHANGE publicise publicise TINYINT(1) DEFAULT 1 NOT NULL, CHANGE start start DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6AF675F31B');
        $this->addSql('DROP INDEX IDX_E01FBE6AF675F31B ON images');
        $this->addSql('ALTER TABLE images ADD url VARCHAR(255) DEFAULT NULL, CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE imagevariants DROP FOREIGN KEY FK_52C0E5403DA5256D');
        $this->addSql('DROP INDEX IDX_52C0E5403DA5256D ON imagevariants');
        $this->addSql('ALTER TABLE imagevariants ADD file_original_name VARCHAR(255) DEFAULT NULL, ADD file_mime_type VARCHAR(255) DEFAULT NULL, ADD file_size INT DEFAULT NULL, ADD file_dimensions VARCHAR(255) DEFAULT NULL, CHANGE image_id image_id INT DEFAULT NULL, CHANGE description description VARCHAR(64) NOT NULL, CHANGE club_showcase club_showcase INT DEFAULT 0, CHANGE personal_showcase personal_showcase INT DEFAULT 0, CHANGE photo photo TINYINT(1) DEFAULT 0');
        $this->addSql('ALTER TABLE organisations CHANGE short_title short_title VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE people DROP FOREIGN KEY FK_28166A26882BF0EB');
        $this->addSql('ALTER TABLE people DROP FOREIGN KEY FK_28166A26382C368F');
        $this->addSql('DROP INDEX UNIQ_28166A26882BF0EB ON people');
        $this->addSql('DROP INDEX IDX_28166A26382C368F ON people');
        $this->addSql('ALTER TABLE people ADD avatar_url VARCHAR(255) DEFAULT NULL, ADD profile_picture VARCHAR(255) DEFAULT NULL, ADD profile_picture_original_name VARCHAR(255) DEFAULT NULL, ADD profile_picture_mime_type VARCHAR(255) DEFAULT NULL, ADD profile_picture_size INT DEFAULT NULL, ADD profile_picture_dimensions VARCHAR(255) DEFAULT NULL, ADD nb_post INT DEFAULT 0, ADD banned TINYINT(1) DEFAULT 0, CHANGE profile_picture_name profile_picture_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE people_talks DROP FOREIGN KEY FK_8C41FB9A369BBB55');
        $this->addSql('ALTER TABLE people_talks DROP FOREIGN KEY FK_8C41FB9AB11F9DC0');
        $this->addSql('DROP INDEX IDX_8C41FB9A369BBB55 ON people_talks');
        $this->addSql('DROP INDEX IDX_8C41FB9AB11F9DC0 ON people_talks');
        $this->addSql('DROP INDEX `primary` ON people_talks');
        $this->addSql('ALTER TABLE pointsaccumulations DROP FOREIGN KEY FK_A949154C97EF49F');
        $this->addSql('DROP INDEX IDX_A949154C97EF49F ON pointsaccumulations');
        $this->addSql('ALTER TABLE pointsaccumulations CHANGE name name VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE private private TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE resetpasswordrequests ADD authentication_id INT NOT NULL, CHANGE selector selector VARCHAR(20) DEFAULT NULL, CHANGE hashed_token hashed_token VARCHAR(100) DEFAULT NULL, CHANGE requested_at requested_at DATETIME DEFAULT NULL, CHANGE expires_at expires_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE sectionawards CHANGE title title VARCHAR(30) NOT NULL, CHANGE rank rank INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A6603D5282CF');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A66057B7BE99');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A6604935AC23');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A660B65A60EA');
        $this->addSql('DROP INDEX UNIQ_9E83A6603D5282CF ON sectionentries');
        $this->addSql('DROP INDEX IDX_9E83A66057B7BE99 ON sectionentries');
        $this->addSql('DROP INDEX IDX_9E83A6604935AC23 ON sectionentries');
        $this->addSql('DROP INDEX IDX_9E83A660B65A60EA ON sectionentries');
        $this->addSql('ALTER TABLE sectionentries CHANGE image_variant_id image_variant_id INT DEFAULT NULL, CHANGE title title VARCHAR(255) NOT NULL, CHANGE score score NUMERIC(4, 1) DEFAULT NULL, CHANGE removed removed TINYINT(1) DEFAULT 0 NOT NULL, CHANGE disqualified disqualified TINYINT(1) DEFAULT 0 NOT NULL, CHANGE tagged tagged TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE statictext CHANGE name name VARCHAR(50) NOT NULL, CHANGE content content VARCHAR(4000) NOT NULL');
        $this->addSql('ALTER TABLE talks DROP FOREIGN KEY FK_472281DA71F7E88B');
        $this->addSql('DROP INDEX UNIQ_472281DA71F7E88B ON talks');
        $this->addSql('ALTER TABLE talks CHANGE synopsis synopsis VARCHAR(4000) CHARACTER SET cp1250 DEFAULT NULL COLLATE `cp1250_general_ci`');
        $this->addSql('ALTER TABLE venues ADD location_latitude DOUBLE PRECISION DEFAULT \'0\' NOT NULL, ADD location_longitude DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
    }
}
