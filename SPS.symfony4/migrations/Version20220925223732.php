<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220925223732 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES authentications (id)');
        $this->addSql('DROP TABLE resetpasswordrequests');
        $this->addSql('DROP TABLE workingforum_forum');
        $this->addSql('DROP TABLE workingforum_post');
        $this->addSql('DROP TABLE workingforum_post_report');
        $this->addSql('DROP TABLE workingforum_post_vote');
        $this->addSql('DROP TABLE workingforum_setting');
        $this->addSql('DROP TABLE workingforum_subforum');
        $this->addSql('DROP TABLE workingforum_thread');
        $this->addSql('ALTER TABLE author_points DROP INDEX FK_C279B804F675F31B, ADD UNIQUE INDEX UNIQ_C279B804F675F31B (author_id)');
        $this->addSql('ALTER TABLE competitioncategory CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE short_name short_name VARCHAR(255) NOT NULL, CHANGE full_name full_name VARCHAR(255) NOT NULL, CHANGE rank rank INT DEFAULT NULL');
        $this->addSql('ALTER TABLE competitioncategory ADD CONSTRAINT FK_6DB13812727ACA70 FOREIGN KEY (parent_id) REFERENCES competitioncategory (id)');
        $this->addSql('CREATE INDEX IDX_6DB13812727ACA70 ON competitioncategory (parent_id)');
        $this->addSql('ALTER TABLE competitions ADD CONSTRAINT FK_A7DD463DA2A63DB2 FOREIGN KEY (secretary_id) REFERENCES people (id)');
        $this->addSql('ALTER TABLE competitions ADD CONSTRAINT FK_A7DD463D12469DE2 FOREIGN KEY (category_id) REFERENCES competitioncategory (id)');
        $this->addSql('ALTER TABLE competitions ADD CONSTRAINT FK_A7DD463D3DA5256D FOREIGN KEY (image_id) REFERENCES imagevariants (id)');
        $this->addSql('ALTER TABLE competitions ADD CONSTRAINT FK_A7DD463D71F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('CREATE INDEX IDX_A7DD463DA2A63DB2 ON competitions (secretary_id)');
        $this->addSql('CREATE INDEX IDX_A7DD463D12469DE2 ON competitions (category_id)');
        $this->addSql('CREATE INDEX IDX_A7DD463D3DA5256D ON competitions (image_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A7DD463D71F7E88B ON competitions (event_id)');
        $this->addSql('ALTER TABLE competitions_people ADD PRIMARY KEY (has_judged_id, judges_id)');
        $this->addSql('ALTER TABLE competitions_people ADD CONSTRAINT FK_60493A12FCCFC170 FOREIGN KEY (has_judged_id) REFERENCES competitions (id)');
        $this->addSql('ALTER TABLE competitions_people ADD CONSTRAINT FK_60493A1210A43C19 FOREIGN KEY (judges_id) REFERENCES people (id)');
        $this->addSql('CREATE INDEX IDX_60493A12FCCFC170 ON competitions_people (has_judged_id)');
        $this->addSql('CREATE INDEX IDX_60493A1210A43C19 ON competitions_people (judges_id)');
        $this->addSql('ALTER TABLE events RENAME INDEX fk_5387574a9a82c896 TO IDX_5387574A9A82C896');
        $this->addSql('ALTER TABLE events RENAME INDEX fk_5387574a40a73eba TO IDX_5387574A40A73EBA');
        $this->addSql('ALTER TABLE images RENAME INDEX fk_e01fbe6af675f31b TO IDX_E01FBE6AF675F31B');
        $this->addSql('ALTER TABLE imagevariants CHANGE file_name file_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE imagevariants ADD CONSTRAINT FK_52C0E5403DA5256D FOREIGN KEY (image_id) REFERENCES images (id)');
        $this->addSql('CREATE INDEX IDX_52C0E5403DA5256D ON imagevariants (image_id)');
        $this->addSql('ALTER TABLE people DROP avatar_url, DROP profile_picture, DROP profile_picture_original_name, DROP profile_picture_mime_type, DROP profile_picture_size, DROP profile_picture_dimensions, DROP nb_post, DROP banned, CHANGE profile_picture_name profile_picture_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE people ADD CONSTRAINT FK_28166A26882BF0EB FOREIGN KEY (showcase_image_id) REFERENCES imagevariants (id)');
        $this->addSql('ALTER TABLE people ADD CONSTRAINT FK_28166A26382C368F FOREIGN KEY (member_of_id) REFERENCES organisations (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_28166A26882BF0EB ON people (showcase_image_id)');
        $this->addSql('CREATE INDEX IDX_28166A26382C368F ON people (member_of_id)');
        $this->addSql('ALTER TABLE distinctions_people ADD PRIMARY KEY (inv_distinctions_id, distinctions_id)');
        $this->addSql('ALTER TABLE distinctions_people ADD CONSTRAINT FK_6D60098556EEE8EA FOREIGN KEY (inv_distinctions_id) REFERENCES people (id)');
        $this->addSql('ALTER TABLE distinctions_people ADD CONSTRAINT FK_6D600985952F297F FOREIGN KEY (distinctions_id) REFERENCES distinctions (id)');
        $this->addSql('CREATE INDEX IDX_6D60098556EEE8EA ON distinctions_people (inv_distinctions_id)');
        $this->addSql('CREATE INDEX IDX_6D600985952F297F ON distinctions_people (distinctions_id)');
        $this->addSql('ALTER TABLE pointsaccumulations CHANGE name name VARCHAR(255) NOT NULL, CHANGE private private TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE pointsaccumulations ADD CONSTRAINT FK_A949154C97EF49F FOREIGN KEY (part_of_id) REFERENCES pointssets (id)');
        $this->addSql('CREATE INDEX IDX_A949154C97EF49F ON pointsaccumulations (part_of_id)');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation ADD PRIMARY KEY (contributes_to_id, from_sections_id)');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation ADD CONSTRAINT FK_14FA7C28963579DB FOREIGN KEY (contributes_to_id) REFERENCES pointsaccumulations (id)');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation ADD CONSTRAINT FK_14FA7C28C9363781 FOREIGN KEY (from_sections_id) REFERENCES competition_sections (id)');
        $this->addSql('CREATE INDEX IDX_14FA7C28963579DB ON competitionsection_pointsaccumulation (contributes_to_id)');
        $this->addSql('CREATE INDEX IDX_14FA7C28C9363781 ON competitionsection_pointsaccumulation (from_sections_id)');
        $this->addSql('ALTER TABLE sectionawards CHANGE title title VARCHAR(255) DEFAULT NULL, CHANGE rank rank INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sectionentries CHANGE image_variant_id image_variant_id INT NOT NULL, CHANGE title title VARCHAR(255) DEFAULT NULL, CHANGE score score DOUBLE PRECISION DEFAULT NULL, CHANGE removed removed TINYINT(1) DEFAULT NULL, CHANGE disqualified disqualified TINYINT(1) DEFAULT NULL, CHANGE tagged tagged TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE sectionentries ADD CONSTRAINT FK_9E83A6603D5282CF FOREIGN KEY (award_id) REFERENCES sectionawards (id)');
        $this->addSql('ALTER TABLE sectionentries ADD CONSTRAINT FK_9E83A66057B7BE99 FOREIGN KEY (image_variant_id) REFERENCES imagevariants (id)');
        $this->addSql('ALTER TABLE sectionentries ADD CONSTRAINT FK_9E83A6604935AC23 FOREIGN KEY (inv_entries_id) REFERENCES author_points (id)');
        $this->addSql('ALTER TABLE sectionentries ADD CONSTRAINT FK_9E83A660B65A60EA FOREIGN KEY (entered_in_id) REFERENCES competition_sections (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9E83A6603D5282CF ON sectionentries (award_id)');
        $this->addSql('CREATE INDEX IDX_9E83A66057B7BE99 ON sectionentries (image_variant_id)');
        $this->addSql('CREATE INDEX IDX_9E83A6604935AC23 ON sectionentries (inv_entries_id)');
        $this->addSql('CREATE INDEX IDX_9E83A660B65A60EA ON sectionentries (entered_in_id)');
        $this->addSql('ALTER TABLE statictext CHANGE name name VARCHAR(255) NOT NULL, CHANGE content content VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE talks CHANGE synopsis synopsis VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE talks ADD CONSTRAINT FK_472281DA71F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_472281DA71F7E88B ON talks (event_id)');
        $this->addSql('ALTER TABLE people_talks ADD PRIMARY KEY (has_presented_id, presenters_id)');
        $this->addSql('ALTER TABLE people_talks ADD CONSTRAINT FK_8C41FB9A369BBB55 FOREIGN KEY (has_presented_id) REFERENCES talks (id)');
        $this->addSql('ALTER TABLE people_talks ADD CONSTRAINT FK_8C41FB9AB11F9DC0 FOREIGN KEY (presenters_id) REFERENCES people (id)');
        $this->addSql('CREATE INDEX IDX_8C41FB9A369BBB55 ON people_talks (has_presented_id)');
        $this->addSql('CREATE INDEX IDX_8C41FB9AB11F9DC0 ON people_talks (presenters_id)');
        $this->addSql('ALTER TABLE venues DROP location_latitude, DROP location_longitude');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE resetpasswordrequests (id INT AUTO_INCREMENT NOT NULL, selector VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, hashed_token VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, requested_at DATETIME DEFAULT NULL, expires_at DATETIME DEFAULT NULL, authentication_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_0900_ai_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_forum (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, slug VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_post (id INT AUTO_INCREMENT NOT NULL, thread_id INT NOT NULL, content VARCHAR(4000) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, published SMALLINT DEFAULT 0 NOT NULL, user_id INT NOT NULL, cdate DATETIME NOT NULL, ip VARCHAR(16) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, moderateReason VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, voteUp INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_post_report (id INT AUTO_INCREMENT NOT NULL, post_id INT NOT NULL, user_id INT NOT NULL, cdate DATETIME NOT NULL, processed SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_post_vote (id INT AUTO_INCREMENT NOT NULL, post_id INT NOT NULL, thread_id INT NOT NULL, voteType INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_setting (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, value VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, type VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_subforum (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, forum_id INT NOT NULL, name VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, nb_thread INT DEFAULT NULL, slug VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, nb_post INT DEFAULT NULL, last_reply_date DATETIME NOT NULL, lastReplyUser INT DEFAULT NULL, allowed_roles VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE workingforum_thread (id INT AUTO_INCREMENT NOT NULL, subforum_id INT NOT NULL, author_id INT NOT NULL, cdate DATETIME NOT NULL, nbReplies INT NOT NULL, lastReplyDate DATETIME NOT NULL, lastReplyUser INT DEFAULT NULL, resolved TINYINT(1) DEFAULT NULL, locked TINYINT(1) DEFAULT NULL, label VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, sublabel VARCHAR(100) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, slug VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, pin TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET latin1 COLLATE `latin1_swedish_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('ALTER TABLE author_points DROP INDEX UNIQ_C279B804F675F31B, ADD INDEX FK_C279B804F675F31B (author_id)');
        $this->addSql('ALTER TABLE competitioncategory DROP FOREIGN KEY FK_6DB13812727ACA70');
        $this->addSql('DROP INDEX IDX_6DB13812727ACA70 ON competitioncategory');
        $this->addSql('ALTER TABLE competitioncategory CHANGE id id INT NOT NULL, CHANGE short_name short_name VARCHAR(45) NOT NULL, CHANGE full_name full_name VARCHAR(120) DEFAULT NULL, CHANGE rank rank INT DEFAULT 0');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463DA2A63DB2');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D12469DE2');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D3DA5256D');
        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D71F7E88B');
        $this->addSql('DROP INDEX IDX_A7DD463DA2A63DB2 ON competitions');
        $this->addSql('DROP INDEX IDX_A7DD463D12469DE2 ON competitions');
        $this->addSql('DROP INDEX IDX_A7DD463D3DA5256D ON competitions');
        $this->addSql('DROP INDEX UNIQ_A7DD463D71F7E88B ON competitions');
        $this->addSql('ALTER TABLE competitions_people DROP FOREIGN KEY FK_60493A12FCCFC170');
        $this->addSql('ALTER TABLE competitions_people DROP FOREIGN KEY FK_60493A1210A43C19');
        $this->addSql('DROP INDEX IDX_60493A12FCCFC170 ON competitions_people');
        $this->addSql('DROP INDEX IDX_60493A1210A43C19 ON competitions_people');
        $this->addSql('DROP INDEX `primary` ON competitions_people');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation DROP FOREIGN KEY FK_14FA7C28963579DB');
        $this->addSql('ALTER TABLE competitionsection_pointsaccumulation DROP FOREIGN KEY FK_14FA7C28C9363781');
        $this->addSql('DROP INDEX IDX_14FA7C28963579DB ON competitionsection_pointsaccumulation');
        $this->addSql('DROP INDEX IDX_14FA7C28C9363781 ON competitionsection_pointsaccumulation');
        $this->addSql('DROP INDEX `primary` ON competitionsection_pointsaccumulation');
        $this->addSql('ALTER TABLE distinctions_people DROP FOREIGN KEY FK_6D60098556EEE8EA');
        $this->addSql('ALTER TABLE distinctions_people DROP FOREIGN KEY FK_6D600985952F297F');
        $this->addSql('DROP INDEX IDX_6D60098556EEE8EA ON distinctions_people');
        $this->addSql('DROP INDEX IDX_6D600985952F297F ON distinctions_people');
        $this->addSql('DROP INDEX `primary` ON distinctions_people');
        $this->addSql('ALTER TABLE events RENAME INDEX idx_5387574a40a73eba TO FK_5387574A40A73EBA');
        $this->addSql('ALTER TABLE events RENAME INDEX idx_5387574a9a82c896 TO FK_5387574A9A82C896');
        $this->addSql('ALTER TABLE images RENAME INDEX idx_e01fbe6af675f31b TO FK_E01FBE6AF675F31B');
        $this->addSql('ALTER TABLE imagevariants DROP FOREIGN KEY FK_52C0E5403DA5256D');
        $this->addSql('DROP INDEX IDX_52C0E5403DA5256D ON imagevariants');
        $this->addSql('ALTER TABLE imagevariants CHANGE file_name file_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE people DROP FOREIGN KEY FK_28166A26882BF0EB');
        $this->addSql('ALTER TABLE people DROP FOREIGN KEY FK_28166A26382C368F');
        $this->addSql('DROP INDEX UNIQ_28166A26882BF0EB ON people');
        $this->addSql('DROP INDEX IDX_28166A26382C368F ON people');
        $this->addSql('ALTER TABLE people ADD avatar_url VARCHAR(255) DEFAULT NULL, ADD profile_picture VARCHAR(255) DEFAULT NULL, ADD profile_picture_original_name VARCHAR(255) DEFAULT NULL, ADD profile_picture_mime_type VARCHAR(255) DEFAULT NULL, ADD profile_picture_size INT DEFAULT NULL, ADD profile_picture_dimensions VARCHAR(255) DEFAULT NULL, ADD nb_post INT DEFAULT 0, ADD banned TINYINT(1) DEFAULT 0, CHANGE profile_picture_name profile_picture_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE people_talks DROP FOREIGN KEY FK_8C41FB9A369BBB55');
        $this->addSql('ALTER TABLE people_talks DROP FOREIGN KEY FK_8C41FB9AB11F9DC0');
        $this->addSql('DROP INDEX IDX_8C41FB9A369BBB55 ON people_talks');
        $this->addSql('DROP INDEX IDX_8C41FB9AB11F9DC0 ON people_talks');
        $this->addSql('DROP INDEX `primary` ON people_talks');
        $this->addSql('ALTER TABLE pointsaccumulations DROP FOREIGN KEY FK_A949154C97EF49F');
        $this->addSql('DROP INDEX IDX_A949154C97EF49F ON pointsaccumulations');
        $this->addSql('ALTER TABLE pointsaccumulations CHANGE name name VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE private private TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE sectionawards CHANGE title title VARCHAR(30) NOT NULL, CHANGE rank rank INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A6603D5282CF');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A66057B7BE99');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A6604935AC23');
        $this->addSql('ALTER TABLE sectionentries DROP FOREIGN KEY FK_9E83A660B65A60EA');
        $this->addSql('DROP INDEX UNIQ_9E83A6603D5282CF ON sectionentries');
        $this->addSql('DROP INDEX IDX_9E83A66057B7BE99 ON sectionentries');
        $this->addSql('DROP INDEX IDX_9E83A6604935AC23 ON sectionentries');
        $this->addSql('DROP INDEX IDX_9E83A660B65A60EA ON sectionentries');
        $this->addSql('ALTER TABLE sectionentries CHANGE image_variant_id image_variant_id INT DEFAULT NULL, CHANGE title title VARCHAR(255) NOT NULL, CHANGE score score NUMERIC(4, 1) DEFAULT NULL, CHANGE removed removed TINYINT(1) DEFAULT 0 NOT NULL, CHANGE disqualified disqualified TINYINT(1) DEFAULT 0 NOT NULL, CHANGE tagged tagged TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE statictext CHANGE name name VARCHAR(50) NOT NULL, CHANGE content content VARCHAR(4000) NOT NULL');
        $this->addSql('ALTER TABLE talks DROP FOREIGN KEY FK_472281DA71F7E88B');
        $this->addSql('DROP INDEX UNIQ_472281DA71F7E88B ON talks');
        $this->addSql('ALTER TABLE talks CHANGE synopsis synopsis VARCHAR(4000) CHARACTER SET cp1250 DEFAULT NULL COLLATE `cp1250_general_ci`');
        $this->addSql('ALTER TABLE venues ADD location_latitude DOUBLE PRECISION DEFAULT \'0\' NOT NULL, ADD location_longitude DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
    }
}
