<?php

namespace App\Authentication;

use App\Entity\Authentication;
use App\Repository\AuthenticationRepository;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3Validator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'login';

    /**
     * @var RouterInterface $router
     */
    private RouterInterface $router;
    
    /**
     * @var AuthenticationRepository $authenticationRepository
     */
    private AuthenticationRepository $authenticationRepository;
    
    /**
     * @var LoggerInterface $logger
     */
    private LoggerInterface $logger;

    public function __construct(RouterInterface $router, AuthenticationRepository $authenticationRepository, Recaptcha3Validator $recaptcha3Validator, LoggerInterface $securityLogger)
    {
        $this->router = $router;
        $this->authenticationRepository = $authenticationRepository;
        $this->logger = $securityLogger;
    }

    /**
     * @param Request $request
     * 
     * @return Passport
     */
    public function authenticate(Request $request): Passport
    {
        /** var string $email */
        $email = $request->request->get('email');
        $request->getSession()->set(Security::LAST_USERNAME, $email);
    
        $this->logger->info("Form login attempt for " . $email);
    
        return new Passport(
            /* @phpstan-ignore-next-line */
            new UserBadge($email),
            new PasswordCredentials((string) $request->request->get('plainPassword')),
            [
                /* @phpstan-ignore-next-line */
                new CsrfTokenBadge('authentication_login', $request->request->get('_token')),
                new RememberMeBadge()
            ]);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $firewallName
     *
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        /** @var Authentication $user */
        $user = $token->getUser();
        $user->setLastLogin(new \DateTime());
        $this->authenticationRepository->save($user, true);
    
        $this->logger->info("Form login by " . $user->getUserIdentifier());
    
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }
    
        return new RedirectResponse($this->router->generate('competitions'));
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getLoginUrl(Request $request): string
    {
        return $this->router->generate(self::LOGIN_ROUTE);
    }
}
