<?php

namespace App\Command;

use App\Repository\ImageVariantsRepository;
use App\Service\ImageVariantsService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\RuntimeException;


#[AsCommand(name: 'app:set-image-size', description: 'Record size of images.')]
class SetImageSizeCommand extends Command
{
    private ImageVariantsService $imageVariantService;

    public function __construct(ImageVariantsService $imageVariantService)
    {
        parent::__construct();
        
        $this->imageVariantService = $imageVariantService;
    }

    protected function configure(): void
    {
        $this->setHelp('Set the size of images without a recorded size.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->imageVariantService->setImageSizes();
        return Command::SUCCESS;
    }
}