<?php
namespace App\Controller\Web;

use App\Repository\StaticTextRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AboutUsController extends AbstractController
{
    /**
     * @var StaticTextRepository $staticTextRepository
     */
    private StaticTextRepository $staticTextRepository;

    public function __construct(StaticTextRepository $staticTextRepository)
    {
        $this->staticTextRepository = $staticTextRepository;
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/about', name: 'aboutUs')]
    public function aboutUs(): Response
    {
        $aboutUs = $this->staticTextRepository->findAboutUsText();
        if (is_null($aboutUs)) {
            throw $this->createNotFoundException('The StaticText does not exist');
        }
        return $this->render('about_us/about_us.html.twig', [
            'aboutUs' => $aboutUs
        ]);
    }

}
