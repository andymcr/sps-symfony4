<?php
namespace App\Controller\Web;

use App\Entity\Authentication;
use App\Entity\ResetPasswordRequest;
use App\Form\ForgottenPasswordRequest;
use App\Form\ForgottenPasswordReset;
use App\Form\AuthenticationLoginType;
use App\Form\ChangePasswordType;
use App\Repository\AuthenticationRepository;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3Validator;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;


class AuthenticationController extends AbstractController
{
    use ResetPasswordControllerTrait;

    /**
     * @var ResetPasswordHelperInterface
     */
    private ResetPasswordHelperInterface $resetPasswordHelper;
    
    /**
     * @var AuthenticationRepository $authenticationRepository
     */
    private AuthenticationRepository $authenticationRepository;
    
    /**
     * @var TranslatorInterface $translator
     */
    private TranslatorInterface $translator;
    
    /**
     * @var LoggerInterface $logger
     */
    private LoggerInterface $logger;

    public function __construct(AuthenticationRepository $authenticationRepository, ResetPasswordHelperInterface $resetPasswordHelper, TranslatorInterface $translator, LoggerInterface $securityLogger)
    {
        $this->authenticationRepository = $authenticationRepository;
        $this->resetPasswordHelper = $resetPasswordHelper;
        $this->translator = $translator;
        $this->logger = $securityLogger;
    }

	/**
	 **/
	#[Route('/login', name: 'login')]
	public function login(AuthenticationUtils $authenticationUtils): Response
	{
	    if ($this->getUser()) {
	        return $this->redirectToRoute("competitions_past");
	    }
	
	    $loginForm = $this->createForm(AuthenticationLoginType::class);
	
	    $parameters = [];
	    $parameters['login'] = $loginForm->createView();
	    $parameters['error'] = $authenticationUtils->getLastAuthenticationError();
	    return $this->render('authentication/login.html.twig', $parameters);
	}

	/**
	 */
	#[Route('/logout', name: 'logout')]
	public function logout(): Response
	{
	    throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
	}

	/**
	 **/
	#[Route('/forgotten-password', name: 'forgotten-password')]
	public function forgottenRequest(Request $request, MailerInterface $mailer): Response
	{
	    if ($this->getUser()) {
	        $this->logger->debug("Forgotten password process started by logged in user " . $this->getUser()->getUserIdentifier());
	        return $this->redirectToRoute("competitions_past");
	    }
	
	    $forgottenForm = $this->createForm(ForgottenPasswordRequest::class);
	    $forgottenForm->handleRequest($request);
	    if ($forgottenForm->isSubmitted() && $forgottenForm->isValid()) {
	        return $this->processSendingPasswordResetEmail(
	            $forgottenForm->get('email')->getData(),
	            $mailer
	        );
	    }
	
	    $parameters = [];
	    $parameters['forgotten'] = $forgottenForm->createView();
	    return $this->render('authentication/forgotten_request.html.twig', $parameters);
	}
	
	private function processSendingPasswordResetEmail(string $identifier, MailerInterface $mailer): RedirectResponse
	{
	    $this->logger->info("Request to reset password for " . $identifier);
	    $user = $this->authenticationRepository->findOneBy([
	        'email' => $identifier,
	    ]);
	
	    // Do not reveal whether a user account was found or not.
	    if (!$user) {
	        $this->logger->info($identifier . "does not identify a valid user");
	        return $this->redirectToRoute('check-email');
	    }
	
	    try {
	        $resetToken = $this->resetPasswordHelper->generateResetToken($user);
	    } catch (ResetPasswordExceptionInterface $e) {
	        $this->logger->info("Problem getting reset token " . $e->getReason());
	        // If you want to tell the user why a reset email was not sent, uncomment
	        // the lines below and change the redirect to 'forgotten-password'.
	        // Caution: This may reveal if a user is registered or not.
	        //
	        // $this->addFlash('reset_password_error', sprintf(
	        //     'There was a problem handling your password reset request - %s',
	        //     $e->getReason()
	        // ));
	
	        return $this->redirectToRoute('check-email');
	    }
	
	    $this->logger->info("Sending password reset request email to " . $user->getEmail());
	    $email = (new TemplatedEmail())
	        ->from(new Address(
	                /* @phpstan-ignore-next-line */
	                $this->getParameter('app.security.from_email'),
	                /* @phpstan-ignore-next-line */
	                $this->getParameter('app.security.from_name')))
	        ->to($user->getEmail())
	        ->subject('Your password reset request')
	        ->htmlTemplate('authentication/forgotten_email.html.twig')
	        ->context([
	            'user' => $user,
	            'resetToken' => $resetToken,
	        ]);
	
	    $mailer->send($email);
	
	    // Store the token object in session for retrieval in check-email route.
	    $this->setTokenObjectInSession($resetToken);
	
	    return $this->redirectToRoute('check-email');
	}
	
	/**
	 * Confirmation page after a user has requested a password reset.
	 *
	 **/
	#[Route('/forgotten-password/check-email', name: 'check-email')]
	public function forgottenCheckEmail(): Response
	{
	    // Generate a fake token if the user does not exist or someone hit this page directly.
	    // This prevents exposing whether or not a user was found with the given email address or not
	    if (null === ($resetToken = $this->getTokenObjectFromSession())) {
	        $resetToken = $this->resetPasswordHelper->generateFakeResetToken();
	    }
	
	    return $this->render('authentication/forgotten_check.html.twig', [
	//        'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
	       'resetToken' => $resetToken,
	    ]);
	}
	
	/**
	 **/
	#[Route('/forgotten-password/reset/{token}', name: 'reset')]
	public function forgottenReset(Request $request, UserPasswordHasherInterface $passwordHasher, string $token = null): Response
	{
	    if ($token) {
	        // We store the token in session and remove it from the URL, to avoid the URL being
	        // loaded in a browser and potentially leaking the token to 3rd party JavaScript.
	        $this->storeTokenInSession($token);
	
	        return $this->redirectToRoute('reset');
	    }
	
	    $token = $this->getTokenFromSession();
	    if (null === $token) {
	        throw $this->createNotFoundException('No reset password token found in the URL or in the session.');
	    }
	
	    try {
	        /** @var Authentication $user */
	        $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
	    } catch (ResetPasswordExceptionInterface $e) {
	        $this->addFlash('reset_password_error', sprintf(
	            '%s - %s',
	            $this->translator->trans(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE, [], 'ResetPasswordBundle'),
	            $this->translator->trans($e->getReason(), [], 'ResetPasswordBundle')
	        ));
	
	        return $this->redirectToRoute('forgotten-password');
	    }
	
	    // The token is valid; allow the user to change their password.
	    $forgottenForm = $this->createForm(ForgottenPasswordReset::class);
	    $forgottenForm->handleRequest($request);
	    if ($forgottenForm->isSubmitted() && $forgottenForm->isValid()) {
	        // A password reset token should be used only once, remove it.
	        $this->resetPasswordHelper->removeResetRequest($token);
	
	        $encodedPassword = $passwordHasher->hashPassword(
	            $user,
	            $forgottenForm->get('plainPassword')->getData()
	        );
	        $user->setPassword($encodedPassword);
	        $this->authenticationRepository->save($user, true);
	
	        // The session is cleaned up after the password has been changed.
	        $this->cleanSessionAfterReset();
	        $this->logger->info("Password changed for " . $user->getUserIdentifier());
	
	        return $this->redirectToRoute('competitions_past');
	    }
	
	    $parameters = [];
	    $parameters['forgotten'] = $forgottenForm->createView();
	    return $this->render('authentication/forgotten_reset.html.twig', $parameters);
	}

	/**
	 **/
	#[Route('/change-password/{token}', name: 'change-password')]
	public function resetReset(Request $request, UserPasswordHasherInterface $passwordHasher, string $token = null): Response
	{
	}
}
