<?php
namespace App\Controller\Web;

use App\Repository\CompetitionsRepository;
use App\Repository\SectionEntriesRepository;
use App\Service\ImageVariantsService;
use App\Service\SectionEntriesService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CompetitionResultsController extends AbstractController
{
    /**
     * @var CompetitionsRepository $competitionsRepository
     */
    private CompetitionsRepository $competitionsRepository;
    
    /**
     * @var SectionEntriesRepository $sectionEntriesRepository
     */
    private SectionEntriesRepository $sectionEntriesRepository;

    public function __construct(CompetitionsRepository $competitionsRepository, SectionEntriesRepository $sectionEntriesRepository)
    {
        $this->competitionsRepository = $competitionsRepository;
        $this->sectionEntriesRepository = $sectionEntriesRepository;
    }

    /**
     */
    #[Route('/competitions/{title}/results', name: 'competitionResults')]
    public function competitionResults(string $title): Response
    {
        $parameters = [];
        
        $results = $this->competitionsRepository->findOne($title);
        if (is_null($results)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $parameters['results'] = $results;
        
        if (!($results->isPublicResults() != 1)) {
            $parameters['entries'] = $this->sectionEntriesRepository->findOfCompetition($title);
        }
        
        return $this->render('competition_results/index.html.twig', $parameters);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT_PLUS')]
    #[Route('/competitions/{eventTitle}/sections/{competitionSectionTitle}/entries/{sectionEntryTitle}/showcase', name: 'competitionResults_showcase')]
    public function showcase(SectionEntriesService $sectionEntriesService, string $eventTitle, string $competitionSectionTitle, string $sectionEntryTitle): Response
    {
        $sectionEntry = $this->sectionEntriesRepository->findOne($eventTitle, $competitionSectionTitle, $sectionEntryTitle);
        if (is_null($sectionEntry)) {
            throw $this->createNotFoundException('The SectionEntry does not exist');
        }
        $sectionEntriesService->toggleClubShowcase($sectionEntry);
        return $this->redirectToRoute('competitionResults', [
            'title' => $eventTitle,
        ]);
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT')]
    #[Route('/competitions/{eventTitle}/sections/{competitionSectionTitle}/entries/{sectionEntryTitle}/download', name: 'competitionResults_download')]
    public function download(ImageVariantsService $imageVariantsService, string $eventTitle, string $competitionSectionTitle, string $sectionEntryTitle): BinaryFileResponse
    {
        $sectionEntry = $this->sectionEntriesRepository->findOne($eventTitle, $competitionSectionTitle, $sectionEntryTitle);
        if (is_null($sectionEntry)) {
            throw $this->createNotFoundException('The SectionEntry does not exist');
        }
        $file = $imageVariantsService->exportImage($sectionEntry);
        $response = $this->file($file, $sectionEntry->getTitle() . '.jpg');
        $response->headers->set('Content-Type', 'image/jpeg');
        return $response;
    }

}
