<?php
namespace App\Controller\Web;

use App\Entity\Competition;
use App\Entity\CumulativeCompetition;
use App\Repository\CompetitionsRepository;
use App\Repository\CumulativeCompetitionsRepository;
use App\Repository\CumulativeEntriesRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CompetitionsController extends AbstractController
{
    private const UPCOMING_PAGE_SIZE = 12;
    
    private const PAST_PAGE_SIZE = 12;
    
    private const BPE_PAGE_SIZE = 10;
    
    private const CUMULATIVE_PAGE_SIZE = 12;

    /**
     * @var CompetitionsRepository $competitionsRepository
     */
    private CompetitionsRepository $competitionsRepository;
    
    /**
     * @var CumulativeCompetitionsRepository $cumulativeCompetitionsRepository
     */
    private CumulativeCompetitionsRepository $cumulativeCompetitionsRepository;
    
    /**
     * @var CumulativeEntriesRepository $cumulativeEntriesRepository
     */
    private CumulativeEntriesRepository $cumulativeEntriesRepository;

    public function __construct(CompetitionsRepository $competitionsRepository, CumulativeCompetitionsRepository $cumulativeCompetitionsRepository, CumulativeEntriesRepository $cumulativeEntriesRepository)
    {
        $this->competitionsRepository = $competitionsRepository;
        $this->cumulativeCompetitionsRepository = $cumulativeCompetitionsRepository;
        $this->cumulativeEntriesRepository = $cumulativeEntriesRepository;
    }

    /**
     */
    #[Route('/competitions', name: 'competitions')]
    public function upcoming(Request $request): Response
    {
        $query = $this->competitionsRepository->findFuturePublicQuery();
        $adapter = new QueryAdapter($query);
        $upcoming = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::UPCOMING_PAGE_SIZE);
        return $this->render('competitions/upcoming.html.twig', [
            'upcoming' => $upcoming
        ]);
    }

    /**
     */
    #[Route('/competitions/{title}/details', name: 'competitions_details')]
    public function details(Request $request, string $title): Response
    {
        $details = $this->competitionsRepository->findOne($title);
        if (is_null($details)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        return $this->render('competitions/details.html.twig', [
            'details' => $details
        ]);
    }

    /**
     */
    #[Route('', name: 'competitions_past')]
    public function past(Request $request): Response
    {
        if ($request->query->has('publicWithTitleLikeFilter'))
            $query = $this->competitionsRepository->findPastPublicWithTitleQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('competitionTitle'),
            );
        else
            $query = $this->competitionsRepository->findPastPublicQuery();
        $adapter = new QueryAdapter($query);
        $past = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::PAST_PAGE_SIZE);
        return $this->render('competitions/past.html.twig', [
            'past' => $past
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_BPE_ACTIVE')]
    #[Route('/competitions/bpe', name: 'competitions_bpe')]
    public function bpe(Request $request): Response
    {
        $query = $this->competitionsRepository->findBPEQuery();
        $adapter = new QueryAdapter($query);
        $bpe = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::BPE_PAGE_SIZE);
        return $this->render('competitions/bpe.html.twig', [
            'bpe' => $bpe
        ]);
    }

    /**
     */
    #[Route('/competitions/cumulative', name: 'competitions_cumulative')]
    public function cumulative(Request $request): Response
    {
        if ($request->query->has('nameLikeFilter'))
            $query = $this->cumulativeCompetitionsRepository->findPublicWithNameQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('competition'),
            );
        else
            $query = $this->cumulativeCompetitionsRepository->findPublicQuery();
        $adapter = new QueryAdapter($query);
        $cumulative = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::CUMULATIVE_PAGE_SIZE);
        return $this->render('competitions/cumulative.html.twig', [
            'cumulative' => $cumulative
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER')]
    #[Route('/competitions/cumulative/{name}/cumulative_Details', name: 'competitions_cumulativeDetails')]
    public function cumulativeDetails(Request $request, string $name): Response
    {
        $parameters = [];
        $cumulativeCompetition = $this->cumulativeCompetitionsRepository->findIgnoringGroup($name);
        if (is_null($cumulativeCompetition)) {
            throw $this->createNotFoundException('The CumulativeCompetition does not exist');
        }
        $parameters['cumulativeCompetition'] = $cumulativeCompetition;
        if (!($cumulativeCompetition->isPrivate() == 1)) {
            $parameters['cumulativeDetails'] = $this->cumulativeEntriesRepository->findForCompetition($name);
        }
        return $this->render('competitions/cumulative_details.html.twig', $parameters);
    }

}
