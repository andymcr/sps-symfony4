<?php
namespace App\Controller\Web;

use App\Entity\Person;
use App\Repository\ImagesRepository;
use App\Repository\PeopleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class GalleriesController extends AbstractController
{
    /**
     * @var ImagesRepository $imagesRepository
     */
    private ImagesRepository $imagesRepository;
    
    /**
     * @var PeopleRepository $peopleRepository
     */
    private PeopleRepository $peopleRepository;

    public function __construct(ImagesRepository $imagesRepository, PeopleRepository $peopleRepository)
    {
        $this->imagesRepository = $imagesRepository;
        $this->peopleRepository = $peopleRepository;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/galleries', name: 'galleries')]
    public function galleries(): Response
    {
        $galleries = $this->peopleRepository->findWithGallery();
        return $this->render('galleries/galleries.html.twig', [
            'galleries' => $galleries
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/galleries/{username}', name: 'galleries_gallery')]
    public function gallery(string $username): Response
    {
        $person = $this->peopleRepository->findOne($username);
        if (is_null($person)) {
            throw $this->createNotFoundException('The Person does not exist');
        }
        $gallery = $this->imagesRepository->findPersonalShowcase($username);
        return $this->render('galleries/gallery.html.twig', [
            'person' => $person,
            'gallery' => $gallery
        ]);
    }

}
