<?php
namespace App\Controller\Web;

use App\Repository\EventsRepository;
use App\Repository\ImageVariantsRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    private const EVENTS_PAGE_SIZE = 6;

    /**
     * @var EventsRepository $eventsRepository
     */
    private EventsRepository $eventsRepository;
    
    /**
     * @var ImageVariantsRepository $imageVariantsRepository
     */
    private ImageVariantsRepository $imageVariantsRepository;

    public function __construct(EventsRepository $eventsRepository, ImageVariantsRepository $imageVariantsRepository)
    {
        $this->eventsRepository = $eventsRepository;
        $this->imageVariantsRepository = $imageVariantsRepository;
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/showcase', name: 'home')]
    public function home(Request $request): Response
    {
        $parameters = [];
        
        $parameters['showcase'] = $this->imageVariantsRepository->findClubShowcase();
        
        $query = $this->eventsRepository->findFutureQuery();
        $adapter = new QueryAdapter($query);
        $parameters['events'] = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::EVENTS_PAGE_SIZE);
        
        return $this->render('home/index.html.twig', $parameters);
    }

}
