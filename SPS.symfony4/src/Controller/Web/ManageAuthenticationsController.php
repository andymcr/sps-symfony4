<?php
namespace App\Controller\Web;

use App\Entity\Authentication;
use App\Form\AuthenticationUpdateType;
use App\Repository\AuthenticationRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


class ManageAuthenticationsController extends AbstractController
{
    private const AUTHENTICATIONS_PAGE_SIZE = 12;

    /**
     * @var AuthenticationRepository $authenticationRepository
     */
    private AuthenticationRepository $authenticationRepository;
    
    /**
     * @var TranslatorInterface $translationService
     */
    private TranslatorInterface $translationService;

    public function __construct(AuthenticationRepository $authenticationRepository, TranslatorInterface $translator)
    {
        $this->authenticationRepository = $authenticationRepository;
        $this->translationService = $translator;
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/authentications', name: 'manageAuthentications')]
    public function authentications(Request $request): Response
    {
        $query = $this->authenticationRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $authentications = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::AUTHENTICATIONS_PAGE_SIZE);
        return $this->render('admin/manage_authentications/authentications.html.twig', [
            'authentications' => $authentications
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_SECURITY')]
    #[Route('/admin/authentications/{email}/delete', name: 'manageAuthentications_delete')]
    public function delete(Request $request, string $email): Response
    {
        $authentication = $this->authenticationRepository->findOne($email);
        if (is_null($authentication)) {
            throw $this->createNotFoundException('The Authentication does not exist');
        }
        $this->authenticationRepository->remove($authentication, true);
    
        return $this->redirectToRoute('manageAuthentications', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_SECURITY')]
    #[Route('/admin/authentications/{email}/update', name: 'manageAuthentications_update')]
    public function update(Request $request, string $email): Response
    {
        $update = $this->authenticationRepository->findOne($email);
        if (!$update) {
            throw $this->createNotFoundException('The Authentication does not exist');
        }
        $updateForm = $this->createForm(AuthenticationUpdateType::class, $update, [
            'action' => $this->generateUrl('manageAuthentications_update', array_merge(
                $request->query->all(), [
                    'email' => $email,
            ])),
        ]);
        $updateForm->handleRequest($request);
        if ($updateForm->isSubmitted() && $updateForm->isValid()) {
            $this->authenticationRepository->save($update, true);
            $this->addFlash('success', $this->translationService->trans('manage_authentications.update.messages.success', [], 'admin'));
            return $this->redirectToRoute('manageAuthentications', $request->query->all());
        }
        return $this->renderForm('admin/manage_authentications/update.html.twig', [
            'update' => $updateForm
        ]);
    }

}
