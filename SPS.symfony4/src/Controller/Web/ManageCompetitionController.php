<?php
namespace App\Controller\Web;

use App\Repository\CompetitionsRepository;
use App\Service\CompetitionReportService;
use App\Service\ImageExportService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageCompetitionController extends AbstractController
{
    /**
     * @var CompetitionsRepository $competitionsRepository
     */
    private CompetitionsRepository $competitionsRepository;

    public function __construct(CompetitionsRepository $competitionsRepository)
    {
        $this->competitionsRepository = $competitionsRepository;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details', name: 'manageCompetition')]
    public function competition(string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        return $this->render('admin/manage_competition/competition.html.twig', [
            'competition' => $competition
        ]);
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details/export_Secretary', name: 'manageCompetition_exportSecretary')]
    public function exportSecretary(CompetitionReportService $competitionReportService, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $competitionReportService->entriesDetailed($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.docx');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        return $response;
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details/export_Judge', name: 'manageCompetition_exportJudge')]
    public function exportJudge(CompetitionReportService $competitionReportService, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $competitionReportService->entriesAnonymous($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.docx');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        return $response;
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details/images_Dicentra', name: 'manageCompetition_imagesDicentra')]
    public function imagesDicentra(ImageExportService $imageExportService, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $imageExportService->dicentra($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.zip');
        $response->headers->set('Content-Type', 'application/zip');
        return $response;
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details/images_Judge', name: 'manageCompetition_imagesJudge')]
    public function imagesJudge(ImageExportService $imageExportService, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $imageExportService->judge($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.zip');
        $response->headers->set('Content-Type', 'application/zip');
        return $response;
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details/export_Entry_Lables', name: 'manageCompetition_exportEntryLables')]
    public function exportEntryLables(CompetitionReportService $competitionReportService, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $competitionReportService->entryLabels($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.xlsx');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return $response;
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/details/export_Place_Labels', name: 'manageCompetition_exportPlaceLabels')]
    public function exportPlaceLabels(CompetitionReportService $competitionReportService, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $competitionReportService->placeLabels($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.xlsx');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return $response;
    }

}
