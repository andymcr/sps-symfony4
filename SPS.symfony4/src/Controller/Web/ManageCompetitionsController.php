<?php
namespace App\Controller\Web;

use App\Entity\Competition;
use App\Entity\CompetitionAward;
use App\Entity\CompetitionCategory;
use App\Entity\CompetitionSection;
use App\Form\CompetitionAwardType;
use App\Form\CompetitionCategoryType;
use App\Form\CompetitionType;
use App\Form\CompetitionSectionType;
use App\Repository\CompetitionAwardsRepository;
use App\Repository\CompetitionCategoriesRepository;
use App\Repository\CompetitionSectionsRepository;
use App\Repository\CompetitionsRepository;
use App\Service\CompetitionReportService;
use App\Service\CompetitionsService;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageCompetitionsController extends AbstractController
{
    private const COMPETITIONS_PAGE_SIZE = 12;
    
    private const CATEGORIES_PAGE_SIZE = 10;
    
    private const AWARDS_PAGE_SIZE = 10;

    /**
     * @var CompetitionAwardsRepository $competitionAwardsRepository
     */
    private CompetitionAwardsRepository $competitionAwardsRepository;
    
    /**
     * @var CompetitionCategoriesRepository $competitionCategoriesRepository
     */
    private CompetitionCategoriesRepository $competitionCategoriesRepository;
    
    /**
     * @var CompetitionSectionsRepository $competitionSectionsRepository
     */
    private CompetitionSectionsRepository $competitionSectionsRepository;
    
    /**
     * @var CompetitionsRepository $competitionsRepository
     */
    private CompetitionsRepository $competitionsRepository;

    public function __construct(CompetitionAwardsRepository $competitionAwardsRepository, CompetitionCategoriesRepository $competitionCategoriesRepository, CompetitionSectionsRepository $competitionSectionsRepository, CompetitionsRepository $competitionsRepository)
    {
        $this->competitionAwardsRepository = $competitionAwardsRepository;
        $this->competitionCategoriesRepository = $competitionCategoriesRepository;
        $this->competitionSectionsRepository = $competitionSectionsRepository;
        $this->competitionsRepository = $competitionsRepository;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions', name: 'manageCompetitions')]
    public function competitions(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->competitionsRepository->findWithTitleQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('competitionTitle'),
            );
        else
            $query = $this->competitionsRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $competitions = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::COMPETITIONS_PAGE_SIZE);
        return $this->render('admin/manage_competitions/competitions.html.twig', [
            'competitions' => $competitions
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/delete', name: 'manageCompetitions_deleteCompetition')]
    public function deleteCompetition(Request $request, string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $this->competitionsRepository->remove($competition, true);
    
        return $this->redirectToRoute('manageCompetitions', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/create', name: 'manageCompetitions_createCompetition')]
    public function createCompetition(Request $request): Response
    {
        $editCompetition = new Competition();
        $editCompetitionForm = $this->createForm(CompetitionType::class, $editCompetition, [
            'action' => $this->generateUrl('manageCompetitions_createCompetition', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions');
        }
        $editCompetitionForm->handleRequest($request);
        if ($editCompetitionForm->isSubmitted() && $editCompetitionForm->isValid()) {
            $this->competitionsRepository->save($editCompetition, true);
            return $this->redirectToRoute('manageCompetitions', $request->query->all());
        }
        return $this->renderForm('admin/manage_competitions/edit_competition.html.twig', [
            'editCompetition' => $editCompetitionForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/update', name: 'manageCompetitions_updateCompetition')]
    public function updateCompetition(Request $request, string $title): Response
    {
        $editCompetition = $this->competitionsRepository->findOne($title);
        if (!$editCompetition) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $editCompetitionForm = $this->createForm(CompetitionType::class, $editCompetition, [
            'action' => $this->generateUrl('manageCompetitions_updateCompetition', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions');
        }
        $editCompetitionForm->handleRequest($request);
        if ($editCompetitionForm->isSubmitted() && $editCompetitionForm->isValid()) {
            $this->competitionsRepository->save($editCompetition, true);
            return $this->redirectToRoute('manageCompetitions', $request->query->all());
        }
        return $this->renderForm('admin/manage_competitions/edit_competition.html.twig', [
            'editCompetition' => $editCompetitionForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/categories', name: 'manageCompetitions_categories')]
    public function categories(Request $request): Response
    {
        $query = $this->competitionCategoriesRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $categories = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::CATEGORIES_PAGE_SIZE);
        return $this->render('admin/manage_competitions/categories.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/categories/create', name: 'manageCompetitions_createCategory')]
    public function createCategory(Request $request): Response
    {
        $editCategory = new CompetitionCategory();
        $editCategoryForm = $this->createForm(CompetitionCategoryType::class, $editCategory, [
            'action' => $this->generateUrl('manageCompetitions_createCategory', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions_categories');
        }
        $editCategoryForm->handleRequest($request);
        if ($editCategoryForm->isSubmitted() && $editCategoryForm->isValid()) {
            $this->competitionCategoriesRepository->save($editCategory, true);
            return $this->redirectToRoute('manageCompetitions_categories', $request->query->all());
        }
        return $this->renderForm('admin/manage_competitions/edit_category.html.twig', [
            'editCategory' => $editCategoryForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/categories/{shortName}/update', name: 'manageCompetitions_updateCategory')]
    public function updateCategory(Request $request, string $shortName): Response
    {
        $editCategory = $this->competitionCategoriesRepository->findOne($shortName);
        if (!$editCategory) {
            throw $this->createNotFoundException('The CompetitionCategory does not exist');
        }
        $editCategoryForm = $this->createForm(CompetitionCategoryType::class, $editCategory, [
            'action' => $this->generateUrl('manageCompetitions_updateCategory', array_merge(
                $request->query->all(), [
                    'shortName' => $shortName,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions_categories');
        }
        $editCategoryForm->handleRequest($request);
        if ($editCategoryForm->isSubmitted() && $editCategoryForm->isValid()) {
            $this->competitionCategoriesRepository->save($editCategory, true);
            return $this->redirectToRoute('manageCompetitions_categories', $request->query->all());
        }
        return $this->renderForm('admin/manage_competitions/edit_category.html.twig', [
            'editCategory' => $editCategoryForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/sections', name: 'manageCompetitions_sections')]
    public function sections(Request $request, string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $sections = $this->competitionSectionsRepository->findOfCompetition($title);
        return $this->render('admin/manage_competitions/sections.html.twig', [
            'competition' => $competition,
            'sections' => $sections
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/add_Merits_Sections', name: 'manageCompetitions_addMeritsSections')]
    public function addMeritsSections(CompetitionsService $competitionsService, Request $request, string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $competitionsService->addMeritsSections($competition);
        return $this->redirectToRoute('manageCompetitions_sections', array_merge(
            $request->query->all(),
            [
                'title' => $title,
        ]));
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/add_Quarterly_Sections', name: 'manageCompetitions_addQuarterlySections')]
    public function addQuarterlySections(CompetitionsService $competitionsService, Request $request, string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $competitionsService->addQuarterlySections($competition);
        return $this->redirectToRoute('manageCompetitions_sections', array_merge(
            $request->query->all(),
            [
                'title' => $title,
        ]));
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/export_Certificates', name: 'manageCompetitions_exportCertificates')]
    public function exportCertificates(CompetitionReportService $competitionReportService, Request $request, string $title): BinaryFileResponse
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $file = $competitionReportService->full($competition);
        $response = $this->file($file, $competition->getEvent()->getTitle() . '.xlsx');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return $response;
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{eventTitle}/sections/{competitionSectionTitle}/delete', name: 'manageCompetitions_deleteCompetitionSection')]
    public function deleteCompetitionSection(Request $request, string $eventTitle, string $competitionSectionTitle): Response
    {
        $competitionSection = $this->competitionSectionsRepository->findOne($eventTitle, $competitionSectionTitle);
        if (is_null($competitionSection)) {
            throw $this->createNotFoundException('The CompetitionSection does not exist');
        }
        $this->competitionSectionsRepository->remove($competitionSection, true);
    
        return $this->redirectToRoute('manageCompetitions_sections', array_merge(
            $request->query->all(),
            [
                'title' => $eventTitle,
        ]));
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/sections/create', name: 'manageCompetitions_createSection')]
    public function createSection(Request $request, string $title): Response
    {
        $editSection = new CompetitionSection();
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $editSection->setPartOf($competition);
        $editSectionForm = $this->createForm(CompetitionSectionType::class, $editSection, [
            'action' => $this->generateUrl('manageCompetitions_createSection', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions_sections');
        }
        $editSectionForm->handleRequest($request);
        if ($editSectionForm->isSubmitted() && $editSectionForm->isValid()) {
            $this->competitionSectionsRepository->save($editSection, true);
            return $this->redirectToRoute('manageCompetitions_sections', array_merge(
                $request->query->all(),
                [
                    'title' => $title,
            ]));
        }
        return $this->renderForm('admin/manage_competitions/edit_section.html.twig', [
            'editSection' => $editSectionForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{eventTitle}/sections/{competitionSectionTitle}/update', name: 'manageCompetitions_updateSection')]
    public function updateSection(Request $request, string $eventTitle, string $competitionSectionTitle): Response
    {
        $editSection = $this->competitionSectionsRepository->findOne($eventTitle, $competitionSectionTitle);
        if (!$editSection) {
            throw $this->createNotFoundException('The CompetitionSection does not exist');
        }
        $editSectionForm = $this->createForm(CompetitionSectionType::class, $editSection, [
            'action' => $this->generateUrl('manageCompetitions_updateSection', array_merge(
                $request->query->all(), [
                    'eventTitle' => $eventTitle,
                    'competitionSectionTitle' => $competitionSectionTitle,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions_sections');
        }
        $editSectionForm->handleRequest($request);
        if ($editSectionForm->isSubmitted() && $editSectionForm->isValid()) {
            $this->competitionSectionsRepository->save($editSection, true);
            return $this->redirectToRoute('manageCompetitions_sections', array_merge(
                $request->query->all(),
                [
                    'title' => $eventTitle,
            ]));
        }
        return $this->renderForm('admin/manage_competitions/edit_section.html.twig', [
            'editSection' => $editSectionForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/awards', name: 'manageCompetitions_awards')]
    public function awards(Request $request, string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $query = $this->competitionAwardsRepository->findOfCompetitionQuery($title);
        $adapter = new QueryAdapter($query);
        $awards = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::AWARDS_PAGE_SIZE);
        return $this->render('admin/manage_competitions/awards.html.twig', [
            'competition' => $competition,
            'awards' => $awards
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/awards/create', name: 'manageCompetitions_createAward')]
    public function createAward(Request $request, string $title): Response
    {
        $editAward = new CompetitionAward();
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $editAward->setAwardFor($competition);
        $editAwardForm = $this->createForm(CompetitionAwardType::class, $editAward, [
            'action' => $this->generateUrl('manageCompetitions_createAward', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions_awards');
        }
        $editAwardForm->handleRequest($request);
        if ($editAwardForm->isSubmitted() && $editAwardForm->isValid()) {
            $this->competitionAwardsRepository->save($editAward, true);
            return $this->redirectToRoute('manageCompetitions_awards', array_merge(
                $request->query->all(),
                [
                    'title' => $title,
            ]));
        }
        return $this->renderForm('admin/manage_competitions/edit_award.html.twig', [
            'editAward' => $editAwardForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/awards/{award}/update', name: 'manageCompetitions_updateAward')]
    public function updateAward(Request $request, string $title, int $award): Response
    {
        $editAward = $this->competitionAwardsRepository->findOne($title, $award);
        if (!$editAward) {
            throw $this->createNotFoundException('The CompetitionAward does not exist');
        }
        $editAwardForm = $this->createForm(CompetitionAwardType::class, $editAward, [
            'action' => $this->generateUrl('manageCompetitions_updateAward', array_merge(
                $request->query->all(), [
                    'title' => $title,
                    'award' => $award,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions_awards');
        }
        $editAwardForm->handleRequest($request);
        if ($editAwardForm->isSubmitted() && $editAwardForm->isValid()) {
            $this->competitionAwardsRepository->save($editAward, true);
            return $this->redirectToRoute('manageCompetitions_awards', array_merge(
                $request->query->all(),
                [
                    'title' => $title,
            ]));
        }
        return $this->renderForm('admin/manage_competitions/edit_award.html.twig', [
            'editAward' => $editAwardForm
        ]);
    }

}
