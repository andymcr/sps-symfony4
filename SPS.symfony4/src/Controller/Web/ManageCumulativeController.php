<?php
namespace App\Controller\Web;

use App\Entity\CumulativeCompetition;
use App\Entity\CumulativeGroup;
use App\Form\CumulativeCompetitionType;
use App\Form\CumulativeGroupType;
use App\Repository\CumulativeCompetitionsRepository;
use App\Repository\CumulativeGroupsRepository;
use App\Service\CumulativeService;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


class ManageCumulativeController extends AbstractController
{
    private const CUMULATIVEGROUPS_PAGE_SIZE = 12;

    /**
     * @var CumulativeCompetitionsRepository $cumulativeCompetitionsRepository
     */
    private CumulativeCompetitionsRepository $cumulativeCompetitionsRepository;
    
    /**
     * @var CumulativeGroupsRepository $cumulativeGroupsRepository
     */
    private CumulativeGroupsRepository $cumulativeGroupsRepository;
    
    /**
     * @var TranslatorInterface $translationService
     */
    private TranslatorInterface $translationService;

    public function __construct(CumulativeCompetitionsRepository $cumulativeCompetitionsRepository, CumulativeGroupsRepository $cumulativeGroupsRepository, TranslatorInterface $translator)
    {
        $this->cumulativeCompetitionsRepository = $cumulativeCompetitionsRepository;
        $this->cumulativeGroupsRepository = $cumulativeGroupsRepository;
        $this->translationService = $translator;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative', name: 'manageCumulative')]
    public function cumulativeGroups(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->cumulativeGroupsRepository->findTitleLikeQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('title'),
            );
        else
            $query = $this->cumulativeGroupsRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $cumulativeGroups = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::CUMULATIVEGROUPS_PAGE_SIZE);
        return $this->render('admin/manage_cumulative/cumulative_groups.html.twig', [
            'cumulativeGroups' => $cumulativeGroups
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{name}/tally', name: 'manageCumulative_tally')]
    public function tally(CumulativeService $cumulativeService, Request $request, string $name): Response
    {
        $cumulativeGroup = $this->cumulativeGroupsRepository->findOne($name);
        if (is_null($cumulativeGroup)) {
            throw $this->createNotFoundException('The CumulativeGroup does not exist');
        }
        $cumulativeService->tally($cumulativeGroup);
        $this->addFlash('success', $this->translationService->trans('manage_cumulative.cumulative_groups.actions.tally.messages.success', [], 'admin'));
        return $this->redirectToRoute('manageCumulative', $request->query->all());
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{name}/download_Details', name: 'manageCumulative_downloadDetails')]
    public function downloadDetails(CumulativeService $cumulativeService, Request $request, string $name): BinaryFileResponse
    {
        $cumulativeGroup = $this->cumulativeGroupsRepository->findOne($name);
        if (is_null($cumulativeGroup)) {
            throw $this->createNotFoundException('The CumulativeGroup does not exist');
        }
        $file = $cumulativeService->details($cumulativeGroup);
        $response = $this->file($file, $cumulativeGroup->getName() . '.xlsx');
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return $response;
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{name}/delete', name: 'manageCumulative_deleteCumulativeGroup')]
    public function deleteCumulativeGroup(Request $request, string $name): Response
    {
        $cumulativeGroup = $this->cumulativeGroupsRepository->findOne($name);
        if (is_null($cumulativeGroup)) {
            throw $this->createNotFoundException('The CumulativeGroup does not exist');
        }
        $this->cumulativeGroupsRepository->remove($cumulativeGroup, true);
    
        return $this->redirectToRoute('manageCumulative', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/create', name: 'manageCumulative_createGroup')]
    public function createGroup(Request $request): Response
    {
        $editGroup = new CumulativeGroup();
        $editGroupForm = $this->createForm(CumulativeGroupType::class, $editGroup, [
            'action' => $this->generateUrl('manageCumulative_createGroup', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCumulative');
        }
        $editGroupForm->handleRequest($request);
        if ($editGroupForm->isSubmitted() && $editGroupForm->isValid()) {
            $this->cumulativeGroupsRepository->save($editGroup, true);
            return $this->redirectToRoute('manageCumulative', $request->query->all());
        }
        return $this->renderForm('admin/manage_cumulative/edit_group.html.twig', [
            'editGroup' => $editGroupForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{name}/update', name: 'manageCumulative_updateGroup')]
    public function updateGroup(Request $request, string $name): Response
    {
        $editGroup = $this->cumulativeGroupsRepository->findOne($name);
        if (!$editGroup) {
            throw $this->createNotFoundException('The CumulativeGroup does not exist');
        }
        $editGroupForm = $this->createForm(CumulativeGroupType::class, $editGroup, [
            'action' => $this->generateUrl('manageCumulative_updateGroup', array_merge(
                $request->query->all(), [
                    'name' => $name,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCumulative');
        }
        $editGroupForm->handleRequest($request);
        if ($editGroupForm->isSubmitted() && $editGroupForm->isValid()) {
            $this->cumulativeGroupsRepository->save($editGroup, true);
            return $this->redirectToRoute('manageCumulative', $request->query->all());
        }
        return $this->renderForm('admin/manage_cumulative/edit_group.html.twig', [
            'editGroup' => $editGroupForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{name}/competitions', name: 'manageCumulative_competitions')]
    public function competitions(Request $request, string $name): Response
    {
        $cumulativeGroup = $this->cumulativeGroupsRepository->findOne($name);
        if (is_null($cumulativeGroup)) {
            throw $this->createNotFoundException('The CumulativeGroup does not exist');
        }
        $competitions = $this->cumulativeCompetitionsRepository->findPartOfGroup($name);
        return $this->render('admin/manage_cumulative/competitions.html.twig', [
            'cumulativeGroup' => $cumulativeGroup,
            'competitions' => $competitions
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{cumulativeGroupName}/competitions/{cumulativeCompetitionName}/delete', name: 'manageCumulative_deleteCumulativeCompetition')]
    public function deleteCumulativeCompetition(Request $request, string $cumulativeGroupName, string $cumulativeCompetitionName): Response
    {
        $cumulativeCompetition = $this->cumulativeCompetitionsRepository->findOne($cumulativeGroupName, $cumulativeCompetitionName);
        if (is_null($cumulativeCompetition)) {
            throw $this->createNotFoundException('The CumulativeCompetition does not exist');
        }
        $this->cumulativeCompetitionsRepository->remove($cumulativeCompetition, true);
    
        return $this->redirectToRoute('manageCumulative_competitions', array_merge(
            $request->query->all(),
            [
                'name' => $cumulativeGroupName,
        ]));
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{name}/competitions/create', name: 'manageCumulative_createCompetition')]
    public function createCompetition(Request $request, string $name): Response
    {
        $editCompetition = new CumulativeCompetition();
        $cumulativeGroup = $this->cumulativeGroupsRepository->findOne($name);
        if (is_null($cumulativeGroup)) {
            throw $this->createNotFoundException('The CumulativeGroup does not exist');
        }
        $editCompetition->setPartOf($cumulativeGroup);
        $editCompetitionForm = $this->createForm(CumulativeCompetitionType::class, $editCompetition, [
            'action' => $this->generateUrl('manageCumulative_createCompetition', array_merge(
                $request->query->all(), [
                    'name' => $name,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions');
        }
        $editCompetitionForm->handleRequest($request);
        if ($editCompetitionForm->isSubmitted() && $editCompetitionForm->isValid()) {
            $this->cumulativeCompetitionsRepository->save($editCompetition, true);
            return $this->redirectToRoute('manageCumulative_competitions', array_merge(
                $request->query->all(),
                [
                    'name' => $name,
            ]));
        }
        return $this->renderForm('admin/manage_cumulative/edit_competition.html.twig', [
            'editCompetition' => $editCompetitionForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/cumulative/{cumulativeGroupName}/competitions/{cumulativeCompetitionName}/update', name: 'manageCumulative_updateCompetition')]
    public function updateCompetition(Request $request, string $cumulativeGroupName, string $cumulativeCompetitionName): Response
    {
        $editCompetition = $this->cumulativeCompetitionsRepository->findOne($cumulativeGroupName, $cumulativeCompetitionName);
        if (!$editCompetition) {
            throw $this->createNotFoundException('The CumulativeCompetition does not exist');
        }
        $editCompetitionForm = $this->createForm(CumulativeCompetitionType::class, $editCompetition, [
            'action' => $this->generateUrl('manageCumulative_updateCompetition', array_merge(
                $request->query->all(), [
                    'cumulativeGroupName' => $cumulativeGroupName,
                    'cumulativeCompetitionName' => $cumulativeCompetitionName,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageCompetitions');
        }
        $editCompetitionForm->handleRequest($request);
        if ($editCompetitionForm->isSubmitted() && $editCompetitionForm->isValid()) {
            $this->cumulativeCompetitionsRepository->save($editCompetition, true);
            return $this->redirectToRoute('manageCumulative_competitions', array_merge(
                $request->query->all(),
                [
                    'name' => $cumulativeGroupName,
            ]));
        }
        return $this->renderForm('admin/manage_cumulative/edit_competition.html.twig', [
            'editCompetition' => $editCompetitionForm
        ]);
    }

}
