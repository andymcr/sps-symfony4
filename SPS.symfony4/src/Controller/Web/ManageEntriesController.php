<?php
namespace App\Controller\Web;

use App\Entity\Competition;
use App\Entity\CompetitionSection;
use App\Entity\SectionEntry;
use App\Form\SectionEntryType;
use App\Repository\CompetitionSectionsRepository;
use App\Repository\CompetitionsRepository;
use App\Repository\SectionEntriesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageEntriesController extends AbstractController
{
    /**
     * @var CompetitionSectionsRepository $competitionSectionsRepository
     */
    private CompetitionSectionsRepository $competitionSectionsRepository;
    
    /**
     * @var CompetitionsRepository $competitionsRepository
     */
    private CompetitionsRepository $competitionsRepository;
    
    /**
     * @var SectionEntriesRepository $sectionEntriesRepository
     */
    private SectionEntriesRepository $sectionEntriesRepository;

    public function __construct(CompetitionSectionsRepository $competitionSectionsRepository, CompetitionsRepository $competitionsRepository, SectionEntriesRepository $sectionEntriesRepository)
    {
        $this->competitionSectionsRepository = $competitionSectionsRepository;
        $this->competitionsRepository = $competitionsRepository;
        $this->sectionEntriesRepository = $sectionEntriesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{eventTitle}/sections/{competitionSectionTitle}/section_Entries', name: 'manageEntries_sectionEntries')]
    public function sectionEntries(string $eventTitle, string $competitionSectionTitle): Response
    {
        $competitionSection = $this->competitionSectionsRepository->findOne($eventTitle, $competitionSectionTitle);
        if (is_null($competitionSection)) {
            throw $this->createNotFoundException('The CompetitionSection does not exist');
        }
        $sectionEntries = $this->sectionEntriesRepository->findOfSection($eventTitle, $competitionSectionTitle);
        return $this->render('admin/manage_entries/section_entries.html.twig', [
            'competitionSection' => $competitionSection,
            'sectionEntries' => $sectionEntries
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{eventTitle}/sections/{competitionSectionTitle}/section_Entries/{sectionEntryTitle}/delete', name: 'manageEntries_delete')]
    public function delete(string $eventTitle, string $competitionSectionTitle, string $sectionEntryTitle): Response
    {
        $sectionEntry = $this->sectionEntriesRepository->findOne($eventTitle, $competitionSectionTitle, $sectionEntryTitle);
        if (is_null($sectionEntry)) {
            throw $this->createNotFoundException('The SectionEntry does not exist');
        }
        $this->sectionEntriesRepository->remove($sectionEntry, true);
    
        return $this->redirectToRoute('manageEntries_sectionEntries', [
            'eventTitle' => $eventTitle,
            'competitionSectionTitle' => $competitionSectionTitle,
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{eventTitle}/sections/{competitionSectionTitle}/entries/create', name: 'manageEntries_createSectionEntry')]
    public function createSectionEntry(Request $request, string $eventTitle, string $competitionSectionTitle): Response
    {
        $editSectionEntry = new SectionEntry();
        $competitionSection = $this->competitionSectionsRepository->findOne($eventTitle, $competitionSectionTitle);
        if (is_null($competitionSection)) {
            throw $this->createNotFoundException('The CompetitionSection does not exist');
        }
        $editSectionEntry->setEnteredIn($competitionSection);
        $editSectionEntryForm = $this->createForm(SectionEntryType::class, $editSectionEntry, [
            'action' => $this->generateUrl('manageEntries_createSectionEntry', [
                'eventTitle' => $eventTitle,
                'competitionSectionTitle' => $competitionSectionTitle,
            ]),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageEntries_sectionEntries');
        }
        $editSectionEntryForm->handleRequest($request);
        if ($editSectionEntryForm->isSubmitted() && $editSectionEntryForm->isValid()) {
            $this->sectionEntriesRepository->save($editSectionEntry, true);
            return $this->redirectToRoute('manageEntries_sectionEntries', [
                'eventTitle' => $eventTitle,
                'competitionSectionTitle' => $competitionSectionTitle,
            ]);
        }
        return $this->renderForm('admin/manage_entries/edit_section_entry.html.twig', [
            'editSectionEntry' => $editSectionEntryForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{eventTitle}/sections/{competitionSectionTitle}/section_Entries/{sectionEntryTitle}/update', name: 'manageEntries_updateSectionEntry')]
    public function updateSectionEntry(Request $request, string $eventTitle, string $competitionSectionTitle, string $sectionEntryTitle): Response
    {
        $editSectionEntry = $this->sectionEntriesRepository->findOne($eventTitle, $competitionSectionTitle, $sectionEntryTitle);
        if (!$editSectionEntry) {
            throw $this->createNotFoundException('The SectionEntry does not exist');
        }
        $editSectionEntryForm = $this->createForm(SectionEntryType::class, $editSectionEntry, [
            'action' => $this->generateUrl('manageEntries_updateSectionEntry', [
                'eventTitle' => $eventTitle,
                'competitionSectionTitle' => $competitionSectionTitle,
                'sectionEntryTitle' => $sectionEntryTitle,
            ]),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageEntries_sectionEntries');
        }
        $editSectionEntryForm->handleRequest($request);
        if ($editSectionEntryForm->isSubmitted() && $editSectionEntryForm->isValid()) {
            $this->sectionEntriesRepository->save($editSectionEntry, true);
            return $this->redirectToRoute('manageEntries_sectionEntries', [
                'eventTitle' => $eventTitle,
                'competitionSectionTitle' => $competitionSectionTitle,
            ]);
        }
        return $this->renderForm('admin/manage_entries/edit_section_entry.html.twig', [
            'editSectionEntry' => $editSectionEntryForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/competitions/{title}/author_Entries', name: 'manageEntries')]
    public function authorEntries(string $title): Response
    {
        $competition = $this->competitionsRepository->findOne($title);
        if (is_null($competition)) {
            throw $this->createNotFoundException('The Competition does not exist');
        }
        $authorEntries = $this->sectionEntriesRepository->findAuthorsOfCompetition($title);
        return $this->render('admin/manage_entries/author_entries.html.twig', [
            'competition' => $competition,
            'authorEntries' => $authorEntries
        ]);
    }

}
