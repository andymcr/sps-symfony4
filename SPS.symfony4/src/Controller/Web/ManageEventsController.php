<?php
namespace App\Controller\Web;

use App\Entity\Event;
use App\Entity\Venue;
use App\Form\EventType;
use App\Form\VenueType;
use App\Repository\EventsRepository;
use App\Repository\VenuesRepository;
use App\Service\EventsService;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageEventsController extends AbstractController
{
    private const EVENTS_PAGE_SIZE = 12;
    
    private const VENUES_PAGE_SIZE = 10;

    /**
     * @var EventsRepository $eventsRepository
     */
    private EventsRepository $eventsRepository;
    
    /**
     * @var VenuesRepository $venuesRepository
     */
    private VenuesRepository $venuesRepository;

    public function __construct(EventsRepository $eventsRepository, VenuesRepository $venuesRepository)
    {
        $this->eventsRepository = $eventsRepository;
        $this->venuesRepository = $venuesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/events', name: 'manageEvents')]
    public function events(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->eventsRepository->findTitleLikeQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('eventTitle'),
            );
        else
            $query = $this->eventsRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $events = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::EVENTS_PAGE_SIZE);
        return $this->render('admin/manage_events/events.html.twig', [
            'events' => $events
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/events/{title}/delete', name: 'manageEvents_deleteEvent')]
    public function deleteEvent(Request $request, string $title): Response
    {
        $event = $this->eventsRepository->findOne($title);
        if (is_null($event)) {
            throw $this->createNotFoundException('The Event does not exist');
        }
        $this->eventsRepository->remove($event, true);
    
        return $this->redirectToRoute('manageEvents', $request->query->all());
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/events/syllabus', name: 'manageEvents_syllabus')]
    public function syllabus(EventsService $eventsService, Request $request, string $title): BinaryFileResponse
    {
        $file = $eventsService->syllabus();
        $response = $this->file($file, 'Syllabus.xlsx');
        return $response;
    }

    /**
     * @return BinaryFileResponse
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/events/syllabus_Next', name: 'manageEvents_syllabusNext')]
    public function syllabusNext(EventsService $eventsService, Request $request, string $title): BinaryFileResponse
    {
        $file = $eventsService->syllabusNext();
        $response = $this->file($file, 'SyllabusNext.xlsx');
        return $response;
    }

    /**
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/events/create', name: 'manageEvents_createEvent')]
    public function createEvent(Request $request): Response
    {
        $editEvent = new Event();
        $editEventForm = $this->createForm(EventType::class, $editEvent, [
            'action' => $this->generateUrl('manageEvents_createEvent', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageEvents');
        }
        $editEventForm->handleRequest($request);
        if ($editEventForm->isSubmitted() && $editEventForm->isValid()) {
            $this->eventsRepository->save($editEvent, true);
            return $this->redirectToRoute('manageEvents', $request->query->all());
        }
        return $this->renderForm('admin/manage_events/edit_event.html.twig', [
            'editEvent' => $editEventForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/events/{title}/update', name: 'manageEvents_updateEvent')]
    public function updateEvent(Request $request, string $title): Response
    {
        $editEvent = $this->eventsRepository->findOne($title);
        if (!$editEvent) {
            throw $this->createNotFoundException('The Event does not exist');
        }
        $editEventForm = $this->createForm(EventType::class, $editEvent, [
            'action' => $this->generateUrl('manageEvents_updateEvent', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageEvents');
        }
        $editEventForm->handleRequest($request);
        if ($editEventForm->isSubmitted() && $editEventForm->isValid()) {
            $this->eventsRepository->save($editEvent, true);
            return $this->redirectToRoute('manageEvents', $request->query->all());
        }
        return $this->renderForm('admin/manage_events/edit_event.html.twig', [
            'editEvent' => $editEventForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_VENUE_MANAGER')]
    #[Route('/admin/events/venues', name: 'manageEvents_venues')]
    public function venues(Request $request): Response
    {
        $query = $this->venuesRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $venues = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::VENUES_PAGE_SIZE);
        return $this->render('admin/manage_events/venues.html.twig', [
            'venues' => $venues
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_VENUE_MANAGER')]
    #[Route('/admin/events/venues/{name}/delete', name: 'manageEvents_deleteVenue')]
    public function deleteVenue(Request $request, string $name): Response
    {
        $venue = $this->venuesRepository->findOne($name);
        if (is_null($venue)) {
            throw $this->createNotFoundException('The Venue does not exist');
        }
        $this->venuesRepository->remove($venue, true);
    
        return $this->redirectToRoute('manageEvents_venues', $request->query->all());
    }

    

}
