<?php
namespace App\Controller\Web;

use App\Repository\ImageVariantsRepository;
use App\Repository\ImagesRepository;
use App\Repository\SectionEntriesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageImageEntriesController extends AbstractController
{
    /**
     * @var ImageVariantsRepository $imageVariantsRepository
     */
    private ImageVariantsRepository $imageVariantsRepository;
    
    /**
     * @var ImagesRepository $imagesRepository
     */
    private ImagesRepository $imagesRepository;
    
    /**
     * @var SectionEntriesRepository $sectionEntriesRepository
     */
    private SectionEntriesRepository $sectionEntriesRepository;

    public function __construct(ImageVariantsRepository $imageVariantsRepository, ImagesRepository $imagesRepository, SectionEntriesRepository $sectionEntriesRepository)
    {
        $this->imageVariantsRepository = $imageVariantsRepository;
        $this->imagesRepository = $imagesRepository;
        $this->sectionEntriesRepository = $sectionEntriesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/entries', name: 'manageImageEntries')]
    public function manageImageEntries(string $username, string $title): Response
    {
        $parameters = [];
        
        $authorLink = $this->imagesRepository->findOne($username, $title);
        if (is_null($authorLink)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $parameters['authorLink'] = $authorLink;
        
        $parameters['variants'] = $this->imageVariantsRepository->findVariantsOfImage($username, $title);
        
        $parameters['entries'] = $this->sectionEntriesRepository->findOfImage($username, $title);
        
        return $this->render('admin/manage_image_entries/index.html.twig', $parameters);
    }

}
