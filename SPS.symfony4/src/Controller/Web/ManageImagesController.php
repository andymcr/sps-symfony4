<?php
namespace App\Controller\Web;

use App\Repository\ImagesRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageImagesController extends AbstractController
{
    private const IMAGES_PAGE_SIZE = 12;

    /**
     * @var ImagesRepository $imagesRepository
     */
    private ImagesRepository $imagesRepository;

    public function __construct(ImagesRepository $imagesRepository)
    {
        $this->imagesRepository = $imagesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/images', name: 'manageImages')]
    public function images(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->imagesRepository->findWithTitleQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('title'),
            );
        else
            $query = $this->imagesRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $images = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::IMAGES_PAGE_SIZE);
        return $this->render('admin/manage_images/images.html.twig', [
            'images' => $images
        ]);
    }

}
