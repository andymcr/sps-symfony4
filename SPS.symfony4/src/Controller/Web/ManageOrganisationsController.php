<?php
namespace App\Controller\Web;

use App\Entity\Organisation;
use App\Form\OrganisationType;
use App\Repository\OrganisationsRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageOrganisationsController extends AbstractController
{
    private const ORGANISATIONS_PAGE_SIZE = 10;

    /**
     * @var OrganisationsRepository $organisationsRepository
     */
    private OrganisationsRepository $organisationsRepository;

    public function __construct(OrganisationsRepository $organisationsRepository)
    {
        $this->organisationsRepository = $organisationsRepository;
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/organisations', name: 'manageOrganisations')]
    public function organisations(Request $request): Response
    {
        if ($request->query->has('nameLikeFilter'))
            $query = $this->organisationsRepository->findWithNameQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('name'),
            );
        else
            $query = $this->organisationsRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $organisations = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::ORGANISATIONS_PAGE_SIZE);
        return $this->render('admin/manage_organisations/organisations.html.twig', [
            'organisations' => $organisations
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/organisations/{title}/delete', name: 'manageOrganisations_delete')]
    public function delete(Request $request, string $title): Response
    {
        $organisation = $this->organisationsRepository->findOne($title);
        if (is_null($organisation)) {
            throw $this->createNotFoundException('The Organisation does not exist');
        }
        $this->organisationsRepository->remove($organisation, true);
    
        return $this->redirectToRoute('manageOrganisations', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/organisations/create', name: 'manageOrganisations_create')]
    public function create(Request $request): Response
    {
        $edit = new Organisation();
        $editForm = $this->createForm(OrganisationType::class, $edit, [
            'action' => $this->generateUrl('manageOrganisations_create', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageOrganisations');
        }
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->organisationsRepository->save($edit, true);
            return $this->redirectToRoute('manageOrganisations', $request->query->all());
        }
        return $this->renderForm('admin/manage_organisations/edit.html.twig', [
            'edit' => $editForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/organisations/{title}/update', name: 'manageOrganisations_update')]
    public function update(Request $request, string $title): Response
    {
        $edit = $this->organisationsRepository->findOne($title);
        if (!$edit) {
            throw $this->createNotFoundException('The Organisation does not exist');
        }
        $editForm = $this->createForm(OrganisationType::class, $edit, [
            'action' => $this->generateUrl('manageOrganisations_update', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageOrganisations');
        }
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->organisationsRepository->save($edit, true);
            return $this->redirectToRoute('manageOrganisations', $request->query->all());
        }
        return $this->renderForm('admin/manage_organisations/edit.html.twig', [
            'edit' => $editForm
        ]);
    }

}
