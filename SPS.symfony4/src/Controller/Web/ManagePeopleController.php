<?php
namespace App\Controller\Web;

use App\Entity\Distinction;
use App\Entity\Image;
use App\Entity\ImageVariant;
use App\Entity\Person;
use App\Form\DistinctionType;
use App\Form\ImageType;
use App\Form\PersonType;
use App\Form\ImageVariantType;
use App\Repository\DistinctionsRepository;
use App\Repository\ImageVariantsRepository;
use App\Repository\ImagesRepository;
use App\Repository\PeopleRepository;
use App\Service\ImageVariantsService;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManagePeopleController extends AbstractController
{
    private const PEOPLE_PAGE_SIZE = 12;
    
    private const DISTINCTIONS_PAGE_SIZE = 12;
    
    private const IMAGES_PAGE_SIZE = 12;

    /**
     * @var DistinctionsRepository $distinctionsRepository
     */
    private DistinctionsRepository $distinctionsRepository;
    
    /**
     * @var ImageVariantsRepository $imageVariantsRepository
     */
    private ImageVariantsRepository $imageVariantsRepository;
    
    /**
     * @var ImagesRepository $imagesRepository
     */
    private ImagesRepository $imagesRepository;
    
    /**
     * @var PeopleRepository $peopleRepository
     */
    private PeopleRepository $peopleRepository;

    public function __construct(DistinctionsRepository $distinctionsRepository, ImageVariantsRepository $imageVariantsRepository, ImagesRepository $imagesRepository, PeopleRepository $peopleRepository)
    {
        $this->distinctionsRepository = $distinctionsRepository;
        $this->imageVariantsRepository = $imageVariantsRepository;
        $this->imagesRepository = $imagesRepository;
        $this->peopleRepository = $peopleRepository;
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people', name: 'managePeople')]
    public function people(Request $request): Response
    {
        if ($request->query->has('nameLikeFilter'))
            $query = $this->peopleRepository->findWithNameQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('forenames'),
                /* @phpstan-ignore-next-line */
                $request->query->get('familyName'),
            );
        else
            $query = $this->peopleRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $people = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::PEOPLE_PAGE_SIZE);
        return $this->render('admin/manage_people/people.html.twig', [
            'people' => $people
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/{username}/delete', name: 'managePeople_deletePerson')]
    public function deletePerson(Request $request, string $username): Response
    {
        $person = $this->peopleRepository->findOne($username);
        if (is_null($person)) {
            throw $this->createNotFoundException('The Person does not exist');
        }
        $this->peopleRepository->remove($person, true);
    
        return $this->redirectToRoute('managePeople', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/create', name: 'managePeople_createPerson')]
    public function createPerson(Request $request): Response
    {
        $editPerson = new Person();
        $editPersonForm = $this->createForm(PersonType::class, $editPerson, [
            'action' => $this->generateUrl('managePeople_createPerson', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople');
        }
        $editPersonForm->handleRequest($request);
        if ($editPersonForm->isSubmitted() && $editPersonForm->isValid()) {
            $this->peopleRepository->save($editPerson, true);
            return $this->redirectToRoute('managePeople', $request->query->all());
        }
        return $this->renderForm('admin/manage_people/edit_person.html.twig', [
            'editPerson' => $editPersonForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/{username}/update', name: 'managePeople_updatePerson')]
    public function updatePerson(Request $request, string $username): Response
    {
        $editPerson = $this->peopleRepository->findOne($username);
        if (!$editPerson) {
            throw $this->createNotFoundException('The Person does not exist');
        }
        $editPersonForm = $this->createForm(PersonType::class, $editPerson, [
            'action' => $this->generateUrl('managePeople_updatePerson', array_merge(
                $request->query->all(), [
                    'username' => $username,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople');
        }
        $editPersonForm->handleRequest($request);
        if ($editPersonForm->isSubmitted() && $editPersonForm->isValid()) {
            $this->peopleRepository->save($editPerson, true);
            return $this->redirectToRoute('managePeople', $request->query->all());
        }
        return $this->renderForm('admin/manage_people/edit_person.html.twig', [
            'editPerson' => $editPersonForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/{username}/participation', name: 'managePeople_participation')]
    public function participation(Request $request, string $username): Response
    {
        $participation = $this->peopleRepository->findOne($username);
        if (is_null($participation)) {
            throw $this->createNotFoundException('The Person does not exist');
        }
        return $this->render('admin/manage_people/participation.html.twig', [
            'participation' => $participation
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/distinctions', name: 'managePeople_distinctions')]
    public function distinctions(Request $request): Response
    {
        if ($request->query->has('nameLikeFilter'))
            $query = $this->distinctionsRepository->findWithNameQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('name'),
            );
        else
            $query = $this->distinctionsRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $distinctions = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::DISTINCTIONS_PAGE_SIZE);
        return $this->render('admin/manage_people/distinctions.html.twig', [
            'distinctions' => $distinctions
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/distinctions/{title}/delete', name: 'managePeople_deleteDistinction')]
    public function deleteDistinction(Request $request, string $title): Response
    {
        $distinction = $this->distinctionsRepository->findOne(urldecode($title));
        if (is_null($distinction)) {
            throw $this->createNotFoundException('The Distinction does not exist');
        }
        $this->distinctionsRepository->remove($distinction, true);
    
        return $this->redirectToRoute('managePeople_distinctions', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/distinctions/create', name: 'managePeople_createDistinction')]
    public function createDistinction(Request $request): Response
    {
        $editDistinction = new Distinction();
        $editDistinctionForm = $this->createForm(DistinctionType::class, $editDistinction, [
            'action' => $this->generateUrl('managePeople_createDistinction', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople_distinctions');
        }
        $editDistinctionForm->handleRequest($request);
        if ($editDistinctionForm->isSubmitted() && $editDistinctionForm->isValid()) {
            $this->distinctionsRepository->save($editDistinction, true);
            return $this->redirectToRoute('managePeople_distinctions', $request->query->all());
        }
        return $this->renderForm('admin/manage_people/edit_distinction.html.twig', [
            'editDistinction' => $editDistinctionForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_PEOPLE_MANAGER')]
    #[Route('/admin/people/distinctions/{title}/update', name: 'managePeople_updateDistinction')]
    public function updateDistinction(Request $request, string $title): Response
    {
        $editDistinction = $this->distinctionsRepository->findOne(urldecode($title));
        if (!$editDistinction) {
            throw $this->createNotFoundException('The Distinction does not exist');
        }
        $editDistinctionForm = $this->createForm(DistinctionType::class, $editDistinction, [
            'action' => $this->generateUrl('managePeople_updateDistinction', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople_distinctions');
        }
        $editDistinctionForm->handleRequest($request);
        if ($editDistinctionForm->isSubmitted() && $editDistinctionForm->isValid()) {
            $this->distinctionsRepository->save($editDistinction, true);
            return $this->redirectToRoute('managePeople_distinctions', $request->query->all());
        }
        return $this->renderForm('admin/manage_people/edit_distinction.html.twig', [
            'editDistinction' => $editDistinctionForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images', name: 'managePeople_images')]
    public function images(Request $request, string $username): Response
    {
        $person = $this->peopleRepository->findOne($username);
        if (is_null($person)) {
            throw $this->createNotFoundException('The Person does not exist');
        }
        if ($request->query->has('titleLikeFilter'))
            $query = $this->imagesRepository->findByPersonWithTitleQuery(
                $username, 
                /* @phpstan-ignore-next-line */
                $request->query->get('title'),
            );
        else
            $query = $this->imagesRepository->findByPersonQuery($username);
        $adapter = new QueryAdapter($query);
        $images = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::IMAGES_PAGE_SIZE);
        return $this->render('admin/manage_people/images.html.twig', [
            'person' => $person,
            'images' => $images
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/delete', name: 'managePeople_deleteImage')]
    public function deleteImage(Request $request, string $username, string $title): Response
    {
        $image = $this->imagesRepository->findOne($username, $title);
        if (is_null($image)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $this->imagesRepository->remove($image, true);
    
        return $this->redirectToRoute('managePeople_images', array_merge(
            $request->query->all(),
            [
                'username' => $username,
        ]));
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/create', name: 'managePeople_createImage')]
    public function createImage(Request $request, string $username): Response
    {
        $editImage = new Image();
        $person = $this->peopleRepository->findOne($username);
        if (is_null($person)) {
            throw $this->createNotFoundException('The Person does not exist');
        }
        $editImage->setAuthor($person);
        $editImageForm = $this->createForm(ImageType::class, $editImage, [
            'action' => $this->generateUrl('managePeople_createImage', array_merge(
                $request->query->all(), [
                    'username' => $username,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople_images');
        }
        $editImageForm->handleRequest($request);
        if ($editImageForm->isSubmitted() && $editImageForm->isValid()) {
            $this->imagesRepository->save($editImage, true);
            return $this->redirectToRoute('managePeople_images', array_merge(
                $request->query->all(),
                [
                    'username' => $username,
            ]));
        }
        return $this->renderForm('admin/manage_people/edit_image.html.twig', [
            'editImage' => $editImageForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/update', name: 'managePeople_updateImage')]
    public function updateImage(Request $request, string $username, string $title): Response
    {
        $editImage = $this->imagesRepository->findOne($username, $title);
        if (!$editImage) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $editImageForm = $this->createForm(ImageType::class, $editImage, [
            'action' => $this->generateUrl('managePeople_updateImage', array_merge(
                $request->query->all(), [
                    'username' => $username,
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople_images');
        }
        $editImageForm->handleRequest($request);
        if ($editImageForm->isSubmitted() && $editImageForm->isValid()) {
            $this->imagesRepository->save($editImage, true);
            return $this->redirectToRoute('managePeople_images', array_merge(
                $request->query->all(),
                [
                    'username' => $username,
            ]));
        }
        return $this->renderForm('admin/manage_people/edit_image.html.twig', [
            'editImage' => $editImageForm
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/variants', name: 'managePeople_variants')]
    public function variants(Request $request, string $username, string $title): Response
    {
        $image = $this->imagesRepository->findOne($username, $title);
        if (is_null($image)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $variants = $this->imageVariantsRepository->findVariantsOfImage($username, $title);
        return $this->render('admin/manage_people/variants.html.twig', [
            'image' => $image,
            'variants' => $variants
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/variants/{description}/delete', name: 'managePeople_deleteImageVariant')]
    public function deleteImageVariant(Request $request, string $username, string $title, string $description): Response
    {
        $imageVariant = $this->imageVariantsRepository->findOne($username, $title, $description);
        if (is_null($imageVariant)) {
            throw $this->createNotFoundException('The ImageVariant does not exist');
        }
        $this->imageVariantsRepository->remove($imageVariant, true);
    
        return $this->redirectToRoute('managePeople_variants', array_merge(
            $request->query->all(),
            [
                'username' => $username,
                'title' => $title,
        ]));
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT')]
    #[Route('/admin/people/{username}/images/{title}/variants/{description}/showcase', name: 'managePeople_showcase')]
    public function showcase(ImageVariantsService $imageVariantsService, Request $request, string $username, string $title, string $description): Response
    {
        $imageVariant = $this->imageVariantsRepository->findOne($username, $title, $description);
        if (is_null($imageVariant)) {
            throw $this->createNotFoundException('The ImageVariant does not exist');
        }
        $imageVariantsService->toggleClubShowcase($imageVariant);
        return $this->redirectToRoute('managePeople_variants', array_merge(
            $request->query->all(),
            [
                'username' => $username,
                'title' => $title,
        ]));
    }

    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/variants/create', name: 'managePeople_createVariants')]
    public function createVariants(Request $request, string $username, string $title): Response
    {
        $editVariants = new ImageVariant();
        $image = $this->imagesRepository->findOne($username, $title);
        if (is_null($image)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $editVariants->setImage($image);
        $editVariantsForm = $this->createForm(ImageVariantType::class, $editVariants, [
            'action' => $this->generateUrl('managePeople_createVariants', array_merge(
                $request->query->all(), [
                    'username' => $username,
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople_variants');
        }
        $editVariantsForm->handleRequest($request);
        if ($editVariantsForm->isSubmitted() && $editVariantsForm->isValid()) {
            $this->imageVariantsRepository->save($editVariants, true);
            return $this->redirectToRoute('managePeople_variants', array_merge(
                $request->query->all(),
                [
                    'username' => $username,
                    'title' => $title,
            ]));
        }
        return $this->renderForm('admin/manage_people/edit_variants.html.twig', [
            'editVariants' => $editVariantsForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_COMPETITION_SECRETARY')]
    #[Route('/admin/people/{username}/images/{title}/variants/{description}/update', name: 'managePeople_updateVariants')]
    public function updateVariants(Request $request, string $username, string $title, string $description): Response
    {
        $editVariants = $this->imageVariantsRepository->findOne($username, $title, $description);
        if (!$editVariants) {
            throw $this->createNotFoundException('The ImageVariant does not exist');
        }
        $editVariantsForm = $this->createForm(ImageVariantType::class, $editVariants, [
            'action' => $this->generateUrl('managePeople_updateVariants', array_merge(
                $request->query->all(), [
                    'username' => $username,
                    'title' => $title,
                    'description' => $description,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('managePeople_variants');
        }
        $editVariantsForm->handleRequest($request);
        if ($editVariantsForm->isSubmitted() && $editVariantsForm->isValid()) {
            $this->imageVariantsRepository->save($editVariants, true);
            return $this->redirectToRoute('managePeople_variants', array_merge(
                $request->query->all(),
                [
                    'username' => $username,
                    'title' => $title,
            ]));
        }
        return $this->renderForm('admin/manage_people/edit_variants.html.twig', [
            'editVariants' => $editVariantsForm
        ]);
    }

}
