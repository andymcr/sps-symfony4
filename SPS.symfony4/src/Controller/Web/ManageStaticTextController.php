<?php
namespace App\Controller\Web;

use App\Entity\StaticText;
use App\Form\StaticTextType;
use App\Repository\StaticTextRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageStaticTextController extends AbstractController
{
    private const TEXT_PAGE_SIZE = 10;

    /**
     * @var StaticTextRepository $staticTextRepository
     */
    private StaticTextRepository $staticTextRepository;

    public function __construct(StaticTextRepository $staticTextRepository)
    {
        $this->staticTextRepository = $staticTextRepository;
    }

    /**
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT_PLUS')]
    #[Route('/admin/statictext', name: 'manageStaticText')]
    public function text(Request $request): Response
    {
        $query = $this->staticTextRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $text = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::TEXT_PAGE_SIZE);
        return $this->render('admin/manage_static_text/text.html.twig', [
            'text' => $text
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT_PLUS')]
    #[Route('/admin/statictext/{name}/delete', name: 'manageStaticText_delete')]
    public function delete(Request $request, string $name): Response
    {
        $staticText = $this->staticTextRepository->findOne($name);
        if (is_null($staticText)) {
            throw $this->createNotFoundException('The StaticText does not exist');
        }
        $this->staticTextRepository->remove($staticText, true);
    
        return $this->redirectToRoute('manageStaticText', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT_PLUS')]
    #[Route('/admin/statictext/create', name: 'manageStaticText_create')]
    public function create(Request $request): Response
    {
        $edit = new StaticText();
        $editForm = $this->createForm(StaticTextType::class, $edit, [
            'action' => $this->generateUrl('manageStaticText_create', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageStaticText');
        }
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->staticTextRepository->save($edit, true);
            return $this->redirectToRoute('manageStaticText', $request->query->all());
        }
        return $this->renderForm('admin/manage_static_text/edit.html.twig', [
            'edit' => $editForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_PUBLIC_ENGAGEMENT_PLUS')]
    #[Route('/admin/statictext/{name}/update', name: 'manageStaticText_update')]
    public function update(Request $request, string $name): Response
    {
        $edit = $this->staticTextRepository->findOne($name);
        if (!$edit) {
            throw $this->createNotFoundException('The StaticText does not exist');
        }
        $editForm = $this->createForm(StaticTextType::class, $edit, [
            'action' => $this->generateUrl('manageStaticText_update', array_merge(
                $request->query->all(), [
                    'name' => $name,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageStaticText');
        }
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->staticTextRepository->save($edit, true);
            return $this->redirectToRoute('manageStaticText', $request->query->all());
        }
        return $this->renderForm('admin/manage_static_text/edit.html.twig', [
            'edit' => $editForm
        ]);
    }

}
