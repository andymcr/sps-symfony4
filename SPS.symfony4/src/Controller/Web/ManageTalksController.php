<?php
namespace App\Controller\Web;

use App\Entity\Talk;
use App\Form\TalkType;
use App\Repository\TalksRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ManageTalksController extends AbstractController
{
    private const TALKS_PAGE_SIZE = 10;

    /**
     * @var TalksRepository $talksRepository
     */
    private TalksRepository $talksRepository;

    public function __construct(TalksRepository $talksRepository)
    {
        $this->talksRepository = $talksRepository;
    }

    /**
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/talks', name: 'manageTalks')]
    public function talks(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->talksRepository->findWithTitleQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('title'),
            );
        else
            $query = $this->talksRepository->findAllQuery();
        $adapter = new QueryAdapter($query);
        $talks = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::TALKS_PAGE_SIZE);
        return $this->render('admin/manage_talks/talks.html.twig', [
            'talks' => $talks
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/talks/{title}/delete', name: 'manageTalks_delete')]
    public function delete(Request $request, string $title): Response
    {
        $talk = $this->talksRepository->findOne($title);
        if (is_null($talk)) {
            throw $this->createNotFoundException('The Talk does not exist');
        }
        $this->talksRepository->remove($talk, true);
    
        return $this->redirectToRoute('manageTalks', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/talks/create', name: 'manageTalks_create')]
    public function create(Request $request): Response
    {
        $edit = new Talk();
        $editForm = $this->createForm(TalkType::class, $edit, [
            'action' => $this->generateUrl('manageTalks_create', $request->query->all()),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageTalks');
        }
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->talksRepository->save($edit, true);
            return $this->redirectToRoute('manageTalks', $request->query->all());
        }
        return $this->renderForm('admin/manage_talks/edit.html.twig', [
            'edit' => $editForm
        ]);
    }
    
    /**
     */
    #[IsGranted('ROLE_EVENT_MANAGER')]
    #[Route('/admin/talks/{title}/update', name: 'manageTalks_update')]
    public function update(Request $request, string $title): Response
    {
        $edit = $this->talksRepository->findOne($title);
        if (!$edit) {
            throw $this->createNotFoundException('The Talk does not exist');
        }
        $editForm = $this->createForm(TalkType::class, $edit, [
            'action' => $this->generateUrl('manageTalks_update', array_merge(
                $request->query->all(), [
                    'title' => $title,
            ])),
        ]);
        if ($request->isMethod('POST') && $request->request->has('cancel')) {
        	return $this->redirectToRoute('manageTalks');
        }
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->talksRepository->save($edit, true);
            return $this->redirectToRoute('manageTalks', $request->query->all());
        }
        return $this->renderForm('admin/manage_talks/edit.html.twig', [
            'edit' => $editForm
        ]);
    }

}
