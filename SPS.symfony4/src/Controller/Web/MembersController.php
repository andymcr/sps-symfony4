<?php
namespace App\Controller\Web;

use App\Repository\PeopleRepository;
use App\Repository\StaticTextRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MembersController extends AbstractController
{
    private const JUDGES_PAGE_SIZE = 10;

    /**
     * @var PeopleRepository $peopleRepository
     */
    private PeopleRepository $peopleRepository;
    
    /**
     * @var StaticTextRepository $staticTextRepository
     */
    private StaticTextRepository $staticTextRepository;

    public function __construct(PeopleRepository $peopleRepository, StaticTextRepository $staticTextRepository)
    {
        $this->peopleRepository = $peopleRepository;
        $this->staticTextRepository = $staticTextRepository;
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/members/judges', name: 'members')]
    public function judges(Request $request): Response
    {
        if ($request->query->has('nameLikeFilter'))
            $query = $this->peopleRepository->findJudgeWithNameQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('forenames'),
                /* @phpstan-ignore-next-line */
                $request->query->get('familyName'),
            );
        else
            $query = $this->peopleRepository->findJudgesQuery();
        $adapter = new QueryAdapter($query);
        $judges = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::JUDGES_PAGE_SIZE);
        return $this->render('members/judges.html.twig', [
            'judges' => $judges
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/members/committee', name: 'members_committee')]
    public function committee(Request $request): Response
    {
        $committee = $this->staticTextRepository->findCommitteeText();
        if (is_null($committee)) {
            throw $this->createNotFoundException('The StaticText does not exist');
        }
        return $this->render('members/committee.html.twig', [
            'committee' => $committee
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/members/mentors', name: 'members_mentors')]
    public function mentors(Request $request): Response
    {
        $mentors = $this->staticTextRepository->findMentorsText();
        if (is_null($mentors)) {
            throw $this->createNotFoundException('The StaticText does not exist');
        }
        return $this->render('members/mentors.html.twig', [
            'mentors' => $mentors
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/members/noticeboard', name: 'members_noticeboard')]
    public function noticeboard()
    {
        return $this->render('members/noticeboard.html.twig');
    }

}
