<?php
namespace App\Controller\Web;

use App\Entity\SectionEntry;
use App\Repository\SectionEntriesRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MyEntriesController extends AbstractController
{
    private const SPS_PAGE_SIZE = 12;
    
    private const LCPU_PAGE_SIZE = 12;
    
    private const BPE_PAGE_SIZE = 12;

    /**
     * @var SectionEntriesRepository $sectionEntriesRepository
     */
    private SectionEntriesRepository $sectionEntriesRepository;

    public function __construct(SectionEntriesRepository $sectionEntriesRepository)
    {
        $this->sectionEntriesRepository = $sectionEntriesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER')]
    #[Route('/me/entries/sps', name: 'myEntries')]
    public function sps(Request $request): Response
    {
        if ($request->query->has('imageTitleLikeFilter'))
            $query = $this->sectionEntriesRepository->findImageTitleLikeSPSQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('imageTitle'),
            );
        elseif ($request->query->has('entryTitleLikeFilter'))
            $query = $this->sectionEntriesRepository->findEntryTitleLikeSPSQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('entryTitle'),
            );
        else
            $query = $this->sectionEntriesRepository->findMySpsEntriesQuery();
        $adapter = new QueryAdapter($query);
        $sps = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::SPS_PAGE_SIZE);
        return $this->render('me/my_entries/sps.html.twig', [
            'sps' => $sps
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER')]
    #[Route('/me/entries/lcpu', name: 'myEntries_lcpu')]
    public function lcpu(Request $request): Response
    {
        if ($request->query->has('entryTitleLikeFilter'))
            $query = $this->sectionEntriesRepository->findEntryTitleLikeLCPUQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('entryTitle'),
            );
        elseif ($request->query->has('imageTitleLikeFilter'))
            $query = $this->sectionEntriesRepository->findImageTitleLikeLCPUQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('imageTitle'),
            );
        else
            $query = $this->sectionEntriesRepository->findMyLCPUEntriesQuery();
        $adapter = new QueryAdapter($query);
        $lcpu = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::LCPU_PAGE_SIZE);
        return $this->render('me/my_entries/lcpu.html.twig', [
            'lcpu' => $lcpu
        ]);
    }

    /**
     */
    #[Route('/me/entries/bpe', name: 'myEntries_bpe')]
    public function bpe(Request $request): Response
    {
        $query = $this->sectionEntriesRepository->findMyBpeEntriesQuery();
        $adapter = new QueryAdapter($query);
        $bpe = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::BPE_PAGE_SIZE);
        return $this->render('me/my_entries/bpe.html.twig', [
            'bpe' => $bpe
        ]);
    }

    /**
     */
    #[IsGranted('ROLE_BPE_ACTIVE')]
    #[Route('/me/entries/bpe_By_Date', name: 'myEntries_bpeByDate')]
    public function bpeByDate(Request $request): Response
    {
        $bpeByDate = $this->sectionEntriesRepository->findMyBpeEntriesByExhibition();
        return $this->render('me/my_entries/bpe_by_date.html.twig', [
            'bpeByDate' => $bpeByDate
        ]);
    }

}
