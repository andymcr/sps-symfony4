<?php
namespace App\Controller\Web;

use App\Repository\ImageVariantsRepository;
use App\Repository\ImagesRepository;
use App\Repository\SectionEntriesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MyImageEntriesController extends AbstractController
{
    /**
     * @var ImageVariantsRepository $imageVariantsRepository
     */
    private ImageVariantsRepository $imageVariantsRepository;
    
    /**
     * @var ImagesRepository $imagesRepository
     */
    private ImagesRepository $imagesRepository;
    
    /**
     * @var SectionEntriesRepository $sectionEntriesRepository
     */
    private SectionEntriesRepository $sectionEntriesRepository;

    public function __construct(ImageVariantsRepository $imageVariantsRepository, ImagesRepository $imagesRepository, SectionEntriesRepository $sectionEntriesRepository)
    {
        $this->imageVariantsRepository = $imageVariantsRepository;
        $this->imagesRepository = $imagesRepository;
        $this->sectionEntriesRepository = $sectionEntriesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER')]
    #[Route('/me/images/{title}/entries', name: 'myImageEntries')]
    public function myImageEntries(string $title): Response
    {
        $parameters = [];
        
        $image = $this->imagesRepository->findMyImage($title);
        if (is_null($image)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $parameters['image'] = $image;
        if (!(count($image->getVariants()) < 2)) {
            $parameters['variants'] = $this->imageVariantsRepository->findMyVariants($title);
        }
        
        $variants = $this->imagesRepository->findMyImage($title);
        if (is_null($variants)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $parameters['variants'] = $variants;
        $parameters['entries'] = $this->sectionEntriesRepository->findMyImageEntries($title);
        
        return $this->render('me/my_image_entries/index.html.twig', $parameters);
    }

}
