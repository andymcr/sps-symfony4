<?php
namespace App\Controller\Web;

use App\Repository\ImagesRepository;
use App\Service\ImagesService;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MyImagesController extends AbstractController
{
    private const IMAGES_PAGE_SIZE = 12;
    
    private const WITHOUTPICTURE_PAGE_SIZE = 12;

    /**
     * @var ImagesRepository $imagesRepository
     */
    private ImagesRepository $imagesRepository;

    public function __construct(ImagesRepository $imagesRepository)
    {
        $this->imagesRepository = $imagesRepository;
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER')]
    #[Route('/me/images', name: 'myImages')]
    public function images(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->imagesRepository->findMineWithTitleQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('title'),
            );
        else
            $query = $this->imagesRepository->findMyImagesQuery();
        $adapter = new QueryAdapter($query);
        $images = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::IMAGES_PAGE_SIZE);
        return $this->render('me/my_images/images.html.twig', [
            'images' => $images
        ]);
    }

    /**
     * @return Response
     */
    #[IsGranted('ROLE_MEMBER_PLUS')]
    #[Route('/me/images/{title}/showcase', name: 'myImages_showcase')]
    public function showcase(ImagesService $imagesService, Request $request, string $title): Response
    {
        $image = $this->imagesRepository->findMyImage($title);
        if (is_null($image)) {
            throw $this->createNotFoundException('The Image does not exist');
        }
        $imagesService->togglePersonalShowcase($image);
        return $this->redirectToRoute('myImages', $request->query->all());
    }

    /**
     */
    #[IsGranted('ROLE_MEMBER')]
    #[Route('/me/images/without_Picture', name: 'myImages_withoutPicture')]
    public function withoutPicture(Request $request): Response
    {
        if ($request->query->has('titleLikeFilter'))
            $query = $this->imagesRepository->findMineWithTitleQuery(
                /* @phpstan-ignore-next-line */
                $request->query->get('title'),
            );
        else
            $query = $this->imagesRepository->findMyImagesWithoutPicturesQuery();
        $adapter = new QueryAdapter($query);
        $withoutPicture = Pagerfanta::createForCurrentPageWithMaxPerPage(
            $adapter,
            $request->query->getInt('page', 1),
            self::WITHOUTPICTURE_PAGE_SIZE);
        return $this->render('me/my_images/without_picture.html.twig', [
            'withoutPicture' => $withoutPicture
        ]);
    }

}
