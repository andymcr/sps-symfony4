<?php
namespace App\Entity;

use App\Repository\AuthenticationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: AuthenticationRepository::class)]
#[ORM\Table(name: "authentications")]
#[UniqueEntity("username")]
#[UniqueEntity("email")]
class Authentication implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var array<int, string> $roles
     */
    #[ORM\Column(type: "array")]
    protected array $roles = array();

    /**
     * @var string $password
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $password = '';

    /**
     * @var string $username
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $username = '';
    
    /**
     * @var bool $enabled
     */
    #[ORM\Column(type: "boolean")]
    #[Assert\NotBlank]
    protected bool $enabled;
    
    /**
     * @var string $email
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $email = '';
    
    /**
     * @var bool $emailVerified
     */
    #[ORM\Column(name: "email_verified", type: "boolean")]
    #[Assert\NotBlank]
    protected bool $emailVerified;
    
    /**
     * @var bool $localed
     */
    #[ORM\Column(name: "locked", type: "boolean")]
    #[Assert\NotBlank]
    protected bool $localed;
    
    /**
     * @var bool $expired
     */
    #[ORM\Column(type: "boolean")]
    #[Assert\NotBlank]
    protected bool $expired;
    
    /**
     * @var \DateTimeInterface $expiresAt
     */
    #[ORM\Column(name: "expires_at", type: "datetime")]
    #[Assert\NotBlank]
    protected \DateTimeInterface $expiresAt;
    
    /**
     * @var \DateTimeInterface $passwordChangedAt
     */
    #[ORM\Column(name: "password_changed_at", type: "datetime")]
    #[Assert\NotBlank]
    protected \DateTimeInterface $passwordChangedAt;
    
    /**
     * @var bool $credentialsExpired
     */
    #[ORM\Column(name: "credentials_expired", type: "boolean")]
    #[Assert\NotBlank]
    protected bool $credentialsExpired = false;
    
    /**
     * @var \DateTimeInterface $credentialsExpireAt
     */
    #[ORM\Column(name: "credentials_expire_at", type: "datetime")]
    #[Assert\NotBlank]
    protected \DateTimeInterface $credentialsExpireAt;
    
    /**
     * @var \DateTimeInterface $lastLogin
     */
    #[ORM\Column(name: "last_login", type: "datetime")]
    #[Assert\NotBlank]
    protected \DateTimeInterface $lastLogin;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->enabled = true;
        $this->emailVerified = false;
        $this->localed = false;
        $this->expired = false;
        $this->expiresAt = new \DateTime('00:00:00');
        $this->passwordChangedAt = new \DateTime('00:00:00');
        $this->credentialsExpireAt = new \DateTime('00:00:00');
        $this->lastLogin = new \DateTime('0000-00-00 00:00:00');
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->getEmail();
    }
    
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }
    
        return $roles;
    }
    
        /**
         *
         *
         * @param string $role
         */
        public function addRole(string $role): self
        {
            $this->roles[] = $role;
            return $this;
        }
        
        /**
         *
         *
         * @param string $role
         */
        public function removeRole(string $role): self
        {
            unset($this->roles[array_search($role, $this->roles)]);
            return $this;
        }
    
    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    { 
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    { 
        return (string) $this->password; 
    } 
    
    public function setPassword(string $password): self
    {
        $this->password = $password;
        $this->setPasswordChangedAt(new \DateTimeImmutable());
        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
    
    /**
     *
     *
     * @param string $username
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
    
    /**
     *
     *
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    /**
     *
     *
     * @param string $email
     */
    public function setEmail(string $email): self
    {
        $this->email = strtolower($email);
        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function isEmailVerified(): bool
    {
        return $this->emailVerified;
    }
    
    /**
     *
     *
     * @param bool $emailVerified
     */
    public function setEmailVerified(bool $emailVerified): self
    {
        $this->emailVerified = $emailVerified;
        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function isLocaled(): bool
    {
        return $this->localed;
    }
    
    /**
     *
     *
     * @param bool $localed
     */
    public function setLocaled(bool $localed): self
    {
        $this->localed = $localed;
        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->expired;
    }
    
    /**
     *
     *
     * @param bool $expired
     */
    public function setExpired(bool $expired): self
    {
        $this->expired = $expired;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface
     */
    public function getExpiresAt(): \DateTimeInterface
    {
        return $this->expiresAt;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface $expiresAt
     */
    public function setExpiresAt(\DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface
     */
    public function getPasswordChangedAt(): \DateTimeInterface
    {
        return $this->passwordChangedAt;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface $passwordChangedAt
     */
    public function setPasswordChangedAt(\DateTimeInterface $passwordChangedAt): self
    {
        $this->passwordChangedAt = $passwordChangedAt;
        return $this;
    }

    /**
     *
     *
     * @return bool
     */
    public function isCredentialsExpired(): bool
    {
        return $this->credentialsExpired;
    }
    
    /**
     *
     *
     * @param bool $credentialsExpired
     */
    public function setCredentialsExpired(bool $credentialsExpired): self
    {
        $this->credentialsExpired = $credentialsExpired;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface
     */
    public function getCredentialsExpireAt(): \DateTimeInterface
    {
        return $this->credentialsExpireAt;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface $credentialsExpireAt
     */
    public function setCredentialsExpireAt(\DateTimeInterface $credentialsExpireAt): self
    {
        $this->credentialsExpireAt = $credentialsExpireAt;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface
     */
    public function getLastLogin(): \DateTimeInterface
    {
        return $this->lastLogin;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface $lastLogin
     */
    public function setLastLogin(\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->email);
    }

}
