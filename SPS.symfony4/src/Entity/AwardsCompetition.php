<?php
namespace App\Entity;

class AwardsCompetition
{
	const LandscapeWinner = 1;

    /**
     *
     *
     * @return array<string, int>
     */
	public static function values(): array
	{
		$values = [];
		$values['Landscape Winner'] = self::LandscapeWinner;

		return $values;
	}

    /**
     *
     * @param string $value
     * @param string $default
     *
     * @return string
     */
	public static function value(string $value, string $default = ''): string
	{
		$values = array_flip(AwardsCompetition::values());

		if (array_key_exists($value, $values)) {
			return $values[$value];
		}

		return $default;
	}

}
