<?php
namespace App\Entity;

use App\Repository\CompetitionsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CompetitionsRepository::class)]
#[ORM\Table(name: "competitions")]
class Competition
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string|null $website
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $website = null;
    
    /**
     * @var int|null $maximumScore
     */
    #[ORM\Column(name: "maximum_score", type: "integer", nullable: true)]
    protected ?int $maximumScore = 0;
    
    /**
     * @var bool|null $publicAuthors
     */
    #[ORM\Column(name: "public_authors", type: "boolean", nullable: true)]
    protected ?bool $publicAuthors = false;
    
    /**
     * @var bool|null $publicResults
     */
    #[ORM\Column(name: "public_results", type: "boolean", nullable: true)]
    protected ?bool $publicResults = false;
    
    /**
     * @var \DateTimeInterface|null $submissionDeadline
     */
    #[ORM\Column(name: "submission_deadline", type: "date", nullable: true)]
    protected ?\DateTimeInterface $submissionDeadline;
    
    /**
     * @var Person|null $secretary
     */
    #[ORM\ManyToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(name: "secretary_id", referencedColumnName: "id")]
    protected ?Person $secretary = null;
    
    /**
     * @var CompetitionCategory $category
     */
    #[ORM\ManyToOne(targetEntity: CompetitionCategory::class)]
    #[ORM\JoinColumn(name: "category_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected CompetitionCategory $category;
    
    /**
     * @var ImageVariant|null $image
     */
    #[ORM\ManyToOne(targetEntity: ImageVariant::class)]
    #[ORM\JoinColumn(name: "image_id", referencedColumnName: "id")]
    protected ?ImageVariant $image = null;
    
    /**
     * @var Collection<int, Person>&Selectable<int, Person> $judges
     */
    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: "hasJudged")]
    #[ORM\JoinTable(name: "competitions_people")]
    #[ORM\JoinColumn(name: "has_judged_id", nullable: false)]
    #[ORM\InverseJoinColumn(name: "judges_id", nullable: false)]
    protected Collection $judges;
    
    /**
     * @var Event $event
     */
    #[ORM\OneToOne(targetEntity: Event::class)]
    #[ORM\JoinColumn(name: "event_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Event $event;
    
    /**
     * @var Collection<int, CompetitionSection>&Selectable<int, CompetitionSection> $sections
     */
    #[ORM\OneToMany(targetEntity: CompetitionSection::class, mappedBy: "partOf", cascade: ["persist", "remove"])]
    #[Assert\Unique(fields: ['title'])]
    protected Collection $sections;
    
    /**
     * @var Collection<int, CompetitionAward>&Selectable<int, CompetitionAward> $awards
     */
    #[ORM\OneToMany(targetEntity: CompetitionAward::class, mappedBy: "awardFor", cascade: ["persist", "remove"])]
    #[Assert\Unique(fields: ['award'])]
    protected Collection $awards;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->submissionDeadline = new \DateTime('00:00:00');
        $this->judges = new ArrayCollection();
        $this->sections = new ArrayCollection();
        $this->awards = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }
    
    /**
     *
     *
     * @param string|null $website
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getMaximumScore(): ?int
    {
        return $this->maximumScore;
    }
    
    /**
     *
     *
     * @param int|null $maximumScore
     */
    public function setMaximumScore(?int $maximumScore): self
    {
        $this->maximumScore = $maximumScore;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPublicAuthors(): ?bool
    {
        return $this->publicAuthors;
    }
    
    /**
     *
     *
     * @param bool|null $publicAuthors
     */
    public function setPublicAuthors(?bool $publicAuthors): self
    {
        $this->publicAuthors = $publicAuthors;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPublicResults(): ?bool
    {
        return $this->publicResults;
    }
    
    /**
     *
     *
     * @param bool|null $publicResults
     */
    public function setPublicResults(?bool $publicResults): self
    {
        $this->publicResults = $publicResults;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface|null
     */
    public function getSubmissionDeadline(): ?\DateTimeInterface
    {
        return $this->submissionDeadline;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface|null $submissionDeadline
     */
    public function setSubmissionDeadline(?\DateTimeInterface $submissionDeadline): self
    {
        $this->submissionDeadline = $submissionDeadline;
        return $this;
    }

    /**
     *
     *
     * @return Person|null
     */
    public function getSecretary(): ?Person
    {
        return $this->secretary;
    }
    
    /**
     *
     *
     * @param Person $secretary
     */
    public function setSecretary(Person $secretary): self
    {
        $this->secretary = $secretary;
        return $this;
    }

    /**
     *
     *
     * @return CompetitionCategory
     */
    public function getCategory(): CompetitionCategory
    {
        return $this->category;
    }
    
    /**
     *
     *
     * @param CompetitionCategory $category
     */
    public function setCategory(CompetitionCategory $category): self
    {
        $this->category = $category;
        return $this;
    }

    /**
     *
     *
     * @return ImageVariant|null
     */
    public function getImage(): ?ImageVariant
    {
        return $this->image;
    }
    
    /**
     *
     *
     * @param ImageVariant $image
     */
    public function setImage(ImageVariant $image): self
    {
        $this->image = $image;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, Person>&Selectable<int, Person>
     */
    public function getJudges(): Collection
    {
        return $this->judges;
    }

    /**
     *
     *
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }
    
    /**
     *
     *
     * @param Event $event
     */
    public function setEvent(Event $event): self
    {
        $this->event = $event;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, CompetitionSection>&Selectable<int, CompetitionSection>
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    /**
     *
     *
     * @return Collection<int, CompetitionAward>&Selectable<int, CompetitionAward>
     */
    public function getAwards(): Collection
    {
        return $this->awards;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->event->getDefaultLabel());
    }

}
