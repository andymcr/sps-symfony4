<?php
namespace App\Entity;

use App\Repository\CompetitionAwardsRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CompetitionAwardsRepository::class)]
#[ORM\Table(name: "competition_awards")]
class CompetitionAward
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var int $award
     */
    #[ORM\Column(type: "integer")]
    #[Assert\NotBlank]
    protected int $award = 0;
    
    /**
     * @var SectionEntry $entry
     */
    #[ORM\ManyToOne(targetEntity: SectionEntry::class)]
    #[ORM\JoinColumn(name: "entry_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected SectionEntry $entry;
    
    /**
     * @var Competition $awardFor
     */
    #[ORM\ManyToOne(targetEntity: Competition::class, inversedBy: "awards")]
    #[ORM\JoinColumn(name: "award_for_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Competition $awardFor;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return int
     */
    public function getAward(): int
    {
        return $this->award;
    }
    
    /**
     *
     *
     * @param int $award
     */
    public function setAward(int $award): self
    {
        $this->award = $award;
        return $this;
    }

    /**
     *
     *
     * @return SectionEntry
     */
    public function getEntry(): SectionEntry
    {
        return $this->entry;
    }
    
    /**
     *
     *
     * @param SectionEntry $entry
     */
    public function setEntry(SectionEntry $entry): self
    {
        $this->entry = $entry;
        return $this;
    }

    /**
     *
     *
     * @return Competition
     */
    public function getAwardFor(): Competition
    {
        return $this->awardFor;
    }
    
    /**
     *
     *
     * @param Competition $awardFor
     */
    public function setAwardFor(Competition $awardFor): self
    {
        $this->awardFor = $awardFor;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->award);
    }

    /**
     *
     * @return string
     */
    public function getCompetitionLabel(): String
    {
        return sprintf('', );
    }

}
