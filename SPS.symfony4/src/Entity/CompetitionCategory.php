<?php
namespace App\Entity;

use App\Repository\CompetitionCategoriesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CompetitionCategoriesRepository::class)]
#[ORM\Table(name: "competitioncategory")]
#[UniqueEntity("shortName")]
#[UniqueEntity("fullName")]
class CompetitionCategory
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $shortName
     */
    #[ORM\Column(name: "short_name", type: "string")]
    #[Assert\NotBlank]
    protected string $shortName = '';
    
    /**
     * @var string $fullName
     */
    #[ORM\Column(name: "full_name", type: "string")]
    #[Assert\NotBlank]
    protected string $fullName = '';
    
    /**
     * @var int|null $rank
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $rank = 0;
    
    /**
     * @var CompetitionCategory|null $parent
     */
    #[ORM\ManyToOne(targetEntity: CompetitionCategory::class, inversedBy: "subcategories")]
    #[ORM\JoinColumn(name: "parent_id", referencedColumnName: "id")]
    protected ?CompetitionCategory $parent = null;
    
    /**
     * @var Collection<int, CompetitionCategory>&Selectable<int, CompetitionCategory> $subcategories
     */
    #[ORM\OneToMany(targetEntity: CompetitionCategory::class, mappedBy: "parent")]
    protected Collection $subcategories;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->subcategories = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }
    
    /**
     *
     *
     * @param string $shortName
     */
    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }
    
    /**
     *
     *
     * @param string $fullName
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getRank(): ?int
    {
        return $this->rank;
    }
    
    /**
     *
     *
     * @param int|null $rank
     */
    public function setRank(?int $rank): self
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     *
     *
     * @return CompetitionCategory|null
     */
    public function getParent(): ?CompetitionCategory
    {
        return $this->parent;
    }
    
    /**
     *
     *
     * @param CompetitionCategory $parent
     */
    public function setParent(CompetitionCategory $parent): self
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, CompetitionCategory>&Selectable<int, CompetitionCategory>
     */
    public function getSubcategories(): Collection
    {
        return $this->subcategories;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->shortName);
    }

}
