<?php
namespace App\Entity;

use App\Repository\CompetitionSectionsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CompetitionSectionsRepository::class)]
#[ORM\Table(name: "competition_sections")]
class CompetitionSection
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $title
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $title = '';
    
    /**
     * @var int|null $order
     */
    #[ORM\Column(name: "`order`", type: "integer", nullable: true)]
    protected ?int $order = 0;
    
    /**
     * @var int|null $maximumEntries
     */
    #[ORM\Column(name: "maximum_entries", type: "integer", nullable: true)]
    protected ?int $maximumEntries = 0;
    
    /**
     * @var int|null $acceptanceScore
     */
    #[ORM\Column(name: "acceptance_score", type: "integer", nullable: true)]
    protected ?int $acceptanceScore = 0;
    
    /**
     * @var Collection<int, CumulativeCompetition>&Selectable<int, CumulativeCompetition> $contributesTo
     */
    #[ORM\ManyToMany(targetEntity: CumulativeCompetition::class, mappedBy: "fromSections")]
    protected Collection $contributesTo;
    
    /**
     * @var Competition $partOf
     */
    #[ORM\ManyToOne(targetEntity: Competition::class, inversedBy: "sections")]
    #[ORM\JoinColumn(name: "part_of_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Competition $partOf;
    
    /**
     * @var Collection<int, SectionEntry>&Selectable<int, SectionEntry> $entries
     */
    #[ORM\OneToMany(targetEntity: SectionEntry::class, mappedBy: "enteredIn", cascade: ["persist", "remove"])]
    #[Assert\Unique(fields: ['title'])]
    protected Collection $entries;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->contributesTo = new ArrayCollection();
        $this->entries = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    
    /**
     *
     *
     * @param string $title
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getOrder(): ?int
    {
        return $this->order;
    }
    
    /**
     *
     *
     * @param int|null $order
     */
    public function setOrder(?int $order): self
    {
        $this->order = $order;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getMaximumEntries(): ?int
    {
        return $this->maximumEntries;
    }
    
    /**
     *
     *
     * @param int|null $maximumEntries
     */
    public function setMaximumEntries(?int $maximumEntries): self
    {
        $this->maximumEntries = $maximumEntries;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getAcceptanceScore(): ?int
    {
        return $this->acceptanceScore;
    }
    
    /**
     *
     *
     * @param int|null $acceptanceScore
     */
    public function setAcceptanceScore(?int $acceptanceScore): self
    {
        $this->acceptanceScore = $acceptanceScore;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, CumulativeCompetition>&Selectable<int, CumulativeCompetition>
     */
    public function getContributesTo(): Collection
    {
        return $this->contributesTo;
    }

    /**
     *
     *
     * @return Competition
     */
    public function getPartOf(): Competition
    {
        return $this->partOf;
    }
    
    /**
     *
     *
     * @param Competition $partOf
     */
    public function setPartOf(Competition $partOf): self
    {
        $this->partOf = $partOf;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, SectionEntry>&Selectable<int, SectionEntry>
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getCompetitionAndSectionLabel(): String
    {
        return sprintf('%s - %s', $this->partOf->getDefaultLabel(), $this->title);
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->title);
    }

}
