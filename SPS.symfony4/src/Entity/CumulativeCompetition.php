<?php
namespace App\Entity;

use App\Repository\CumulativeCompetitionsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CumulativeCompetitionsRepository::class)]
#[ORM\Table(name: "cumulativecompetitions")]
#[UniqueEntity("name")]
class CumulativeCompetition
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $name
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $name = '';
    
    /**
     * @var bool|null $private
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $private;
    
    /**
     * @var Collection<int, CompetitionSection>&Selectable<int, CompetitionSection> $fromSections
     */
    #[ORM\ManyToMany(targetEntity: CompetitionSection::class, inversedBy: "contributesTo")]
    #[ORM\JoinTable(name: "competitionsection_pointsaccumulation")]
    #[ORM\JoinColumn(name: "contributes_to_id", nullable: false)]
    #[ORM\InverseJoinColumn(name: "from_sections_id", nullable: false)]
    protected Collection $fromSections;
    
    /**
     * @var CumulativeGroup $partOf
     */
    #[ORM\ManyToOne(targetEntity: CumulativeGroup::class, inversedBy: "competitions")]
    #[ORM\JoinColumn(name: "part_of_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected CumulativeGroup $partOf;
    
    /**
     * @var Collection<int, CumulativeEntry>&Selectable<int, CumulativeEntry> $scores
     */
    #[ORM\OneToMany(targetEntity: CumulativeEntry::class, mappedBy: "partOf", cascade: ["persist", "remove"])]
    protected Collection $scores;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->private = false;
        $this->fromSections = new ArrayCollection();
        $this->scores = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     *
     *
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPrivate(): ?bool
    {
        return $this->private;
    }
    
    /**
     *
     *
     * @param bool|null $private
     */
    public function setPrivate(?bool $private): self
    {
        $this->private = $private;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, CompetitionSection>&Selectable<int, CompetitionSection>
     */
    public function getFromSections(): Collection
    {
        return $this->fromSections;
    }

    /**
     *
     *
     * @return CumulativeGroup
     */
    public function getPartOf(): CumulativeGroup
    {
        return $this->partOf;
    }
    
    /**
     *
     *
     * @param CumulativeGroup $partOf
     */
    public function setPartOf(CumulativeGroup $partOf): self
    {
        $this->partOf = $partOf;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, CumulativeEntry>&Selectable<int, CumulativeEntry>
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }
    
    /**
     *
     *
     * @param CumulativeEntry $Score
     */
    public function addScore(CumulativeEntry $Score): self
    {
        $this->scores->add($Score);
        $Score->setPartOf($this);
        return $this;
    }
    
    /**
     *
     *
     * @param CumulativeEntry $Score
     */
    public function removeScore(CumulativeEntry $Score): self
    {
        $this->scores->removeElement($Score);
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->name);
    }

}
