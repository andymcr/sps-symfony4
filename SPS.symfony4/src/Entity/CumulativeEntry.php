<?php
namespace App\Entity;

use App\Repository\CumulativeEntriesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CumulativeEntriesRepository::class)]
#[ORM\Table(name: "cumulativeentries")]
class CumulativeEntry
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var int|null $score
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $score = 0;
    
    /**
     * @var Person $author
     */
    #[ORM\OneToOne(targetEntity: Person::class)]
    #[ORM\JoinColumn(name: "author_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Person $author;
    
    /**
     * @var Collection<int, SectionEntry>&Selectable<int, SectionEntry> $entries
     */
    #[ORM\ManyToMany(targetEntity: SectionEntry::class)]
    #[ORM\JoinTable(name: "cumulativeentry_sectionentry")]
    #[ORM\JoinColumn(name: "cumulativeentry_id", nullable: false)]
    #[ORM\InverseJoinColumn(name: "sectionentry_id", nullable: false)]
    protected Collection $entries;
    
    /**
     * @var CumulativeCompetition $partOf
     */
    #[ORM\ManyToOne(targetEntity: CumulativeCompetition::class, inversedBy: "scores")]
    #[ORM\JoinColumn(name: "part_of_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected CumulativeCompetition $partOf;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->entries = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getScore(): ?int
    {
        return $this->score;
    }
    
    /**
     *
     *
     * @param int|null $score
     */
    public function setScore(?int $score): self
    {
        $this->score = $score;
        return $this;
    }

    /**
     *
     *
     * @return Person
     */
    public function getAuthor(): Person
    {
        return $this->author;
    }
    
    /**
     *
     *
     * @param Person $author
     */
    public function setAuthor(Person $author): self
    {
        $this->author = $author;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, SectionEntry>&Selectable<int, SectionEntry>
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }
    
    /**
     *
     *
     * @param SectionEntry $entry
     */
    public function addEntry(SectionEntry $entry): self
    {
        $this->entries->add($entry);
        return $this;
    }
    
    /**
     *
     *
     * @param SectionEntry $entry
     */
    public function removeEntry(SectionEntry $entry): self
    {
        $this->entries->removeElement($entry);
        return $this;
    }

    /**
     *
     *
     * @return CumulativeCompetition
     */
    public function getPartOf(): CumulativeCompetition
    {
        return $this->partOf;
    }
    
    /**
     *
     *
     * @param CumulativeCompetition $partOf
     */
    public function setPartOf(CumulativeCompetition $partOf): self
    {
        $this->partOf = $partOf;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s: %d', $this->author->getDefaultLabel(), $this->score);
    }

}
