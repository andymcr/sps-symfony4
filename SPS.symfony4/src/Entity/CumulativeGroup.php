<?php
namespace App\Entity;

use App\Repository\CumulativeGroupsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: CumulativeGroupsRepository::class)]
#[ORM\Table(name: "pointssets")]
#[UniqueEntity("name")]
class CumulativeGroup
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $name
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $name = '';
    
    /**
     * @var Collection<int, CumulativeCompetition>&Selectable<int, CumulativeCompetition> $competitions
     */
    #[ORM\OneToMany(targetEntity: CumulativeCompetition::class, mappedBy: "partOf", cascade: ["persist", "remove"])]
    protected Collection $competitions;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->competitions = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     *
     *
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, CumulativeCompetition>&Selectable<int, CumulativeCompetition>
     */
    public function getCompetitions(): Collection
    {
        return $this->competitions;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->name);
    }

}
