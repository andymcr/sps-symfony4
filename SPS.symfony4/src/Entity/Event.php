<?php
namespace App\Entity;

use App\Repository\EventsRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: EventsRepository::class)]
#[ORM\Table(name: "events")]
#[UniqueEntity("title")]
class Event
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $title
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $title = '';
    
    /**
     * @var int $category
     */
    #[ORM\Column(type: "integer")]
    #[Assert\NotBlank]
    protected int $category = 0;
    
    /**
     * @var bool|null $draft
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $draft = false;
    
    /**
     * @var bool|null $cancelled
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $cancelled = false;
    
    /**
     * @var bool|null $postponed
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $postponed = false;
    
    /**
     * @var bool|null $publicise
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $publicise;
    
    /**
     * @var \DateTimeInterface $start
     */
    #[ORM\Column(type: "datetime")]
    #[Assert\NotBlank]
    protected \DateTimeInterface $start;
    
    /**
     * @var \DateTimeInterface $duration
     */
    #[ORM\Column(type: "time")]
    #[Assert\NotBlank]
    protected \DateTimeInterface $duration;
    
    /**
     * @var Organisation $organisedBy
     */
    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: "events")]
    #[ORM\JoinColumn(name: "organised_by_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Organisation $organisedBy;
    
    /**
     * @var Venue|null $venue
     */
    #[ORM\ManyToOne(targetEntity: Venue::class)]
    #[ORM\JoinColumn(name: "venue_id", referencedColumnName: "id")]
    protected ?Venue $venue = null;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->publicise = true;
        $this->start = new \DateTime('00:00:00');
        $this->duration = new \DateTime('02:00:00');
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    
    /**
     *
     *
     * @param string $title
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     *
     * @return int
     */
    public function getCategory(): int
    {
        return $this->category;
    }
    
    /**
     *
     *
     * @param int $category
     */
    public function setCategory(int $category): self
    {
        $this->category = $category;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isDraft(): ?bool
    {
        return $this->draft;
    }
    
    /**
     *
     *
     * @param bool|null $draft
     */
    public function setDraft(?bool $draft): self
    {
        $this->draft = $draft;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isCancelled(): ?bool
    {
        return $this->cancelled;
    }
    
    /**
     *
     *
     * @param bool|null $cancelled
     */
    public function setCancelled(?bool $cancelled): self
    {
        $this->cancelled = $cancelled;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPostponed(): ?bool
    {
        return $this->postponed;
    }
    
    /**
     *
     *
     * @param bool|null $postponed
     */
    public function setPostponed(?bool $postponed): self
    {
        $this->postponed = $postponed;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPublicise(): ?bool
    {
        return $this->publicise;
    }
    
    /**
     *
     *
     * @param bool|null $publicise
     */
    public function setPublicise(?bool $publicise): self
    {
        $this->publicise = $publicise;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface
     */
    public function getStart(): \DateTimeInterface
    {
        return $this->start;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface $start
     */
    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;
        return $this;
    }

    /**
     *
     *
     * @return \DateTimeInterface
     */
    public function getDuration(): \DateTimeInterface
    {
        return $this->duration;
    }
    
    /**
     *
     *
     * @param \DateTimeInterface $duration
     */
    public function setDuration(\DateTimeInterface $duration): self
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     *
     *
     * @return Organisation
     */
    public function getOrganisedBy(): Organisation
    {
        return $this->organisedBy;
    }
    
    /**
     *
     *
     * @param Organisation $organisedBy
     */
    public function setOrganisedBy(Organisation $organisedBy): self
    {
        $this->organisedBy = $organisedBy;
        return $this;
    }

    /**
     *
     *
     * @return Venue|null
     */
    public function getVenue(): ?Venue
    {
        return $this->venue;
    }
    
    /**
     *
     *
     * @param Venue $venue
     */
    public function setVenue(Venue $venue): self
    {
        $this->venue = $venue;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->title);
    }

}
