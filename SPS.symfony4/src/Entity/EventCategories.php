<?php
namespace App\Entity;

class EventCategories
{
	const Unknown = 1;
	const Competition = 2;
	const Folio = 3;
	const MembersEvening = 4;
	const Talk = 5;

    /**
     *
     *
     * @return array<string, int>
     */
	public static function values(): array
	{
		$values = [];
		$values['Unknown'] = self::Unknown;
		$values['Competition'] = self::Competition;
		$values['Folio'] = self::Folio;
		$values['Members Evening'] = self::MembersEvening;
		$values['Talk'] = self::Talk;

		return $values;
	}

    /**
     *
     * @param string $value
     * @param string $default
     *
     * @return string
     */
	public static function value(string $value, string $default = ''): string
	{
		$values = array_flip(EventCategories::values());

		if (array_key_exists($value, $values)) {
			return $values[$value];
		}

		return $default;
	}

}
