<?php
namespace App\Entity;

use App\Repository\ImagesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: ImagesRepository::class)]
#[ORM\Table(name: "images")]
class Image
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $title
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $title = '';
    
    /**
     * @var Person $author
     */
    #[ORM\ManyToOne(targetEntity: Person::class, inversedBy: "images")]
    #[ORM\JoinColumn(name: "author_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Person $author;
    
    /**
     * @var Collection<int, ImageVariant>&Selectable<int, ImageVariant> $variants
     */
    #[ORM\OneToMany(targetEntity: ImageVariant::class, mappedBy: "image", cascade: ["persist", "remove"])]
    #[Assert\Unique(fields: ['description'])]
    protected Collection $variants;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->variants = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    
    /**
     *
     *
     * @param string $title
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     *
     * @return Person
     */
    public function getAuthor(): Person
    {
        return $this->author;
    }
    
    /**
     *
     *
     * @param Person $author
     */
    public function setAuthor(Person $author): self
    {
        $this->author = $author;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, ImageVariant>&Selectable<int, ImageVariant>
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->title);
    }

    /**
     *
     * @return string
     */
    public function getAuthorLabel(): String
    {
        return sprintf('%s', $this->author->getDefaultLabel());
    }

    /**
     *
     * @return string
     */
    public function getTitleWithAuthorLabel(): String
    {
        return sprintf('%s - %s', $this->title, $this->author->getDefaultLabel());
    }

}
