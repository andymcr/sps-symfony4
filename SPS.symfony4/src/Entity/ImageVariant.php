<?php
namespace App\Entity;

use App\Repository\ImageVariantsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 *
 **/
#[ORM\Entity(repositoryClass: ImageVariantsRepository::class)]
#[ORM\Table(name: "imagevariants")]
#[Vich\Uploadable]
class ImageVariant
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $description
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $description = '';
    
    /**
     * @var string|null $filePath
     */
    #[ORM\Column(name: "file_path", type: "string", nullable: true)]
    protected ?string $filePath = null;
    
    /**
     * @var bool|null $clubShowcase
     */
    #[ORM\Column(name: "club_showcase", type: "boolean", nullable: true)]
    protected ?bool $clubShowcase = false;
    
    /**
     * @var bool|null $personalShowcase
     */
    #[ORM\Column(name: "personal_showcase", type: "boolean", nullable: true)]
    protected ?bool $personalShowcase = false;
    
    /**
     * @var bool|null $photo
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $photo = false;
    
    /**
     * @var int|null $width
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $width;
    
    /**
     * @var int|null $height
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $height;
    
    /**
     * @var string|null
     */
    #[ORM\Column(type: "string")]
    protected $fileName;
    
    /**
     * @var File|null
     */
    #[Vich\UploadableField(mapping: "file", fileNameProperty: "fileName", size: "fileSize", mimeType: "file.mimeType", originalName: "file.originalName", dimensions: "file.dimensions")]
    protected $fileFile;
    
    /**
     * @var Collection<int, SectionEntry>&Selectable<int, SectionEntry> $entries
     */
    #[ORM\OneToMany(targetEntity: SectionEntry::class, mappedBy: "imageVariant")]
    protected Collection $entries;
    
    /**
     * @var Image $image
     */
    #[ORM\ManyToOne(targetEntity: Image::class, inversedBy: "variants")]
    #[ORM\JoinColumn(name: "image_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Image $image;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->width = 0;
        $this->height = 0;
        $this->entries = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
    
    /**
     *
     *
     * @param string $description
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }
    
    /**
     *
     *
     * @param string|null $filePath
     */
    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isClubShowcase(): ?bool
    {
        return $this->clubShowcase;
    }
    
    /**
     *
     *
     * @param bool|null $clubShowcase
     */
    public function setClubShowcase(?bool $clubShowcase): self
    {
        $this->clubShowcase = $clubShowcase;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPersonalShowcase(): ?bool
    {
        return $this->personalShowcase;
    }
    
    /**
     *
     *
     * @param bool|null $personalShowcase
     */
    public function setPersonalShowcase(?bool $personalShowcase): self
    {
        $this->personalShowcase = $personalShowcase;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isPhoto(): ?bool
    {
        return $this->photo;
    }
    
    /**
     *
     *
     * @param bool|null $photo
     */
    public function setPhoto(?bool $photo): self
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }
    
    /**
     *
     *
     * @param int|null $width
     */
    public function setWidth(?int $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }
    
    /**
     *
     *
     * @param int|null $height
     */
    public function setHeight(?int $height): self
    {
        $this->height = $height;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }
    
    /**
     *
     *
     * @param string|null $fileName
     */
    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;
        return $this;
    }
    
    /**
     *
     *
     * @return File|null
     */
    public function getFileFile(): ?File
    {
        return $this->fileFile;
    }
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $fileFile
     **/
    public function setFileFile(?File $fileFile): void
    {
        $this->fileFile = $fileFile;
        if (null !== $fileFile) {
                // It is required that at least one field changes if you are using doctrine
                // otherwise the event listeners won't be called and the file is lost
            $this->updated = new \DateTimeImmutable();
        }
    }

    /**
     *
     *
     * @return Collection<int, SectionEntry>&Selectable<int, SectionEntry>
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    /**
     *
     *
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }
    
    /**
     *
     *
     * @param Image $image
     */
    public function setImage(Image $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s (%s)', $this->image->getTitleWithAuthorLabel(), $this->description);
    }

    /**
     *
     * @return string
     */
    public function getImageLabel(): String
    {
        return sprintf('%s', $this->image->getDefaultLabel());
    }

}
