<?php
namespace App\Entity;

use App\Repository\OrganisationsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: OrganisationsRepository::class)]
#[ORM\Table(name: "organisations")]
#[UniqueEntity("title")]
class Organisation
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $title
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $title = '';
    
    /**
     * @var string|null $shortTitle
     */
    #[ORM\Column(name: "short_title", type: "string", nullable: true)]
    protected ?string $shortTitle = null;
    
    /**
     * @var string|null $website
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $website = null;
    
    /**
     * @var Collection<int, Person>&Selectable<int, Person> $members
     */
    #[ORM\OneToMany(targetEntity: Person::class, mappedBy: "memberOf")]
    protected Collection $members;
    
    /**
     * @var Collection<int, Event>&Selectable<int, Event> $events
     */
    #[ORM\OneToMany(targetEntity: Event::class, mappedBy: "organisedBy")]
    protected Collection $events;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    
    /**
     *
     *
     * @param string $title
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getShortTitle(): ?string
    {
        return $this->shortTitle;
    }
    
    /**
     *
     *
     * @param string|null $shortTitle
     */
    public function setShortTitle(?string $shortTitle): self
    {
        $this->shortTitle = $shortTitle;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }
    
    /**
     *
     *
     * @param string|null $website
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, Person>&Selectable<int, Person>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    /**
     *
     *
     * @return Collection<int, Event>&Selectable<int, Event>
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->title);
    }

}
