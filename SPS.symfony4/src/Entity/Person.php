<?php
namespace App\Entity;

use App\Repository\PeopleRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: PeopleRepository::class)]
#[ORM\Table(name: "people")]
#[UniqueEntity("username")]
class Person
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $username
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $username = '';
    
    /**
     * @var string $familyName
     */
    #[ORM\Column(name: "family_name", type: "string")]
    #[Assert\NotBlank]
    protected string $familyName = '';
    
    /**
     * @var string $forenames
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $forenames = '';
    
    /**
     * @var string|null $salutation
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $salutation = null;
    
    /**
     * @var string|null $website
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $website = null;
    
    /**
     * @var ImageVariant|null $showcaseImage
     */
    #[ORM\OneToOne(targetEntity: ImageVariant::class)]
    #[ORM\JoinColumn(name: "showcase_image_id", referencedColumnName: "id")]
    protected ?ImageVariant $showcaseImage = null;
    
    /**
     * @var Organisation|null $memberOf
     */
    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: "members")]
    #[ORM\JoinColumn(name: "member_of_id", referencedColumnName: "id")]
    protected ?Organisation $memberOf = null;
    
    /**
     * @var Collection<int, Distinction>&Selectable<int, Distinction> $distinctions
     */
    #[ORM\ManyToMany(targetEntity: Distinction::class)]
    #[ORM\JoinTable(name: "distinctions_people")]
    #[ORM\JoinColumn(name: "inv_distinctions_id", nullable: false)]
    #[ORM\InverseJoinColumn(name: "distinctions_id", nullable: false)]
    protected Collection $distinctions;
    
    /**
     * @var Collection<int, Competition>&Selectable<int, Competition> $hasJudged
     */
    #[ORM\ManyToMany(targetEntity: Competition::class, mappedBy: "judges")]
    protected Collection $hasJudged;
    
    /**
     * @var Collection<int, Talk>&Selectable<int, Talk> $hasPresented
     */
    #[ORM\ManyToMany(targetEntity: Talk::class, mappedBy: "presenters")]
    protected Collection $hasPresented;
    
    /**
     * @var Collection<int, Image>&Selectable<int, Image> $images
     */
    #[ORM\OneToMany(targetEntity: Image::class, mappedBy: "author", cascade: ["persist", "remove"])]
    #[Assert\Unique(fields: ['title'])]
    protected Collection $images;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->distinctions = new ArrayCollection();
        $this->hasJudged = new ArrayCollection();
        $this->hasPresented = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
    
    /**
     *
     *
     * @param string $username
     */
    public function setUsername(string $username): self
    {
        $this->username = strtolower($username);
        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getFamilyName(): string
    {
        return $this->familyName;
    }
    
    /**
     *
     *
     * @param string $familyName
     */
    public function setFamilyName(string $familyName): self
    {
        $this->familyName = $familyName;
        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getForenames(): string
    {
        return $this->forenames;
    }
    
    /**
     *
     *
     * @param string $forenames
     */
    public function setForenames(string $forenames): self
    {
        $this->forenames = $forenames;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getSalutation(): ?string
    {
        return $this->salutation;
    }
    
    /**
     *
     *
     * @param string|null $salutation
     */
    public function setSalutation(?string $salutation): self
    {
        $this->salutation = $salutation;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }
    
    /**
     *
     *
     * @param string|null $website
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;
        return $this;
    }

    /**
     *
     *
     * @return ImageVariant|null
     */
    public function getShowcaseImage(): ?ImageVariant
    {
        return $this->showcaseImage;
    }
    
    /**
     *
     *
     * @param ImageVariant $showcaseImage
     */
    public function setShowcaseImage(ImageVariant $showcaseImage): self
    {
        $this->showcaseImage = $showcaseImage;
        return $this;
    }

    /**
     *
     *
     * @return Organisation|null
     */
    public function getMemberOf(): ?Organisation
    {
        return $this->memberOf;
    }
    
    /**
     *
     *
     * @param Organisation $memberOf
     */
    public function setMemberOf(Organisation $memberOf): self
    {
        $this->memberOf = $memberOf;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, Distinction>&Selectable<int, Distinction>
     */
    public function getDistinctions(): Collection
    {
        return $this->distinctions;
    }

    /**
     *
     *
     * @return Collection<int, Competition>&Selectable<int, Competition>
     */
    public function getHasJudged(): Collection
    {
        return $this->hasJudged;
    }

    /**
     *
     *
     * @return Collection<int, Talk>&Selectable<int, Talk>
     */
    public function getHasPresented(): Collection
    {
        return $this->hasPresented;
    }

    /**
     *
     *
     * @return Collection<int, Image>&Selectable<int, Image>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s %s', $this->forenames, $this->familyName);
    }

    /**
     *
     * @return string
     */
    public function getNameWithDistinctionsLabel(): String
    {
        // Start of user code label.nameWithDistinctions
        $distinctions = '';
        foreach($this->getDistinctions() as $distinction) {
            $distinctions .= ' ' . $distinction->getTitle();
        }
        return sprintf('%s %s', $this->getDefaultLabel(), trim($distinctions));
        // End of user code
    }

}
