<?php
namespace App\Entity;

use App\Repository\SectionAwardRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 *
 **/
#[ORM\Entity(repositoryClass: SectionAwardRepository::class)]
#[ORM\Table(name: "sectionawards")]
class SectionAward
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string|null $title
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $title = null;
    
    /**
     * @var int|null $rank
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $rank = 0;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    /**
     *
     *
     * @param string|null $title
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getRank(): ?int
    {
        return $this->rank;
    }
    
    /**
     *
     *
     * @param int|null $rank
     */
    public function setRank(?int $rank): self
    {
        $this->rank = $rank;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->title);
    }

}
