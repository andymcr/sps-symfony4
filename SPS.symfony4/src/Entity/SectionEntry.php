<?php
namespace App\Entity;

use App\Repository\SectionEntriesRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: SectionEntriesRepository::class)]
#[ORM\Table(name: "sectionentries")]
class SectionEntry
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string|null $title
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $title = null;
    
    /**
     * @var int|null $entryNumber
     */
    #[ORM\Column(name: "entry_number", type: "integer", nullable: true)]
    protected ?int $entryNumber = 0;
    
    /**
     * @var int|null $removalOrder
     */
    #[ORM\Column(name: "removal_order", type: "integer", nullable: true)]
    protected ?int $removalOrder = 0;
    
    /**
     * @var float|null $score
     */
    #[ORM\Column(type: "float", nullable: true)]
    protected ?float $score = 0.0;
    
    /**
     * @var int|null $points
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $points = 0;
    
    /**
     * @var bool|null $removed
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $removed;
    
    /**
     * @var bool|null $disqualified
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $disqualified;
    
    /**
     * @var bool|null $tagged
     */
    #[ORM\Column(type: "boolean", nullable: true)]
    protected ?bool $tagged;
    
    /**
     * @var SectionAward|null $award
     */
    #[ORM\OneToOne(targetEntity: SectionAward::class)]
    #[ORM\JoinColumn(name: "award_id", referencedColumnName: "id")]
    protected ?SectionAward $award = null;
    
    /**
     * @var ImageVariant $imageVariant
     */
    #[ORM\ManyToOne(targetEntity: ImageVariant::class, inversedBy: "entries")]
    #[ORM\JoinColumn(name: "image_variant_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected ImageVariant $imageVariant;
    
    /**
     * @var CompetitionSection $enteredIn
     */
    #[ORM\ManyToOne(targetEntity: CompetitionSection::class, inversedBy: "entries")]
    #[ORM\JoinColumn(name: "entered_in_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected CompetitionSection $enteredIn;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->removed = false;
        $this->disqualified = false;
        $this->tagged = false;
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    /**
     *
     *
     * @param string|null $title
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getEntryNumber(): ?int
    {
        return $this->entryNumber;
    }
    
    /**
     *
     *
     * @param int|null $entryNumber
     */
    public function setEntryNumber(?int $entryNumber): self
    {
        $this->entryNumber = $entryNumber;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getRemovalOrder(): ?int
    {
        return $this->removalOrder;
    }
    
    /**
     *
     *
     * @param int|null $removalOrder
     */
    public function setRemovalOrder(?int $removalOrder): self
    {
        $this->removalOrder = $removalOrder;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getScore(): ?float
    {
        return $this->score;
    }
    
    /**
     *
     *
     * @param float|null $score
     */
    public function setScore(?float $score): self
    {
        $this->score = $score;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getPoints(): ?int
    {
        return $this->points;
    }
    
    /**
     *
     *
     * @param int|null $points
     */
    public function setPoints(?int $points): self
    {
        $this->points = $points;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isRemoved(): ?bool
    {
        return $this->removed;
    }
    
    /**
     *
     *
     * @param bool|null $removed
     */
    public function setRemoved(?bool $removed): self
    {
        $this->removed = $removed;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isDisqualified(): ?bool
    {
        return $this->disqualified;
    }
    
    /**
     *
     *
     * @param bool|null $disqualified
     */
    public function setDisqualified(?bool $disqualified): self
    {
        $this->disqualified = $disqualified;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function isTagged(): ?bool
    {
        return $this->tagged;
    }
    
    /**
     *
     *
     * @param bool|null $tagged
     */
    public function setTagged(?bool $tagged): self
    {
        $this->tagged = $tagged;
        return $this;
    }

    /**
     *
     *
     * @return SectionAward|null
     */
    public function getAward(): ?SectionAward
    {
        return $this->award;
    }
    
    /**
     *
     *
     * @param SectionAward $award
     */
    public function setAward(SectionAward $award): self
    {
        $this->award = $award;
        return $this;
    }

    /**
     *
     *
     * @return ImageVariant
     */
    public function getImageVariant(): ImageVariant
    {
        return $this->imageVariant;
    }
    
    /**
     *
     *
     * @param ImageVariant $imageVariant
     */
    public function setImageVariant(ImageVariant $imageVariant): self
    {
        $this->imageVariant = $imageVariant;
        return $this;
    }

    /**
     *
     *
     * @return CompetitionSection
     */
    public function getEnteredIn(): CompetitionSection
    {
        return $this->enteredIn;
    }
    
    /**
     *
     *
     * @param CompetitionSection $enteredIn
     */
    public function setEnteredIn(CompetitionSection $enteredIn): self
    {
        $this->enteredIn = $enteredIn;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getTitleWithAuthorLabel(): String
    {
        return sprintf('%s - %s', $this->title, $this->imageVariant->getImageLabel());
    }

    /**
     *
     * @return string
     */
    public function getScoreWithPointsLabel(): String
    {
        // Start of user code label.scoreWithPoints
        if ($this->points > 0 ) {
            return sprintf('%s (%s %s)', $this->score, $this->points, $this->points == 1 ? 'point' : 'points');
        } else {
            return sprintf('%s', $this->score);
        }
        // End of user code
    }

    /**
     *
     * @return string
     */
    public function getAwardWithPointsLabel(): String
    {
        // Start of user code label.awardWithPoints
        if ($this->award != null) {
            if ($this->points > 0) {
                return sprintf('%s (%s %s)', $this->award->getTitle(), $this->points, $this->points == 1 ? 'point' : 'points');
            } else {
                return sprintf('%s', $this->award->getTitle());
            }
        } else {
            if ($this->points > 0) {
                return sprintf('None (%s %s)', $this->points, $this->points == 1 ? 'point' : 'points');
            } else {
                return "None";
            }
        }
        // End of user code
    }

    /**
     *
     * @return string
     */
    public function getAwardScoreAndTitleLabel(): String
    {
        return sprintf('%s (%s points) %s', $this->award->getDefaultLabel(), $this->points, $this->title);
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->title);
    }

}
