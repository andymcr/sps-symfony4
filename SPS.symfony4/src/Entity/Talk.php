<?php
namespace App\Entity;

use App\Repository\TalksRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: TalksRepository::class)]
#[ORM\Table(name: "talks")]
class Talk
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string|null $synopsis
     */
    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $synopsis = null;
    
    /**
     * @var Collection<int, Person>&Selectable<int, Person> $presenters
     */
    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: "hasPresented")]
    #[ORM\JoinTable(name: "people_talks")]
    #[ORM\JoinColumn(name: "has_presented_id", nullable: false)]
    #[ORM\InverseJoinColumn(name: "presenters_id", nullable: false)]
    protected Collection $presenters;
    
    /**
     * @var Event $event
     */
    #[ORM\OneToOne(targetEntity: Event::class)]
    #[ORM\JoinColumn(name: "event_id", referencedColumnName: "id", nullable: false)]
    #[Assert\NotNull]
    protected Event $event;

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    public function __construct()
    {
        $this->presenters = new ArrayCollection();
    }

    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }
    
    /**
     *
     *
     * @param string|null $synopsis
     */
    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;
        return $this;
    }

    /**
     *
     *
     * @return Collection<int, Person>&Selectable<int, Person>
     */
    public function getPresenters(): Collection
    {
        return $this->presenters;
    }

    /**
     *
     *
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }
    
    /**
     *
     *
     * @param Event $event
     */
    public function setEvent(Event $event): self
    {
        $this->event = $event;
        return $this;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->event->getDefaultLabel());
    }

}
