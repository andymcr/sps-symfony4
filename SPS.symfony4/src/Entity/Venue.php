<?php
namespace App\Entity;

use App\Repository\VenuesRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 **/
#[ORM\Entity(repositoryClass: VenuesRepository::class)]
#[ORM\Table(name: "venues")]
#[UniqueEntity("name")]
class Venue
{
    /**
     * @var int $id
     */
    #[ORM\Id]
    #[ORM\Column(type: "integer")]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string $name
     */
    #[ORM\Column(type: "string")]
    #[Assert\NotBlank]
    protected string $name = '';

    /**
     * @var \DateTimeInterface $created
     */
    #[ORM\Column(type: "datetime")]
    #[Gedmo\Timestampable(on: "create")]
    protected \DateTimeInterface $created;
    
    /**
     * @var \DateTimeInterface $updated
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    protected \DateTimeInterface $updated;


    /**
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     *
     *
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
    
    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     *
     * @return string
     */
    public function getDefaultLabel(): String
    {
        return sprintf('%s', $this->name);
    }

}
