<?php
namespace App\Extensions\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class EnumerationTypes extends AbstractExtension
{
    public function getName(): string
    {
        return 'project';
    }

    /**
     *
     * @return array<string, TwigFilter>
     **/
    public function getFilters(): array
    {
        return [
            'awardsCompetition' => new TwigFilter('awardsCompetition', ['App\Entity\AwardsCompetition', 'value']),
            'eventCategories' => new TwigFilter('eventCategories', ['App\Entity\EventCategories', 'value']),
        ];
    }
}
