<?php
namespace App\Factory;

use App\Entity\Authentication;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Authentication>
 *
 * @method static Authentication|Proxy createOne(array $attributes = [])
 * @method static Authentication[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Authentication[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Authentication|Proxy find(object|array|mixed $criteria)
 * @method static Authentication|Proxy findOrCreate(array $attributes)
 * @method static Authentication|Proxy first(string $sortedField = 'id')
 * @method static Authentication|Proxy last(string $sortedField = 'id')
 * @method static Authentication|Proxy random(array $attributes = [])
 * @method static Authentication|Proxy randomOrCreate(array $attributes = []))
 * @method static Authentication[]|Proxy[] all()
 * @method static Authentication[]|Proxy[] findBy(array $attributes)
 * @method static Authentication[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Authentication[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static AuthenticationRepository|RepositoryProxy repository()
 * @method Authentication|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Authentication&Proxy createOne(array $attributes = [])
 * @phpstan-method static Authentication[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Authentication[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Authentication&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Authentication&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Authentication&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Authentication&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Authentication&Proxy random(array $attributes = [])
 * @phpstan-method static Authentication&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Authentication[]&Proxy[] all()
 * @phpstan-method static Authentication[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Authentication[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Authentication[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Authentication&Proxy create(array|callable $attributes = [])
 */
class AuthenticationFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'username' => self::faker()->text(),
            'enabled' => self::faker()->boolean(),
            'email' => self::faker()->email(),
            'emailVerified' => self::faker()->boolean(),
            'password' => self::faker()->text(),
            'localed' => self::faker()->boolean(),
            'expired' => self::faker()->boolean(),
            'expiresAt' => new \DateTimeImmutable(self::faker()->iso8601('+2 years')),
            'passwordChangedAt' => new \DateTimeImmutable(self::faker()->iso8601('+2 years')),
            'credentialsExpired' => self::faker()->boolean(),
            'credentialsExpireAt' => new \DateTimeImmutable(self::faker()->iso8601('+2 years')),
            'lastLogin' => new \DateTimeImmutable(self::faker()->iso8601('+2 years')),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Authentication::class;
    }
}
