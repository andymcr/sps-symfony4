<?php
namespace App\Factory;

use App\Entity\CompetitionAward;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<CompetitionAward>
 *
 * @method static CompetitionAward|Proxy createOne(array $attributes = [])
 * @method static CompetitionAward[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CompetitionAward[]&Proxy[] createSequence(array|callable $sequence)
 * @method static CompetitionAward|Proxy find(object|array|mixed $criteria)
 * @method static CompetitionAward|Proxy findOrCreate(array $attributes)
 * @method static CompetitionAward|Proxy first(string $sortedField = 'id')
 * @method static CompetitionAward|Proxy last(string $sortedField = 'id')
 * @method static CompetitionAward|Proxy random(array $attributes = [])
 * @method static CompetitionAward|Proxy randomOrCreate(array $attributes = []))
 * @method static CompetitionAward[]|Proxy[] all()
 * @method static CompetitionAward[]|Proxy[] findBy(array $attributes)
 * @method static CompetitionAward[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static CompetitionAward[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CompetitionAwardsRepository|RepositoryProxy repository()
 * @method CompetitionAward|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static CompetitionAward&Proxy createOne(array $attributes = [])
 * @phpstan-method static CompetitionAward[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static CompetitionAward[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static CompetitionAward&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static CompetitionAward&Proxy findOrCreate(array $attributes)
 * @phpstan-method static CompetitionAward&Proxy first(string $sortedField = 'id')
 * @phpstan-method static CompetitionAward&Proxy last(string $sortedField = 'id')
 * @phpstan-method static CompetitionAward&Proxy random(array $attributes = [])
 * @phpstan-method static CompetitionAward&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static CompetitionAward[]&Proxy[] all()
 * @phpstan-method static CompetitionAward[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static CompetitionAward[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static CompetitionAward[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method CompetitionAward&Proxy create(array|callable $attributes = [])
 */
class CompetitionAwardFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'award' => self::faker()->randomNumber(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return CompetitionAward::class;
    }
}
