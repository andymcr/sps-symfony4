<?php
namespace App\Factory;

use App\Entity\CompetitionCategory;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<CompetitionCategory>
 *
 * @method static CompetitionCategory|Proxy createOne(array $attributes = [])
 * @method static CompetitionCategory[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CompetitionCategory[]&Proxy[] createSequence(array|callable $sequence)
 * @method static CompetitionCategory|Proxy find(object|array|mixed $criteria)
 * @method static CompetitionCategory|Proxy findOrCreate(array $attributes)
 * @method static CompetitionCategory|Proxy first(string $sortedField = 'id')
 * @method static CompetitionCategory|Proxy last(string $sortedField = 'id')
 * @method static CompetitionCategory|Proxy random(array $attributes = [])
 * @method static CompetitionCategory|Proxy randomOrCreate(array $attributes = []))
 * @method static CompetitionCategory[]|Proxy[] all()
 * @method static CompetitionCategory[]|Proxy[] findBy(array $attributes)
 * @method static CompetitionCategory[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static CompetitionCategory[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CompetitionCategoriesRepository|RepositoryProxy repository()
 * @method CompetitionCategory|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static CompetitionCategory&Proxy createOne(array $attributes = [])
 * @phpstan-method static CompetitionCategory[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static CompetitionCategory[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static CompetitionCategory&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static CompetitionCategory&Proxy findOrCreate(array $attributes)
 * @phpstan-method static CompetitionCategory&Proxy first(string $sortedField = 'id')
 * @phpstan-method static CompetitionCategory&Proxy last(string $sortedField = 'id')
 * @phpstan-method static CompetitionCategory&Proxy random(array $attributes = [])
 * @phpstan-method static CompetitionCategory&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static CompetitionCategory[]&Proxy[] all()
 * @phpstan-method static CompetitionCategory[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static CompetitionCategory[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static CompetitionCategory[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method CompetitionCategory&Proxy create(array|callable $attributes = [])
 */
class CompetitionCategoryFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'shortName' => self::faker()->text(),
            'fullName' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return CompetitionCategory::class;
    }
}
