<?php
namespace App\Factory;

use App\Entity\Competition;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Competition>
 *
 * @method static Competition|Proxy createOne(array $attributes = [])
 * @method static Competition[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Competition[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Competition|Proxy find(object|array|mixed $criteria)
 * @method static Competition|Proxy findOrCreate(array $attributes)
 * @method static Competition|Proxy first(string $sortedField = 'id')
 * @method static Competition|Proxy last(string $sortedField = 'id')
 * @method static Competition|Proxy random(array $attributes = [])
 * @method static Competition|Proxy randomOrCreate(array $attributes = []))
 * @method static Competition[]|Proxy[] all()
 * @method static Competition[]|Proxy[] findBy(array $attributes)
 * @method static Competition[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Competition[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CompetitionsRepository|RepositoryProxy repository()
 * @method Competition|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Competition&Proxy createOne(array $attributes = [])
 * @phpstan-method static Competition[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Competition[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Competition&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Competition&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Competition&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Competition&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Competition&Proxy random(array $attributes = [])
 * @phpstan-method static Competition&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Competition[]&Proxy[] all()
 * @phpstan-method static Competition[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Competition[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Competition[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Competition&Proxy create(array|callable $attributes = [])
 */
class CompetitionFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Competition::class;
    }
}
