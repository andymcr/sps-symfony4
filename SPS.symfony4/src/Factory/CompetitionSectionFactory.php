<?php
namespace App\Factory;

use App\Entity\CompetitionSection;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<CompetitionSection>
 *
 * @method static CompetitionSection|Proxy createOne(array $attributes = [])
 * @method static CompetitionSection[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CompetitionSection[]&Proxy[] createSequence(array|callable $sequence)
 * @method static CompetitionSection|Proxy find(object|array|mixed $criteria)
 * @method static CompetitionSection|Proxy findOrCreate(array $attributes)
 * @method static CompetitionSection|Proxy first(string $sortedField = 'id')
 * @method static CompetitionSection|Proxy last(string $sortedField = 'id')
 * @method static CompetitionSection|Proxy random(array $attributes = [])
 * @method static CompetitionSection|Proxy randomOrCreate(array $attributes = []))
 * @method static CompetitionSection[]|Proxy[] all()
 * @method static CompetitionSection[]|Proxy[] findBy(array $attributes)
 * @method static CompetitionSection[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static CompetitionSection[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CompetitionSectionsRepository|RepositoryProxy repository()
 * @method CompetitionSection|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static CompetitionSection&Proxy createOne(array $attributes = [])
 * @phpstan-method static CompetitionSection[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static CompetitionSection[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static CompetitionSection&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static CompetitionSection&Proxy findOrCreate(array $attributes)
 * @phpstan-method static CompetitionSection&Proxy first(string $sortedField = 'id')
 * @phpstan-method static CompetitionSection&Proxy last(string $sortedField = 'id')
 * @phpstan-method static CompetitionSection&Proxy random(array $attributes = [])
 * @phpstan-method static CompetitionSection&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static CompetitionSection[]&Proxy[] all()
 * @phpstan-method static CompetitionSection[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static CompetitionSection[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static CompetitionSection[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method CompetitionSection&Proxy create(array|callable $attributes = [])
 */
class CompetitionSectionFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return CompetitionSection::class;
    }
}
