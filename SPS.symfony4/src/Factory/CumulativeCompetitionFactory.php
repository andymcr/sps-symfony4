<?php
namespace App\Factory;

use App\Entity\CumulativeCompetition;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<CumulativeCompetition>
 *
 * @method static CumulativeCompetition|Proxy createOne(array $attributes = [])
 * @method static CumulativeCompetition[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CumulativeCompetition[]&Proxy[] createSequence(array|callable $sequence)
 * @method static CumulativeCompetition|Proxy find(object|array|mixed $criteria)
 * @method static CumulativeCompetition|Proxy findOrCreate(array $attributes)
 * @method static CumulativeCompetition|Proxy first(string $sortedField = 'id')
 * @method static CumulativeCompetition|Proxy last(string $sortedField = 'id')
 * @method static CumulativeCompetition|Proxy random(array $attributes = [])
 * @method static CumulativeCompetition|Proxy randomOrCreate(array $attributes = []))
 * @method static CumulativeCompetition[]|Proxy[] all()
 * @method static CumulativeCompetition[]|Proxy[] findBy(array $attributes)
 * @method static CumulativeCompetition[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static CumulativeCompetition[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CumulativeCompetitionsRepository|RepositoryProxy repository()
 * @method CumulativeCompetition|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static CumulativeCompetition&Proxy createOne(array $attributes = [])
 * @phpstan-method static CumulativeCompetition[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static CumulativeCompetition[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static CumulativeCompetition&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static CumulativeCompetition&Proxy findOrCreate(array $attributes)
 * @phpstan-method static CumulativeCompetition&Proxy first(string $sortedField = 'id')
 * @phpstan-method static CumulativeCompetition&Proxy last(string $sortedField = 'id')
 * @phpstan-method static CumulativeCompetition&Proxy random(array $attributes = [])
 * @phpstan-method static CumulativeCompetition&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static CumulativeCompetition[]&Proxy[] all()
 * @phpstan-method static CumulativeCompetition[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static CumulativeCompetition[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static CumulativeCompetition[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method CumulativeCompetition&Proxy create(array|callable $attributes = [])
 */
class CumulativeCompetitionFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return CumulativeCompetition::class;
    }
}
