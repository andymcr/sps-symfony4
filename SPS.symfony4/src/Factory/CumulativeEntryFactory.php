<?php
namespace App\Factory;

use App\Entity\CumulativeEntry;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<CumulativeEntry>
 *
 * @method static CumulativeEntry|Proxy createOne(array $attributes = [])
 * @method static CumulativeEntry[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CumulativeEntry[]&Proxy[] createSequence(array|callable $sequence)
 * @method static CumulativeEntry|Proxy find(object|array|mixed $criteria)
 * @method static CumulativeEntry|Proxy findOrCreate(array $attributes)
 * @method static CumulativeEntry|Proxy first(string $sortedField = 'id')
 * @method static CumulativeEntry|Proxy last(string $sortedField = 'id')
 * @method static CumulativeEntry|Proxy random(array $attributes = [])
 * @method static CumulativeEntry|Proxy randomOrCreate(array $attributes = []))
 * @method static CumulativeEntry[]|Proxy[] all()
 * @method static CumulativeEntry[]|Proxy[] findBy(array $attributes)
 * @method static CumulativeEntry[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static CumulativeEntry[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CumulativeEntriesRepository|RepositoryProxy repository()
 * @method CumulativeEntry|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static CumulativeEntry&Proxy createOne(array $attributes = [])
 * @phpstan-method static CumulativeEntry[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static CumulativeEntry[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static CumulativeEntry&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static CumulativeEntry&Proxy findOrCreate(array $attributes)
 * @phpstan-method static CumulativeEntry&Proxy first(string $sortedField = 'id')
 * @phpstan-method static CumulativeEntry&Proxy last(string $sortedField = 'id')
 * @phpstan-method static CumulativeEntry&Proxy random(array $attributes = [])
 * @phpstan-method static CumulativeEntry&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static CumulativeEntry[]&Proxy[] all()
 * @phpstan-method static CumulativeEntry[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static CumulativeEntry[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static CumulativeEntry[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method CumulativeEntry&Proxy create(array|callable $attributes = [])
 */
class CumulativeEntryFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return CumulativeEntry::class;
    }
}
