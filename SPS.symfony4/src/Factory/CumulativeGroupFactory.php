<?php
namespace App\Factory;

use App\Entity\CumulativeGroup;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<CumulativeGroup>
 *
 * @method static CumulativeGroup|Proxy createOne(array $attributes = [])
 * @method static CumulativeGroup[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CumulativeGroup[]&Proxy[] createSequence(array|callable $sequence)
 * @method static CumulativeGroup|Proxy find(object|array|mixed $criteria)
 * @method static CumulativeGroup|Proxy findOrCreate(array $attributes)
 * @method static CumulativeGroup|Proxy first(string $sortedField = 'id')
 * @method static CumulativeGroup|Proxy last(string $sortedField = 'id')
 * @method static CumulativeGroup|Proxy random(array $attributes = [])
 * @method static CumulativeGroup|Proxy randomOrCreate(array $attributes = []))
 * @method static CumulativeGroup[]|Proxy[] all()
 * @method static CumulativeGroup[]|Proxy[] findBy(array $attributes)
 * @method static CumulativeGroup[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static CumulativeGroup[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static CumulativeGroupsRepository|RepositoryProxy repository()
 * @method CumulativeGroup|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static CumulativeGroup&Proxy createOne(array $attributes = [])
 * @phpstan-method static CumulativeGroup[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static CumulativeGroup[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static CumulativeGroup&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static CumulativeGroup&Proxy findOrCreate(array $attributes)
 * @phpstan-method static CumulativeGroup&Proxy first(string $sortedField = 'id')
 * @phpstan-method static CumulativeGroup&Proxy last(string $sortedField = 'id')
 * @phpstan-method static CumulativeGroup&Proxy random(array $attributes = [])
 * @phpstan-method static CumulativeGroup&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static CumulativeGroup[]&Proxy[] all()
 * @phpstan-method static CumulativeGroup[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static CumulativeGroup[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static CumulativeGroup[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method CumulativeGroup&Proxy create(array|callable $attributes = [])
 */
class CumulativeGroupFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return CumulativeGroup::class;
    }
}
