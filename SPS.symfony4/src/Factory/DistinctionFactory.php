<?php
namespace App\Factory;

use App\Entity\Distinction;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Distinction>
 *
 * @method static Distinction|Proxy createOne(array $attributes = [])
 * @method static Distinction[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Distinction[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Distinction|Proxy find(object|array|mixed $criteria)
 * @method static Distinction|Proxy findOrCreate(array $attributes)
 * @method static Distinction|Proxy first(string $sortedField = 'id')
 * @method static Distinction|Proxy last(string $sortedField = 'id')
 * @method static Distinction|Proxy random(array $attributes = [])
 * @method static Distinction|Proxy randomOrCreate(array $attributes = []))
 * @method static Distinction[]|Proxy[] all()
 * @method static Distinction[]|Proxy[] findBy(array $attributes)
 * @method static Distinction[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Distinction[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static DistinctionsRepository|RepositoryProxy repository()
 * @method Distinction|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Distinction&Proxy createOne(array $attributes = [])
 * @phpstan-method static Distinction[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Distinction[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Distinction&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Distinction&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Distinction&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Distinction&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Distinction&Proxy random(array $attributes = [])
 * @phpstan-method static Distinction&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Distinction[]&Proxy[] all()
 * @phpstan-method static Distinction[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Distinction[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Distinction[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Distinction&Proxy create(array|callable $attributes = [])
 */
class DistinctionFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Distinction::class;
    }
}
