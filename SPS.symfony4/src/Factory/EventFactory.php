<?php
namespace App\Factory;

use App\Entity\Event;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Event>
 *
 * @method static Event|Proxy createOne(array $attributes = [])
 * @method static Event[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Event[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Event|Proxy find(object|array|mixed $criteria)
 * @method static Event|Proxy findOrCreate(array $attributes)
 * @method static Event|Proxy first(string $sortedField = 'id')
 * @method static Event|Proxy last(string $sortedField = 'id')
 * @method static Event|Proxy random(array $attributes = [])
 * @method static Event|Proxy randomOrCreate(array $attributes = []))
 * @method static Event[]|Proxy[] all()
 * @method static Event[]|Proxy[] findBy(array $attributes)
 * @method static Event[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Event[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static EventsRepository|RepositoryProxy repository()
 * @method Event|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Event&Proxy createOne(array $attributes = [])
 * @phpstan-method static Event[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Event[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Event&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Event&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Event&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Event&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Event&Proxy random(array $attributes = [])
 * @phpstan-method static Event&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Event[]&Proxy[] all()
 * @phpstan-method static Event[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Event[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Event[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Event&Proxy create(array|callable $attributes = [])
 */
class EventFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->text(),
            'category' => self::faker()->randomNumber(),
            'start' => new \DateTimeImmutable(self::faker()->iso8601('+2 years')),
            'duration' => new \DateTimeImmutable(self::faker()->iso8601('+2 years')),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Event::class;
    }
}
