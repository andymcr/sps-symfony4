<?php
namespace App\Factory;

use App\Entity\Image;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Image>
 *
 * @method static Image|Proxy createOne(array $attributes = [])
 * @method static Image[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Image[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Image|Proxy find(object|array|mixed $criteria)
 * @method static Image|Proxy findOrCreate(array $attributes)
 * @method static Image|Proxy first(string $sortedField = 'id')
 * @method static Image|Proxy last(string $sortedField = 'id')
 * @method static Image|Proxy random(array $attributes = [])
 * @method static Image|Proxy randomOrCreate(array $attributes = []))
 * @method static Image[]|Proxy[] all()
 * @method static Image[]|Proxy[] findBy(array $attributes)
 * @method static Image[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Image[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static ImagesRepository|RepositoryProxy repository()
 * @method Image|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Image&Proxy createOne(array $attributes = [])
 * @phpstan-method static Image[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Image[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Image&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Image&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Image&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Image&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Image&Proxy random(array $attributes = [])
 * @phpstan-method static Image&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Image[]&Proxy[] all()
 * @phpstan-method static Image[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Image[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Image[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Image&Proxy create(array|callable $attributes = [])
 */
class ImageFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Image::class;
    }
}
