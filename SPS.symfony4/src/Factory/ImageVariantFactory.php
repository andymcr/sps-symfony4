<?php
namespace App\Factory;

use App\Entity\ImageVariant;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<ImageVariant>
 *
 * @method static ImageVariant|Proxy createOne(array $attributes = [])
 * @method static ImageVariant[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ImageVariant[]&Proxy[] createSequence(array|callable $sequence)
 * @method static ImageVariant|Proxy find(object|array|mixed $criteria)
 * @method static ImageVariant|Proxy findOrCreate(array $attributes)
 * @method static ImageVariant|Proxy first(string $sortedField = 'id')
 * @method static ImageVariant|Proxy last(string $sortedField = 'id')
 * @method static ImageVariant|Proxy random(array $attributes = [])
 * @method static ImageVariant|Proxy randomOrCreate(array $attributes = []))
 * @method static ImageVariant[]|Proxy[] all()
 * @method static ImageVariant[]|Proxy[] findBy(array $attributes)
 * @method static ImageVariant[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static ImageVariant[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static ImageVariantsRepository|RepositoryProxy repository()
 * @method ImageVariant|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static ImageVariant&Proxy createOne(array $attributes = [])
 * @phpstan-method static ImageVariant[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static ImageVariant[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static ImageVariant&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static ImageVariant&Proxy findOrCreate(array $attributes)
 * @phpstan-method static ImageVariant&Proxy first(string $sortedField = 'id')
 * @phpstan-method static ImageVariant&Proxy last(string $sortedField = 'id')
 * @phpstan-method static ImageVariant&Proxy random(array $attributes = [])
 * @phpstan-method static ImageVariant&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static ImageVariant[]&Proxy[] all()
 * @phpstan-method static ImageVariant[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static ImageVariant[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static ImageVariant[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method ImageVariant&Proxy create(array|callable $attributes = [])
 */
class ImageVariantFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'description' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return ImageVariant::class;
    }
}
