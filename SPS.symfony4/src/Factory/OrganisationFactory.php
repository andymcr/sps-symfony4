<?php
namespace App\Factory;

use App\Entity\Organisation;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Organisation>
 *
 * @method static Organisation|Proxy createOne(array $attributes = [])
 * @method static Organisation[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Organisation[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Organisation|Proxy find(object|array|mixed $criteria)
 * @method static Organisation|Proxy findOrCreate(array $attributes)
 * @method static Organisation|Proxy first(string $sortedField = 'id')
 * @method static Organisation|Proxy last(string $sortedField = 'id')
 * @method static Organisation|Proxy random(array $attributes = [])
 * @method static Organisation|Proxy randomOrCreate(array $attributes = []))
 * @method static Organisation[]|Proxy[] all()
 * @method static Organisation[]|Proxy[] findBy(array $attributes)
 * @method static Organisation[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Organisation[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static OrganisationsRepository|RepositoryProxy repository()
 * @method Organisation|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Organisation&Proxy createOne(array $attributes = [])
 * @phpstan-method static Organisation[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Organisation[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Organisation&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Organisation&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Organisation&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Organisation&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Organisation&Proxy random(array $attributes = [])
 * @phpstan-method static Organisation&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Organisation[]&Proxy[] all()
 * @phpstan-method static Organisation[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Organisation[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Organisation[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Organisation&Proxy create(array|callable $attributes = [])
 */
class OrganisationFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Organisation::class;
    }
}
