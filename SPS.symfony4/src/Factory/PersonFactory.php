<?php
namespace App\Factory;

use App\Entity\Person;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Person>
 *
 * @method static Person|Proxy createOne(array $attributes = [])
 * @method static Person[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Person[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Person|Proxy find(object|array|mixed $criteria)
 * @method static Person|Proxy findOrCreate(array $attributes)
 * @method static Person|Proxy first(string $sortedField = 'id')
 * @method static Person|Proxy last(string $sortedField = 'id')
 * @method static Person|Proxy random(array $attributes = [])
 * @method static Person|Proxy randomOrCreate(array $attributes = []))
 * @method static Person[]|Proxy[] all()
 * @method static Person[]|Proxy[] findBy(array $attributes)
 * @method static Person[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Person[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static PeopleRepository|RepositoryProxy repository()
 * @method Person|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Person&Proxy createOne(array $attributes = [])
 * @phpstan-method static Person[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Person[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Person&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Person&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Person&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Person&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Person&Proxy random(array $attributes = [])
 * @phpstan-method static Person&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Person[]&Proxy[] all()
 * @phpstan-method static Person[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Person[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Person[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Person&Proxy create(array|callable $attributes = [])
 */
class PersonFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'username' => self::faker()->email(),
            'familyName' => self::faker()->text(),
            'forenames' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Person::class;
    }
}
