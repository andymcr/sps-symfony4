<?php
namespace App\Factory;

use App\Entity\SectionAward;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<SectionAward>
 *
 * @method static SectionAward|Proxy createOne(array $attributes = [])
 * @method static SectionAward[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static SectionAward[]&Proxy[] createSequence(array|callable $sequence)
 * @method static SectionAward|Proxy find(object|array|mixed $criteria)
 * @method static SectionAward|Proxy findOrCreate(array $attributes)
 * @method static SectionAward|Proxy first(string $sortedField = 'id')
 * @method static SectionAward|Proxy last(string $sortedField = 'id')
 * @method static SectionAward|Proxy random(array $attributes = [])
 * @method static SectionAward|Proxy randomOrCreate(array $attributes = []))
 * @method static SectionAward[]|Proxy[] all()
 * @method static SectionAward[]|Proxy[] findBy(array $attributes)
 * @method static SectionAward[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static SectionAward[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static SectionAwardRepository|RepositoryProxy repository()
 * @method SectionAward|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static SectionAward&Proxy createOne(array $attributes = [])
 * @phpstan-method static SectionAward[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static SectionAward[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static SectionAward&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static SectionAward&Proxy findOrCreate(array $attributes)
 * @phpstan-method static SectionAward&Proxy first(string $sortedField = 'id')
 * @phpstan-method static SectionAward&Proxy last(string $sortedField = 'id')
 * @phpstan-method static SectionAward&Proxy random(array $attributes = [])
 * @phpstan-method static SectionAward&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static SectionAward[]&Proxy[] all()
 * @phpstan-method static SectionAward[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static SectionAward[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static SectionAward[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method SectionAward&Proxy create(array|callable $attributes = [])
 */
class SectionAwardFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return SectionAward::class;
    }
}
