<?php
namespace App\Factory;

use App\Entity\SectionEntry;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<SectionEntry>
 *
 * @method static SectionEntry|Proxy createOne(array $attributes = [])
 * @method static SectionEntry[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static SectionEntry[]&Proxy[] createSequence(array|callable $sequence)
 * @method static SectionEntry|Proxy find(object|array|mixed $criteria)
 * @method static SectionEntry|Proxy findOrCreate(array $attributes)
 * @method static SectionEntry|Proxy first(string $sortedField = 'id')
 * @method static SectionEntry|Proxy last(string $sortedField = 'id')
 * @method static SectionEntry|Proxy random(array $attributes = [])
 * @method static SectionEntry|Proxy randomOrCreate(array $attributes = []))
 * @method static SectionEntry[]|Proxy[] all()
 * @method static SectionEntry[]|Proxy[] findBy(array $attributes)
 * @method static SectionEntry[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static SectionEntry[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static SectionEntriesRepository|RepositoryProxy repository()
 * @method SectionEntry|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static SectionEntry&Proxy createOne(array $attributes = [])
 * @phpstan-method static SectionEntry[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static SectionEntry[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static SectionEntry&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static SectionEntry&Proxy findOrCreate(array $attributes)
 * @phpstan-method static SectionEntry&Proxy first(string $sortedField = 'id')
 * @phpstan-method static SectionEntry&Proxy last(string $sortedField = 'id')
 * @phpstan-method static SectionEntry&Proxy random(array $attributes = [])
 * @phpstan-method static SectionEntry&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static SectionEntry[]&Proxy[] all()
 * @phpstan-method static SectionEntry[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static SectionEntry[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static SectionEntry[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method SectionEntry&Proxy create(array|callable $attributes = [])
 */
class SectionEntryFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return SectionEntry::class;
    }
}
