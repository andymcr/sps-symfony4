<?php
namespace App\Factory;

use App\Entity\StaticText;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<StaticText>
 *
 * @method static StaticText|Proxy createOne(array $attributes = [])
 * @method static StaticText[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static StaticText[]&Proxy[] createSequence(array|callable $sequence)
 * @method static StaticText|Proxy find(object|array|mixed $criteria)
 * @method static StaticText|Proxy findOrCreate(array $attributes)
 * @method static StaticText|Proxy first(string $sortedField = 'id')
 * @method static StaticText|Proxy last(string $sortedField = 'id')
 * @method static StaticText|Proxy random(array $attributes = [])
 * @method static StaticText|Proxy randomOrCreate(array $attributes = []))
 * @method static StaticText[]|Proxy[] all()
 * @method static StaticText[]|Proxy[] findBy(array $attributes)
 * @method static StaticText[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static StaticText[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static StaticTextRepository|RepositoryProxy repository()
 * @method StaticText|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static StaticText&Proxy createOne(array $attributes = [])
 * @phpstan-method static StaticText[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static StaticText[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static StaticText&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static StaticText&Proxy findOrCreate(array $attributes)
 * @phpstan-method static StaticText&Proxy first(string $sortedField = 'id')
 * @phpstan-method static StaticText&Proxy last(string $sortedField = 'id')
 * @phpstan-method static StaticText&Proxy random(array $attributes = [])
 * @phpstan-method static StaticText&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static StaticText[]&Proxy[] all()
 * @phpstan-method static StaticText[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static StaticText[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static StaticText[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method StaticText&Proxy create(array|callable $attributes = [])
 */
class StaticTextFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(),
            'content' => self::faker()->paragraph(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return StaticText::class;
    }
}
