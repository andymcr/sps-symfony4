<?php
namespace App\Factory;

use App\Entity\Talk;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Talk>
 *
 * @method static Talk|Proxy createOne(array $attributes = [])
 * @method static Talk[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Talk[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Talk|Proxy find(object|array|mixed $criteria)
 * @method static Talk|Proxy findOrCreate(array $attributes)
 * @method static Talk|Proxy first(string $sortedField = 'id')
 * @method static Talk|Proxy last(string $sortedField = 'id')
 * @method static Talk|Proxy random(array $attributes = [])
 * @method static Talk|Proxy randomOrCreate(array $attributes = []))
 * @method static Talk[]|Proxy[] all()
 * @method static Talk[]|Proxy[] findBy(array $attributes)
 * @method static Talk[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Talk[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static TalksRepository|RepositoryProxy repository()
 * @method Talk|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Talk&Proxy createOne(array $attributes = [])
 * @phpstan-method static Talk[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Talk[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Talk&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Talk&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Talk&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Talk&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Talk&Proxy random(array $attributes = [])
 * @phpstan-method static Talk&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Talk[]&Proxy[] all()
 * @phpstan-method static Talk[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Talk[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Talk[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Talk&Proxy create(array|callable $attributes = [])
 */
class TalkFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Talk::class;
    }
}
