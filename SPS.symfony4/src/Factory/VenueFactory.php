<?php
namespace App\Factory;

use App\Entity\Venue;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Venue>
 *
 * @method static Venue|Proxy createOne(array $attributes = [])
 * @method static Venue[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Venue[]&Proxy[] createSequence(array|callable $sequence)
 * @method static Venue|Proxy find(object|array|mixed $criteria)
 * @method static Venue|Proxy findOrCreate(array $attributes)
 * @method static Venue|Proxy first(string $sortedField = 'id')
 * @method static Venue|Proxy last(string $sortedField = 'id')
 * @method static Venue|Proxy random(array $attributes = [])
 * @method static Venue|Proxy randomOrCreate(array $attributes = []))
 * @method static Venue[]|Proxy[] all()
 * @method static Venue[]|Proxy[] findBy(array $attributes)
 * @method static Venue[]|Proxy[] randomSet(int $number, array $attributes = []))
 * @method static Venue[]|Proxy[] randomRange(int $min, int $max, array $attributes = []))
 * @method static VenuesRepository|RepositoryProxy repository()
 * @method Venue|Proxy create(array|callable $attributes = [])
 *
 * @phpstan-method static Venue&Proxy createOne(array $attributes = [])
 * @phpstan-method static Venue[]&Proxy[] createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static Venue[]&Proxy[] createSequence(array|callable $sequence)
 * @phpstan-method static Venue&Proxy find(object|array|mixed $criteria)
 * @phpstan-method static Venue&Proxy findOrCreate(array $attributes)
 * @phpstan-method static Venue&Proxy first(string $sortedField = 'id')
 * @phpstan-method static Venue&Proxy last(string $sortedField = 'id')
 * @phpstan-method static Venue&Proxy random(array $attributes = [])
 * @phpstan-method static Venue&Proxy randomOrCreate(array $attributes = [])
 * @phpstan-method static Venue[]&Proxy[] all()
 * @phpstan-method static Venue[]&Proxy[] findBy(array $attributes)
 * @phpstan-method static Venue[]&Proxy[] randomSet(int $number, array $attributes = [])
 * @phpstan-method static Venue[]&Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method Venue&Proxy create(array|callable $attributes = [])
 */
class VenueFactory extends ModelFactory
{

    /**
     * @inheritdoc ModelFactory
     * 
     * @return array<string, mixed>
     **/
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(),
        ];
    }


    /**
     * @link ModelFactory
     */
    protected static function getClass(): string
    {
        return Venue::class;
    }
}
