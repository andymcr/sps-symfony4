<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AuthenticationUpdateType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('credentialsExpireAt', DateTimeType::class, [
            'label' => 'authentication.credentials_expire_at.caption',
            'translation_domain' => 'entity',
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'attr' => [
                'placeholder' => 'authentication.credentials_expire_at.caption',
                'title' => 'authentication.credentials_expire_at.caption',
            ],
        ]);

        $builder->add('credentialsExpired', CheckboxType::class, [
            'label' => 'authentication.credentials_expired.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.credentials_expired.caption',
                'title' => 'authentication.credentials_expired.caption',
            ],
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'authentication.email.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.email.caption',
                'title' => 'authentication.email.caption',
            ],
        ]);

        $builder->add('emailVerified', CheckboxType::class, [
            'label' => 'authentication.email_verified.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.email_verified.caption',
                'title' => 'authentication.email_verified.caption',
            ],
        ]);

        $builder->add('enabled', CheckboxType::class, [
            'label' => 'authentication.enabled.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.enabled.caption',
                'title' => 'authentication.enabled.caption',
            ],
        ]);

        $builder->add('expired', CheckboxType::class, [
            'label' => 'authentication.expired.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.expired.caption',
                'title' => 'authentication.expired.caption',
            ],
        ]);

        $builder->add('expiresAt', DateTimeType::class, [
            'label' => 'authentication.expires_at.caption',
            'translation_domain' => 'entity',
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'attr' => [
                'placeholder' => 'authentication.expires_at.caption',
                'title' => 'authentication.expires_at.caption',
            ],
        ]);

        $builder->add('lastLogin', DateTimeType::class, [
            'label' => 'authentication.last_login.caption',
            'translation_domain' => 'entity',
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'attr' => [
                'placeholder' => 'authentication.last_login.caption',
                'title' => 'authentication.last_login.caption',
            ],
        ]);

        $builder->add('localed', CheckboxType::class, [
            'label' => 'authentication.localed.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.localed.caption',
                'title' => 'authentication.localed.caption',
            ],
        ]);

        $builder->add('passwordChangedAt', DateTimeType::class, [
            'label' => 'authentication.password_changed_at.caption',
            'translation_domain' => 'entity',
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'attr' => [
                'placeholder' => 'authentication.password_changed_at.caption',
                'title' => 'authentication.password_changed_at.caption',
            ],
        ]);

        $builder->add('roles', CollectionType::class, [
            'label' => 'authentication.roles.caption',
            'translation_domain' => 'entity',
            'entry_type' => TextType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'required' => false,
        ]);

        $builder->add('username', TextType::class, [
            'label' => 'authentication.username.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.username.caption',
                'title' => 'authentication.username.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Authentication',
            'csrf_token_id' => 'manage_authentications_update'
        ]);
    }
}
