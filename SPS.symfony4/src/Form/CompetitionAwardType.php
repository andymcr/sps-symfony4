<?php
namespace App\Form;

use App\Entity\AwardsCompetition;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CompetitionAwardType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('award', ChoiceType::class, [
            'label' => 'competition_award.award.caption',
            'translation_domain' => 'entity',
            'choices' => AwardsCompetition::values(),
            'placeholder' => 'actions.labels.select_one',
            'attr' => [
                'placeholder' => 'competition_award.award.caption',
                'title' => 'competition_award.award.caption',
            ],
        ]);

        $builder->add('entry', EntityType::class, [
            'label' => 'competition_award.entry.caption',
            'translation_domain' => 'entity',
            'class' => 'App\Entity\SectionEntry',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CompetitionAward',
            'csrf_token_id' => 'manage_competitions_edit_award'
        ]);
    }
}
