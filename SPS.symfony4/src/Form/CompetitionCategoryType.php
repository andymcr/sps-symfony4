<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CompetitionCategoryType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('fullName', TextType::class, [
            'label' => 'competition_category.full_name.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'competition_category.full_name.caption',
                'title' => 'competition_category.full_name.caption',
            ],
        ]);

        $builder->add('rank', IntegerType::class, [
            'label' => 'competition_category.rank.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition_category.rank.caption',
                'title' => 'competition_category.rank.caption',
            ],
        ]);

        $builder->add('shortName', TextType::class, [
            'label' => 'competition_category.short_name.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'competition_category.short_name.caption',
                'title' => 'competition_category.short_name.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CompetitionCategory',
            'csrf_token_id' => 'manage_competitions_edit_category'
        ]);
    }
}
