<?php
namespace App\Form;

use App\Entity\CompetitionSection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use Symfony\UX\Autocomplete\Form\ParentEntityAutocompleteType;


#[AsEntityAutocompleteField]
class CompetitionSectionAutocompleteType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => 'cumulative_competition.from_sections.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => CompetitionSection::class,
            'choice_label' => 'competitionAndSectionLabel',
            'choice_translation_domain' => false,
            'multiple' => true,
            'searchable_fields' => ['partOf.event.title', 'title'],
            'security' => 'ROLE_COMPETITION_SECRETARY',
        ]);
    }

    public function getParent(): string
    {
        return ParentEntityAutocompleteType::class;
    }
}
