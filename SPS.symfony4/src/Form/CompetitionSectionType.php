<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CompetitionSectionType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('acceptanceScore', IntegerType::class, [
            'label' => 'competition_section.acceptance_score.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition_section.acceptance_score.caption',
                'title' => 'competition_section.acceptance_score.caption',
            ],
        ]);

        $builder->add('maximumEntries', IntegerType::class, [
            'label' => 'manage_competitions.edit_section.maximum_entries.caption',
            'translation_domain' => 'admin',
            'required' => false,
            'attr' => [
                'placeholder' => 'manage_competitions.edit_section.maximum_entries.placeholder',
                'title' => 'manage_competitions.edit_section.maximum_entries.title',
            ],
        ]);

        $builder->add('order', IntegerType::class, [
            'label' => 'competition_section.order.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition_section.order.caption',
                'title' => 'competition_section.order.caption',
            ],
        ]);

        $builder->add('title', TextType::class, [
            'label' => 'competition_section.title.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'competition_section.title.caption',
                'title' => 'competition_section.title.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CompetitionSection',
            'csrf_token_id' => 'manage_competitions_edit_section'
        ]);
    }
}
