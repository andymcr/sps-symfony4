<?php
namespace App\Form;

use App\Repository\EventsRepository;
use App\Repository\PeopleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CompetitionType extends AbstractType
{
    /**
     * @var EventsRepository $eventsRepository
     */
    private $eventsRepository;

    /**
     * @var PeopleRepository $peopleRepository
     */
    private $peopleRepository;

    public function __construct(EventsRepository $eventsRepository, PeopleRepository $peopleRepository)
    {
        $this->eventsRepository = $eventsRepository;
        $this->peopleRepository = $peopleRepository;
    }

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('category', EntityType::class, [
            'label' => 'competition.category.caption',
            'translation_domain' => 'entity',
            'class' => 'App\Entity\CompetitionCategory',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('event', EntityType::class, [
            'label' => 'competition.event.caption',
            'translation_domain' => 'entity',
            'class' => 'App\Entity\Event',
            'choices' => $this->eventsRepository->findAll(),
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('image', ImageVariantAutocompleteType::class);;

        $builder->add('judges', EntityType::class, [
            'label' => 'competition.judges.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\Person',
            'choices' => $this->peopleRepository->findAll(),
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'multiple' => true,
        ]);

        $builder->add('maximumScore', IntegerType::class, [
            'label' => 'competition.maximum_score.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition.maximum_score.caption',
                'title' => 'competition.maximum_score.caption',
            ],
        ]);

        $builder->add('publicAuthors', CheckboxType::class, [
            'label' => 'competition.public_authors.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition.public_authors.caption',
                'title' => 'competition.public_authors.caption',
            ],
        ]);

        $builder->add('publicResults', CheckboxType::class, [
            'label' => 'competition.public_results.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition.public_results.caption',
                'title' => 'competition.public_results.caption',
            ],
        ]);

        $builder->add('secretary', EntityType::class, [
            'label' => 'competition.secretary.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\Person',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('submissionDeadline', DateType::class, [
            'label' => 'manage_competitions.edit_competition.submission_deadline.caption',
            'translation_domain' => 'admin',
            'required' => false,
            'widget' => 'single_text',
            'attr' => [
                'placeholder' => 'manage_competitions.edit_competition.submission_deadline.placeholder',
                'title' => 'manage_competitions.edit_competition.submission_deadline.title',
            ],
        ]);

        $builder->add('website', TextType::class, [
            'label' => 'competition.website.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'competition.website.caption',
                'title' => 'competition.website.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Competition',
            'csrf_token_id' => 'manage_competitions_edit_competition'
        ]);
    }
}
