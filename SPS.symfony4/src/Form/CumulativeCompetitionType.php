<?php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CumulativeCompetitionType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('fromSections', CompetitionSectionAutocompleteType::class);;

        $builder->add('name', TextType::class, [
            'label' => 'cumulative_competition.name.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'cumulative_competition.name.caption',
                'title' => 'cumulative_competition.name.caption',
            ],
        ]);

        $builder->add('private', CheckboxType::class, [
            'label' => 'cumulative_competition.private.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'cumulative_competition.private.caption',
                'title' => 'cumulative_competition.private.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CumulativeCompetition',
            'csrf_token_id' => 'manage_cumulative_edit_competition'
        ]);
    }
}
