<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class DistinctionType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('rank', IntegerType::class, [
            'label' => 'distinction.rank.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'distinction.rank.caption',
                'title' => 'distinction.rank.caption',
            ],
        ]);

        $builder->add('title', TextType::class, [
            'label' => 'distinction.title.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'distinction.title.caption',
                'title' => 'distinction.title.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Distinction',
            'csrf_token_id' => 'manage_people_edit_distinction'
        ]);
    }
}
