<?php
namespace App\Form;

use App\Entity\EventCategories;
use App\Repository\OrganisationsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EventType extends AbstractType
{
    /**
     * @var OrganisationsRepository $organisationsRepository
     */
    private $organisationsRepository;

    public function __construct(OrganisationsRepository $organisationsRepository)
    {
        $this->organisationsRepository = $organisationsRepository;
    }

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('cancelled', CheckboxType::class, [
            'label' => 'event.cancelled.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'event.cancelled.caption',
                'title' => 'event.cancelled.caption',
            ],
        ]);

        $builder->add('category', ChoiceType::class, [
            'label' => 'event.category.caption',
            'translation_domain' => 'entity',
            'choices' => EventCategories::values(),
            'placeholder' => 'actions.labels.select_one',
            'attr' => [
                'placeholder' => 'event.category.caption',
                'title' => 'event.category.caption',
            ],
        ]);

        $builder->add('draft', CheckboxType::class, [
            'label' => 'event.draft.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'event.draft.caption',
                'title' => 'event.draft.caption',
            ],
        ]);

        $builder->add('duration', TimeType::class, [
            'label' => 'event.duration.caption',
            'translation_domain' => 'entity',
            'widget' => 'single_text',
            'with_seconds' => TRUE,
            'attr' => [
                'placeholder' => 'event.duration.caption',
                'title' => 'event.duration.caption',
            ],
        ]);

        $builder->add('organisedBy', EntityType::class, [
            'label' => 'event.organised_by.caption',
            'translation_domain' => 'entity',
            'class' => 'App\Entity\Organisation',
            'choices' => $this->organisationsRepository->findAll(),
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('postponed', CheckboxType::class, [
            'label' => 'event.postponed.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'event.postponed.caption',
                'title' => 'event.postponed.caption',
            ],
        ]);

        $builder->add('publicise', CheckboxType::class, [
            'label' => 'event.publicise.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'event.publicise.caption',
                'title' => 'event.publicise.caption',
            ],
        ]);

        $builder->add('start', DateTimeType::class, [
            'label' => 'event.start.caption',
            'translation_domain' => 'entity',
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'attr' => [
                'placeholder' => 'event.start.caption',
                'title' => 'event.start.caption',
            ],
        ]);

        $builder->add('title', TextType::class, [
            'label' => 'event.title.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'event.title.caption',
                'title' => 'event.title.caption',
            ],
        ]);

        $builder->add('venue', EntityType::class, [
            'label' => 'event.venue.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\Venue',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Event',
            'csrf_token_id' => 'manage_events_edit_event'
        ]);
    }
}
