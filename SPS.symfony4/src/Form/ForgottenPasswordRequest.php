<?php
namespace App\Form;

use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ForgottenPasswordRequest extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('email', EmailType::class, [
            'mapped' => false,
            'label' => 'authentication.email.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'authentication.email.caption',
                'title' => 'authentication.email.caption',
            ],
        ]);

        $builder->add('captcha', Recaptcha3Type::class, [
            'mapped' => false,
            'constraints' => new Recaptcha3([
                'message' => 'karser_recaptcha3.message',
                'messageMissingValue' => 'karser_recaptcha3.message_missing_value',
            ]),
            'action_name' => 'forgotten',
        //    'script_nonce_csp' => $nonceCSP,
        ]);
    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_token_id'   => 'authentication_forgotten',
        ]);
    }
}
