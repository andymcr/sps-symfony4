<?php
namespace App\Form;

use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;


class ForgottenPasswordReset extends AbstractType
{
    /**
     * @var TranslatorInterface $translator
     */
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('plainPassword', RepeatedType::class, [
            'mapped' => false,
            'type' => PasswordType::class,
            'translation_domain' => 'authentication',
            'required' => true,
            'options' => [
                'row_attr' => [ 'class' => 'px-2 mb-3' ],
            ],
            'first_options' => [
                'label' => 'forgotten.password.caption',
                'attr' => [
                    'placeholder' => 'forgotten.password.placeholder',
                    'title' => 'forgotten.password.title',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => $this->translator->trans('forgotten.password.messages.required', [], 'authentication'),
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ],
            'second_options' => [
                'label' => 'forgotten.password_second.caption',
                'attr' => [
                    'placeholder' => 'forgotten.password_second.placeholder',
                    'title' => 'forgotten.password_second.title',
                ],
            ],
            'invalid_message' => $this->translator->trans('forgotten.password.messages.mismatch', [], 'authentication'),
        ]);

        $builder->add('captcha', Recaptcha3Type::class, [
            'mapped' => false,
            'constraints' => new Recaptcha3([
                'message' => 'karser_recaptcha3.message',
                'messageMissingValue' => 'karser_recaptcha3.message_missing_value',
            ]),
            'action_name' => 'forgotten',
        //    'script_nonce_csp' => $nonceCSP,
        ]);
      }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_token_id'   => 'authentication_forgotten',
        ]);
    }
}
