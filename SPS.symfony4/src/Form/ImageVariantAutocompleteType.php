<?php
namespace App\Form;

use App\Entity\ImageVariant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use Symfony\UX\Autocomplete\Form\ParentEntityAutocompleteType;


#[AsEntityAutocompleteField]
class ImageVariantAutocompleteType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => 'competition.image.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => ImageVariant::class,
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
            'searchable_fields' => ['image.author.username', 'image.title', 'description'],
            'security' => 'ROLE_COMPETITION_SECRETARY',
        ]);
    }

    public function getParent(): string
    {
        return ParentEntityAutocompleteType::class;
    }
}
