<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ImageVariantType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('clubShowcase', CheckboxType::class, [
            'label' => 'image_variant.club_showcase.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'image_variant.club_showcase.caption',
                'title' => 'image_variant.club_showcase.caption',
            ],
        ]);

        $builder->add('description', TextType::class, [
            'label' => 'image_variant.description.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'image_variant.description.caption',
                'title' => 'image_variant.description.caption',
            ],
        ]);

        $builder->add('filePath', TextType::class, [
            'label' => 'image_variant.file_path.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'image_variant.file_path.caption',
                'title' => 'image_variant.file_path.caption',
            ],
        ]);

        $builder->add('personalShowcase', CheckboxType::class, [
            'label' => 'image_variant.personal_showcase.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'image_variant.personal_showcase.caption',
                'title' => 'image_variant.personal_showcase.caption',
            ],
        ]);

        $builder->add('photo', CheckboxType::class, [
            'label' => 'image_variant.photo.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'image_variant.photo.caption',
                'title' => 'image_variant.photo.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ImageVariant',
            'csrf_token_id' => 'manage_people_edit_variants'
        ]);
    }
}
