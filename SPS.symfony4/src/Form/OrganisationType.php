<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OrganisationType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('shortTitle', TextType::class, [
            'label' => 'organisation.short_title.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'organisation.short_title.caption',
                'title' => 'organisation.short_title.caption',
            ],
        ]);

        $builder->add('title', TextType::class, [
            'label' => 'organisation.title.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'organisation.title.caption',
                'title' => 'organisation.title.caption',
            ],
        ]);

        $builder->add('website', TextType::class, [
            'label' => 'organisation.website.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'organisation.website.caption',
                'title' => 'organisation.website.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Organisation',
            'csrf_token_id' => 'manage_organisations_edit'
        ]);
    }
}
