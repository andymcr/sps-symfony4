<?php
namespace App\Form;

use App\Repository\DistinctionsRepository;
use App\Repository\OrganisationsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PersonType extends AbstractType
{
    /**
     * @var DistinctionsRepository $distinctionsRepository
     */
    private $distinctionsRepository;

    /**
     * @var OrganisationsRepository $organisationsRepository
     */
    private $organisationsRepository;

    public function __construct(DistinctionsRepository $distinctionsRepository, OrganisationsRepository $organisationsRepository)
    {
        $this->distinctionsRepository = $distinctionsRepository;
        $this->organisationsRepository = $organisationsRepository;
    }

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('distinctions', EntityType::class, [
            'label' => 'person.distinctions.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\Distinction',
            'choices' => $this->distinctionsRepository->findAll(),
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'multiple' => true,
        ]);

        $builder->add('familyName', TextType::class, [
            'label' => 'person.family_name.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'person.family_name.caption',
                'title' => 'person.family_name.caption',
            ],
        ]);

        $builder->add('forenames', TextType::class, [
            'label' => 'person.forenames.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'person.forenames.caption',
                'title' => 'person.forenames.caption',
            ],
        ]);

        $builder->add('memberOf', EntityType::class, [
            'label' => 'person.member_of.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\Organisation',
            'choices' => $this->organisationsRepository->findAll(),
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('salutation', TextType::class, [
            'label' => 'person.salutation.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'person.salutation.caption',
                'title' => 'person.salutation.caption',
            ],
        ]);

        $builder->add('showcaseImage', EntityType::class, [
            'label' => 'person.showcase_image.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\ImageVariant',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('username', EmailType::class, [
            'label' => 'person.username.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'person.username.caption',
                'title' => 'person.username.caption',
            ],
        ]);

        $builder->add('website', TextType::class, [
            'label' => 'person.website.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'person.website.caption',
                'title' => 'person.website.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Person',
            'csrf_token_id' => 'manage_people_edit_person'
        ]);
    }
}
