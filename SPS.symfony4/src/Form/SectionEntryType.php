<?php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SectionEntryType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('award', EntityType::class, [
            'label' => 'section_entry.award.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\SectionAward',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('disqualified', CheckboxType::class, [
            'label' => 'section_entry.disqualified.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.disqualified.caption',
                'title' => 'section_entry.disqualified.caption',
            ],
        ]);

        $builder->add('entryNumber', IntegerType::class, [
            'label' => 'section_entry.entry_number.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.entry_number.caption',
                'title' => 'section_entry.entry_number.caption',
            ],
        ]);

        $builder->add('imageVariant', ImageVariantAutocompleteType::class);;

        $builder->add('points', IntegerType::class, [
            'label' => 'section_entry.points.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.points.caption',
                'title' => 'section_entry.points.caption',
            ],
        ]);

        $builder->add('removalOrder', IntegerType::class, [
            'label' => 'section_entry.removal_order.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.removal_order.caption',
                'title' => 'section_entry.removal_order.caption',
            ],
        ]);

        $builder->add('removed', CheckboxType::class, [
            'label' => 'section_entry.removed.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.removed.caption',
                'title' => 'section_entry.removed.caption',
            ],
        ]);

        $builder->add('score', TextType::class, [
            'label' => 'section_entry.score.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.score.caption',
                'title' => 'section_entry.score.caption',
            ],
        ]);

        $builder->add('tagged', CheckboxType::class, [
            'label' => 'section_entry.tagged.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.tagged.caption',
                'title' => 'section_entry.tagged.caption',
            ],
        ]);

        $builder->add('title', TextType::class, [
            'label' => 'section_entry.title.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'section_entry.title.caption',
                'title' => 'section_entry.title.caption',
            ],
        ]);

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (is_null($data->getTitle())) {
                $data->setTitle($data->getImageVariant()->getImage()->getTitle());
            }
        });
    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\SectionEntry',
            'csrf_token_id' => 'manage_entries_edit_section_entry'
        ]);
    }
}
