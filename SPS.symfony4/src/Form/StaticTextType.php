<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class StaticTextType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('content', TextareaType::class, [
            'label' => 'static_text.content.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'static_text.content.caption',
                'title' => 'static_text.content.caption',
            ],
        ]);

        $builder->add('name', TextType::class, [
            'label' => 'static_text.name.caption',
            'translation_domain' => 'entity',
            'attr' => [
                'placeholder' => 'static_text.name.caption',
                'title' => 'static_text.name.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\StaticText',
            'csrf_token_id' => 'manage_static_text_edit'
        ]);
    }
}
