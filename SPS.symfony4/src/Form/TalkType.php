<?php
namespace App\Form;

use App\Repository\EventsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TalkType extends AbstractType
{
    /**
     * @var EventsRepository $eventsRepository
     */
    private $eventsRepository;

    public function __construct(EventsRepository $eventsRepository)
    {
        $this->eventsRepository = $eventsRepository;
    }

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('event', EntityType::class, [
            'label' => 'talk.event.caption',
            'translation_domain' => 'entity',
            'class' => 'App\Entity\Event',
            'choices' => $this->eventsRepository->findAll(),
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'placeholder' => 'actions.labels.select_one',
        ]);

        $builder->add('presenters', EntityType::class, [
            'label' => 'talk.presenters.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'class' => 'App\Entity\Person',
            'choice_label' => 'defaultLabel',
            'choice_translation_domain' => false,
            'multiple' => true,
        ]);

        $builder->add('synopsis', TextareaType::class, [
            'label' => 'talk.synopsis.caption',
            'translation_domain' => 'entity',
            'required' => false,
            'attr' => [
                'placeholder' => 'talk.synopsis.caption',
                'title' => 'talk.synopsis.caption',
            ],
        ]);

    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Talk',
            'csrf_token_id' => 'manage_talks_edit'
        ]);
    }
}
