<?php
namespace App\Repository;

use App\Entity\Authentication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;


/**
 * @extends ServiceEntityRepository<Authentication>
 */
class AuthenticationRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Authentication::class);
    }

	/**
	 * @see PasswordUpgraderInterface
	 */
	public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
	{
	    if (!$user instanceof Authentication) {
	        return;
	    }
	
	    $user->setPassword($newHashedPassword);
	    $this->save($user, true);
	}

    /**
     *
     * @param string $username
     * @param bool $enabled
     * @param string $email
     * @param bool $emailVerified
     * @param string $password
     * @param bool $localed
     * @param bool $expired
     * @param \DateTimeInterface $expiresAt
     * @param \DateTimeInterface $passwordChangedAt
     * @param bool $credentialsExpired
     * @param \DateTimeInterface $credentialsExpireAt
     * @param \DateTimeInterface $lastLogin
     * @param bool $flush
     *
     * @return Authentication
     */
    public function create(string $username, bool $enabled, string $email, bool $emailVerified, string $password, bool $localed, bool $expired, \DateTimeInterface $expiresAt, \DateTimeInterface $passwordChangedAt, bool $credentialsExpired, \DateTimeInterface $credentialsExpireAt, \DateTimeInterface $lastLogin, $flush = false): Authentication
    {
        $new = new Authentication();
        $new->setUsername($username);
        $new->setEnabled($enabled);
        $new->setEmail($email);
        $new->setEmailVerified($emailVerified);
        $new->setPassword($password);
        $new->setLocaled($localed);
        $new->setExpired($expired);
        $new->setExpiresAt($expiresAt);
        $new->setPasswordChangedAt($passwordChangedAt);
        $new->setCredentialsExpired($credentialsExpired);
        $new->setCredentialsExpireAt($credentialsExpireAt);
        $new->setLastLogin($lastLogin);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $email
     *
     * @return Authentication|null
     */
    public function findOne(string $email): ?Authentication
    {
        $queryBuilder = $this->createQueryBuilder('authentication');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('authentication.email', ':email'))
            ->setParameter('email', $email)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('authentication')->getQuery();
	}

    /**
     *
     * @param Authentication $entity
     * @param bool $flush
     */
    public function remove(Authentication $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Authentication $entity
     * @param bool $flush
     */
    public function save(Authentication $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
