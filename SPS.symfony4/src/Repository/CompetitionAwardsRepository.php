<?php
namespace App\Repository;

use App\Entity\CompetitionAward;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<CompetitionAward>
 */
class CompetitionAwardsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CompetitionAward::class);
    }

    /**
     *
     * @param int $award
     * @param bool $flush
     *
     * @return CompetitionAward
     */
    public function create(int $award, $flush = false): CompetitionAward
    {
        $new = new CompetitionAward();
        $new->setAward($award);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $title
     * @param int $award
     *
     * @return CompetitionAward|null
     */
    public function findOne(string $title, int $award): ?CompetitionAward
    {
        $queryBuilder = $this->createQueryBuilder('competitionAward');
        return $queryBuilder
            ->join('competitionAward.awardFor', 'competition')
            ->join('competition.event', 'event')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('event.title', ':title'),
                $queryBuilder->expr()->eq('competitionAward.award', ':award')
                )
            )
            ->setParameter('title', $title)
            ->setParameter('award', $award)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('competitionAward')->getQuery();
	}

    /**
     *
     
     *
     * @return Query
     */
    public function findAwardsDecendingQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionAward');
        $queryBuilder->orderBy('competitionAward.award', 'DESC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, CompetitionAward>
     */
    public function findAwardsDecending(): array
    {
        return $this->findAwardsDecendingQuery()->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findOfCompetitionQuery($title): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionAward');
        $queryBuilder->join('competitionAward.entry', 'sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionAward.awardFor', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->eq('event.title', ':title'));
        $queryBuilder->orderBy('competitionSection.order', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return array<int, CompetitionAward>
     */
    public function findOfCompetition($title): array
    {
        return $this->findOfCompetitionQuery($title)->getResult();
    }

    /**
     *
     * @param CompetitionAward $entity
     * @param bool $flush
     */
    public function remove(CompetitionAward $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param CompetitionAward $entity
     * @param bool $flush
     */
    public function save(CompetitionAward $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
