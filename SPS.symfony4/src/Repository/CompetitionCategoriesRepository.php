<?php
namespace App\Repository;

use App\Entity\CompetitionCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<CompetitionCategory>
 */
class CompetitionCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CompetitionCategory::class);
    }

    /**
     *
     * @param string $shortName
     * @param string $fullName
     * @param int|null $rank
     * @param bool $flush
     *
     * @return CompetitionCategory
     */
    public function create(string $shortName, string $fullName, int $rank = null, $flush = false): CompetitionCategory
    {
        $new = new CompetitionCategory();
        $new->setShortName($shortName);
        $new->setFullName($fullName);
        $new->setRank($rank);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $shortName
     *
     * @return CompetitionCategory|null
     */
    public function findOne(string $shortName): ?CompetitionCategory
    {
        $queryBuilder = $this->createQueryBuilder('competitionCategory');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('competitionCategory.shortName', ':shortName'))
            ->setParameter('shortName', $shortName)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionCategory');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, CompetitionCategory>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }

    /**
     *
     * @param CompetitionCategory $entity
     * @param bool $flush
     */
    public function remove(CompetitionCategory $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param CompetitionCategory $entity
     * @param bool $flush
     */
    public function save(CompetitionCategory $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
