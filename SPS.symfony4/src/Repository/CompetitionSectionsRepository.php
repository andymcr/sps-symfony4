<?php
namespace App\Repository;

use App\Entity\CompetitionSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<CompetitionSection>
 */
class CompetitionSectionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CompetitionSection::class);
    }

    /**
     *
     * @param string $title
     * @param int|null $order
     * @param int|null $maximumEntries
     * @param int|null $acceptanceScore
     * @param bool $flush
     *
     * @return CompetitionSection
     */
    public function create(string $title, int $order = null, int $maximumEntries = null, int $acceptanceScore = null, $flush = false): CompetitionSection
    {
        $new = new CompetitionSection();
        $new->setTitle($title);
        $new->setOrder($order);
        $new->setMaximumEntries($maximumEntries);
        $new->setAcceptanceScore($acceptanceScore);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $eventTitle
     * @param string $competitionSectionTitle
     *
     * @return CompetitionSection|null
     */
    public function findOne(string $eventTitle, string $competitionSectionTitle): ?CompetitionSection
    {
        $queryBuilder = $this->createQueryBuilder('competitionSection');
        return $queryBuilder
            ->join('competitionSection.partOf', 'competition')
            ->join('competition.event', 'event')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('event.title', ':eventTitle'),
                $queryBuilder->expr()->eq('competitionSection.title', ':competitionSectionTitle')
                )
            )
            ->setParameter('eventTitle', $eventTitle)
            ->setParameter('competitionSectionTitle', $competitionSectionTitle)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('competitionSection')->getQuery();
	}

    /**
     *
     
     *
     * @return Query
     */
    public function findOrderAscendingQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionSection');
        $queryBuilder->orderBy('competitionSection.order', 'ASC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, CompetitionSection>
     */
    public function findOrderAscending(): array
    {
        return $this->findOrderAscendingQuery()->getResult();
    }

    /**
     *
     * @param string $cumulativeGroupName
     * @param string $cumulativeCompetitionName
     *
     * @return Query
     */
    public function findPointsCompetitionSectionsQuery($cumulativeGroupName, $cumulativeCompetitionName): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionSection');
        $queryBuilder->join('competitionSection.contributesTo', 'cumulativeCompetition');
        $queryBuilder->join('cumulativeCompetition.partOf', 'cumulativeGroup');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('cumulativeGroup.name', ':cumulativeGroupName'),
            $queryBuilder->expr()->eq('cumulativeCompetition.name', ':cumulativeCompetitionName')
        ));
        $queryBuilder->orderBy('competitionSection.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('cumulativeGroupName', $cumulativeGroupName);
        $query->setParameter('cumulativeCompetitionName', $cumulativeCompetitionName);
    
        return $query;
    }
    
    /**
     *
     * @param string $cumulativeGroupName
     * @param string $cumulativeCompetitionName
     *
     * @return array<int, CompetitionSection>
     */
    public function findPointsCompetitionSections($cumulativeGroupName, $cumulativeCompetitionName): array
    {
        return $this->findPointsCompetitionSectionsQuery($cumulativeGroupName, $cumulativeCompetitionName)->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findOfCompetitionQuery($title): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->eq('event.title', ':title'));
        $queryBuilder->orderBy('competitionSection.order', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return array<int, CompetitionSection>
     */
    public function findOfCompetition($title): array
    {
        return $this->findOfCompetitionQuery($title)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findAllSectionsQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->orderBy('event.start', 'DESC');
        $queryBuilder->addOrderBy('competitionSection.title', 'ASC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, CompetitionSection>
     */
    public function findAllSections(): array
    {
        return $this->findAllSectionsQuery()->getResult();
    }

    /**
     *
     * @param CompetitionSection $entity
     * @param bool $flush
     */
    public function remove(CompetitionSection $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param CompetitionSection $entity
     * @param bool $flush
     */
    public function save(CompetitionSection $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
