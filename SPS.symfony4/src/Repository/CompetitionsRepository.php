<?php
namespace App\Repository;

use App\Entity\Competition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<Competition>
 */
class CompetitionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Competition::class);
    }

    /**
     *
     * @param string|null $website
     * @param int|null $maximumScore
     * @param bool|null $publicAuthors
     * @param bool|null $publicResults
     * @param \DateTimeInterface|null $submissionDeadline
     * @param bool $flush
     *
     * @return Competition
     */
    public function create(string $website = null, int $maximumScore = null, bool $publicAuthors = null, bool $publicResults = null, \DateTimeInterface $submissionDeadline = null, $flush = false): Competition
    {
        $new = new Competition();
        $new->setWebsite($website);
        $new->setMaximumScore($maximumScore);
        $new->setPublicAuthors($publicAuthors);
        $new->setPublicResults($publicResults);
        $new->setSubmissionDeadline($submissionDeadline);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $title
     *
     * @return Competition|null
     */
    public function findOne(string $title): ?Competition
    {
        $queryBuilder = $this->createQueryBuilder('competition');
        return $queryBuilder
            ->join('competition.event', 'event')
            ->where($queryBuilder->expr()->eq('event.title', ':title'))
            ->setParameter('title', $title)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->join('competition.category', 'competitionCategory');
        $queryBuilder->orderBy('event.start', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Competition>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $competitionTitle
     *
     * @return Query
     */
    public function findWithTitleQuery(?string $competitionTitle): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('event.title', ':competitionTitle'));
        $query = $queryBuilder->getQuery();
        if (is_null($competitionTitle) || strcmp(trim($competitionTitle), '') === 0) {
            $query->setParameter('competitionTitle', '%');
        } else {
            $query->setParameter('competitionTitle', strpos($competitionTitle, '%') ? $competitionTitle : '%' . $competitionTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $competitionTitle
     *
     * @return array<int, Competition>
     */
    public function findWithTitle(?string $competitionTitle): array
    {
        return $this->findWithTitleQuery($competitionTitle)->getResult();
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findBPEQuery(): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->eq('competitionCategory.shortName', $queryBuilder->expr()->literal('BPE')));
        $query = $queryBuilder->getQuery();
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Competition>
     */
    public function findBPE(): array
    {
        return $this->findBPEQuery()->getResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function pastBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->join('competition.category', 'competitionCategory');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->neq('event.draft', 1),
            $queryBuilder->expr()->lt('event.start', ':now')
        ));
        $queryBuilder->orderBy('event.start', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     */
    private function pastParameters(Query $query) : void
    {
        $query->setParameter('now', new \DateTime('now'));
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findPastQuery(): Query
    {
        $query = $this->pastBuilder()->getQuery();
        $this->pastParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Competition>
     */
    public function findPast(): array
    {
        return $this->findPastQuery()->getResult();
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findPastPublicQuery(): Query
    {
        $queryBuilder = $this->pastBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->eq('competition.publicResults', 1));
        $query = $queryBuilder->getQuery();
        $this->pastParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Competition>
     */
    public function findPastPublic(): array
    {
        return $this->findPastPublicQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $competitionTitle
     *
     * @return Query
     */
    public function findPastPublicWithTitleQuery(?string $competitionTitle): Query
    {
        $queryBuilder = $this->pastBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->andX(
            $queryBuilder->expr()->like('event.title', ':competitionTitle'),
            $queryBuilder->expr()->eq('competition.publicResults', 1)
        ));
        $query = $queryBuilder->getQuery();
        $this->pastParameters($query);
        if (is_null($competitionTitle) || strcmp(trim($competitionTitle), '') === 0) {
            $query->setParameter('competitionTitle', '%');
        } else {
            $query->setParameter('competitionTitle', strpos($competitionTitle, '%') ? $competitionTitle : '%' . $competitionTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $competitionTitle
     *
     * @return array<int, Competition>
     */
    public function findPastPublicWithTitle(?string $competitionTitle): array
    {
        return $this->findPastPublicWithTitleQuery($competitionTitle)->getResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function futureBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->neq('event.draft', 1),
            $queryBuilder->expr()->gt('event.start', ':now')
        ));
        $queryBuilder->orderBy('competition.submissionDeadline', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     */
    private function futureParameters(Query $query) : void
    {
        $query->setParameter('now', new \DateTime('now'));
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findFutureQuery(): Query
    {
        $query = $this->futureBuilder()->getQuery();
        $this->futureParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Competition>
     */
    public function findFuture(): array
    {
        return $this->findFutureQuery()->getResult();
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findFuturePublicQuery(): Query
    {
        $queryBuilder = $this->futureBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->eq('event.publicise', 1));
        $query = $queryBuilder->getQuery();
        $this->futureParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Competition>
     */
    public function findFuturePublic(): array
    {
        return $this->findFuturePublicQuery()->getResult();
    }

    /**
     *
     * @param Competition $entity
     * @param bool $flush
     */
    public function remove(Competition $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Competition $entity
     * @param bool $flush
     */
    public function save(Competition $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
