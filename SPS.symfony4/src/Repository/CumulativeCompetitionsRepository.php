<?php
namespace App\Repository;

use App\Entity\CumulativeCompetition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<CumulativeCompetition>
 */
class CumulativeCompetitionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CumulativeCompetition::class);
    }

    /**
     *
     * @param string $name
     * @param bool|null $private
     * @param bool $flush
     *
     * @return CumulativeCompetition
     */
    public function create(string $name, bool $private = null, $flush = false): CumulativeCompetition
    {
        $new = new CumulativeCompetition();
        $new->setName($name);
        $new->setPrivate($private);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $cumulativeGroupName
     * @param string $cumulativeCompetitionName
     *
     * @return CumulativeCompetition|null
     */
    public function findOne(string $cumulativeGroupName, string $cumulativeCompetitionName): ?CumulativeCompetition
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeCompetition');
        return $queryBuilder
            ->join('cumulativeCompetition.partOf', 'cumulativeGroup')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('cumulativeGroup.name', ':cumulativeGroupName'),
                $queryBuilder->expr()->eq('cumulativeCompetition.name', ':cumulativeCompetitionName')
                )
            )
            ->setParameter('cumulativeGroupName', $cumulativeGroupName)
            ->setParameter('cumulativeCompetitionName', $cumulativeCompetitionName)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('cumulativeCompetition')->getQuery();
	}

    /**
     *
     * @param string $name
     *
     * @return QueryBuilder
     */
    private function partOfGroupBuilder($name): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeCompetition');
        $queryBuilder->join('cumulativeCompetition.partOf', 'cumulativeGroup');
        $queryBuilder->where($queryBuilder->expr()->eq('cumulativeGroup.name', ':name'));
        $queryBuilder->orderBy('cumulativeCompetition.name', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     * @param string $name
     */
    private function partOfGroupParameters(Query $query, $name) : void
    {
        $query->setParameter('name', $name);
    }
    
    /**
     *
     * @param string $name
     *
     * @return Query
     */
    public function findPartOfGroupQuery($name): Query
    {
        $query = $this->partOfGroupBuilder($name)->getQuery();
        $this->partOfGroupParameters($query, $name);
    
        return $query;
    }
    
    /**
     *
     * @param string $name
     *
     * @return array<int, CumulativeCompetition>
     */
    public function findPartOfGroup($name): array
    {
        return $this->findPartOfGroupQuery($name)->getResult();
    }
    
    /**
     *
     * @param string $name
     * @param string|null $competition
     *
     * @return Query
     */
    public function findWithNameQuery($name, ?string $competition): Query
    {
        $queryBuilder = $this->partOfGroupBuilder($name);
        $queryBuilder->andWhere($queryBuilder->expr()->like('cumulativeCompetition.name', ':competition'));
        $query = $queryBuilder->getQuery();
        $this->partOfGroupParameters($query, $name);
        if (is_null($competition) || strcmp(trim($competition), '') === 0) {
            $query->setParameter('competition', '%');
        } else {
            $query->setParameter('competition', strpos($competition, '%') ? $competition : '%' . $competition . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string $name
     * @param string|null $competition
     *
     * @return array<int, CumulativeCompetition>
     */
    public function findWithName($name, ?string $competition): array
    {
        return $this->findWithNameQuery($name, $competition)->getResult();
    }

    /**
     *
     * @param  $name
     *
     * @return Query
     */
    public function findIgnoringGroupQuery($name): Query
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeCompetition');
        $queryBuilder->where($queryBuilder->expr()->eq('cumulativeCompetition.name', ':name'));
        $query = $queryBuilder->getQuery();
        $query->setParameter('name', $name);
    
        return $query;
    }
    
    /**
     *
     * @param  $name
     *
     * @return CumulativeCompetition|null
     */
    public function findIgnoringGroup($name): ?CumulativeCompetition
    {
        return $this->findIgnoringGroupQuery($name)->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function publicBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeCompetition');
        $queryBuilder->where($queryBuilder->expr()->eq('cumulativeCompetition.private', 0));
        $queryBuilder->orderBy('cumulativeCompetition.name', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findPublicQuery(): Query
    {
        return $this->publicBuilder()->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, CumulativeCompetition>
     */
    public function findPublic(): array
    {
        return $this->findPublicQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $competition
     *
     * @return Query
     */
    public function findPublicWithNameQuery(?string $competition): Query
    {
        $queryBuilder = $this->publicBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->like('cumulativeCompetition.name', ':competition'));
        $query = $queryBuilder->getQuery();
        if (is_null($competition) || strcmp(trim($competition), '') === 0) {
            $query->setParameter('competition', '%');
        } else {
            $query->setParameter('competition', strpos($competition, '%') ? $competition : '%' . $competition . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $competition
     *
     * @return array<int, CumulativeCompetition>
     */
    public function findPublicWithName(?string $competition): array
    {
        return $this->findPublicWithNameQuery($competition)->getResult();
    }

    /**
     *
     * @param CumulativeCompetition $entity
     * @param bool $flush
     */
    public function remove(CumulativeCompetition $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param CumulativeCompetition $entity
     * @param bool $flush
     */
    public function save(CumulativeCompetition $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
