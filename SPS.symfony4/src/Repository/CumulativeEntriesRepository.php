<?php
namespace App\Repository;

use App\Entity\CumulativeEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<CumulativeEntry>
 */
class CumulativeEntriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CumulativeEntry::class);
    }

    /**
     *
     * @param int|null $score
     * @param bool $flush
     *
     * @return CumulativeEntry
     */
    public function create(int $score = null, $flush = false): CumulativeEntry
    {
        $new = new CumulativeEntry();
        $new->setScore($score);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $cumulativeGroupName
     * @param string $cumulativeCompetitionName
     * @param int $score
     *
     * @return CumulativeEntry|null
     */
    public function findOne(string $cumulativeGroupName, string $cumulativeCompetitionName, int $score): ?CumulativeEntry
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeEntry');
        return $queryBuilder
            ->join('cumulativeEntry.partOf', 'cumulativeCompetition')
            ->join('cumulativeCompetition.partOf', 'cumulativeGroup')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('cumulativeGroup.name', ':cumulativeGroupName'),
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('cumulativeCompetition.name', ':cumulativeCompetitionName'),
                $queryBuilder->expr()->eq('cumulativeEntry.score', ':score')
                ))
            )
            ->setParameter('cumulativeGroupName', $cumulativeGroupName)
            ->setParameter('cumulativeCompetitionName', $cumulativeCompetitionName)
            ->setParameter('score', $score)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('cumulativeEntry')->getQuery();
	}

    /**
     *
     * @param string $name
     *
     * @return Query
     */
    public function findForCompetitionQuery(string $name): Query
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeEntry');
        $queryBuilder->join('cumulativeEntry.partOf', 'cumulativeCompetition');
        $queryBuilder->join('cumulativeEntry.author', 'person');
        $queryBuilder->where($queryBuilder->expr()->eq('cumulativeCompetition.name', ':name'));
        $queryBuilder->orderBy('cumulativeEntry.score', 'DESC');
        $queryBuilder->addOrderBy('person.familyName', 'ASC');
        $queryBuilder->addOrderBy('person.forenames', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('name', $name);
    
        return $query;
    }
    
    /**
     *
     * @param string $name
     *
     * @return array<int, CumulativeEntry>
     */
    public function findForCompetition(string $name): array
    {
        return $this->findForCompetitionQuery($name)->getResult();
    }

    /**
     *
     * @param CumulativeEntry $entity
     * @param bool $flush
     */
    public function remove(CumulativeEntry $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param CumulativeEntry $entity
     * @param bool $flush
     */
    public function save(CumulativeEntry $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
