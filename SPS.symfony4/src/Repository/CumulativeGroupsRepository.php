<?php
namespace App\Repository;

use App\Entity\CumulativeGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<CumulativeGroup>
 */
class CumulativeGroupsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, CumulativeGroup::class);
    }

    /**
     *
     * @param string $name
     * @param bool $flush
     *
     * @return CumulativeGroup
     */
    public function create(string $name, $flush = false): CumulativeGroup
    {
        $new = new CumulativeGroup();
        $new->setName($name);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $name
     *
     * @return CumulativeGroup|null
     */
    public function findOne(string $name): ?CumulativeGroup
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeGroup');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('cumulativeGroup.name', ':name'))
            ->setParameter('name', $name)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('cumulativeGroup');
        $queryBuilder->orderBy('cumulativeGroup.name', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, CumulativeGroup>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return Query
     */
    public function findTitleLikeQuery(?string $title): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('cumulativeGroup.name', ':title'));
        $query = $queryBuilder->getQuery();
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return array<int, CumulativeGroup>
     */
    public function findTitleLike(?string $title): array
    {
        return $this->findTitleLikeQuery($title)->getResult();
    }

    /**
     *
     * @param CumulativeGroup $entity
     * @param bool $flush
     */
    public function remove(CumulativeGroup $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param CumulativeGroup $entity
     * @param bool $flush
     */
    public function save(CumulativeGroup $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
