<?php
namespace App\Repository;

use App\Entity\Distinction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<Distinction>
 */
class DistinctionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Distinction::class);
    }

    /**
     *
     * @param string $title
     * @param int|null $rank
     * @param bool $flush
     *
     * @return Distinction
     */
    public function create(string $title, int $rank = null, $flush = false): Distinction
    {
        $new = new Distinction();
        $new->setTitle($title);
        $new->setRank($rank);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $title
     *
     * @return Distinction|null
     */
    public function findOne(string $title): ?Distinction
    {
        $queryBuilder = $this->createQueryBuilder('distinction');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('distinction.title', ':title'))
            ->setParameter('title', $title)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('distinction');
        $queryBuilder->orderBy('distinction.rank', 'ASC');
        $queryBuilder->addOrderBy('distinction.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Distinction>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $name
     *
     * @return Query
     */
    public function findWithNameQuery(?string $name): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('distinction.title', ':name'));
        $query = $queryBuilder->getQuery();
        if (is_null($name) || strcmp(trim($name), '') === 0) {
            $query->setParameter('name', '%');
        } else {
            $query->setParameter('name', strpos($name, '%') ? $name : '%' . $name . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $name
     *
     * @return array<int, Distinction>
     */
    public function findWithName(?string $name): array
    {
        return $this->findWithNameQuery($name)->getResult();
    }

    /**
     *
     * @param Distinction $entity
     * @param bool $flush
     */
    public function remove(Distinction $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Distinction $entity
     * @param bool $flush
     */
    public function save(Distinction $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
