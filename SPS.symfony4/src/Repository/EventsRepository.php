<?php
namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<Event>
 */
class EventsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Event::class);
    }

    /**
     *
     * @param string $title
     * @param int $category
     * @param bool|null $draft
     * @param bool|null $cancelled
     * @param bool|null $postponed
     * @param bool|null $publicise
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $duration
     * @param bool $flush
     *
     * @return Event
     */
    public function create(string $title, int $category, bool $draft = null, bool $cancelled = null, bool $postponed = null, bool $publicise = null, \DateTimeInterface $start, \DateTimeInterface $duration, $flush = false): Event
    {
        $new = new Event();
        $new->setTitle($title);
        $new->setCategory($category);
        $new->setDraft($draft);
        $new->setCancelled($cancelled);
        $new->setPostponed($postponed);
        $new->setPublicise($publicise);
        $new->setStart($start);
        $new->setDuration($duration);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $title
     *
     * @return Event|null
     */
    public function findOne(string $title): ?Event
    {
        $queryBuilder = $this->createQueryBuilder('event');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('event.title', ':title'))
            ->setParameter('title', $title)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('event');
        $queryBuilder->orderBy('event.start', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Event>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $eventTitle
     *
     * @return Query
     */
    public function findTitleLikeQuery(?string $eventTitle): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('event.title', ':eventTitle'));
        $query = $queryBuilder->getQuery();
        if (is_null($eventTitle) || strcmp(trim($eventTitle), '') === 0) {
            $query->setParameter('eventTitle', '%');
        } else {
            $query->setParameter('eventTitle', strpos($eventTitle, '%') ? $eventTitle : '%' . $eventTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $eventTitle
     *
     * @return array<int, Event>
     */
    public function findTitleLike(?string $eventTitle): array
    {
        return $this->findTitleLikeQuery($eventTitle)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findFutureQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('event');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->gt('event.start', ':now'),
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('event.publicise', 1),
                $queryBuilder->expr()->neq('event.draft', 1)
            )
        ));
        $queryBuilder->orderBy('event.start', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('now', new \DateTime('now'));
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Event>
     */
    public function findFuture(): array
    {
        return $this->findFutureQuery()->getResult();
    }

    /**
     *
     * @param Event $entity
     * @param bool $flush
     */
    public function remove(Event $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Event $entity
     * @param bool $flush
     */
    public function save(Event $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
