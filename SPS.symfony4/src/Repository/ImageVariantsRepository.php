<?php
namespace App\Repository;

use App\Entity\ImageVariant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<ImageVariant>
 */
class ImageVariantsRepository extends ServiceEntityRepository
{
    /**
     * @var PeopleRepository $peopleRepository
     */
    private PeopleRepository $peopleRepository;

    public function __construct(ManagerRegistry $managerRegistry, PeopleRepository $peopleRepository)
    {
        parent::__construct($managerRegistry, ImageVariant::class);
        $this->peopleRepository = $peopleRepository;
    }

    /**
     *
     * @param string $description
     * @param string|null $filePath
     * @param bool|null $clubShowcase
     * @param bool|null $personalShowcase
     * @param bool|null $photo
     * @param int|null $width
     * @param int|null $height
     * @param bool $flush
     *
     * @return ImageVariant
     */
    public function create(string $description, string $filePath = null, bool $clubShowcase = null, bool $personalShowcase = null, bool $photo = null, int $width = null, int $height = null, $flush = false): ImageVariant
    {
        $new = new ImageVariant();
        $new->setDescription($description);
        $new->setFilePath($filePath);
        $new->setClubShowcase($clubShowcase);
        $new->setPersonalShowcase($personalShowcase);
        $new->setPhoto($photo);
        $new->setWidth($width);
        $new->setHeight($height);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $username
     * @param string $title
     * @param string $description
     *
     * @return ImageVariant|null
     */
    public function findOne(string $username, string $title, string $description): ?ImageVariant
    {
        $queryBuilder = $this->createQueryBuilder('imageVariant');
        return $queryBuilder
            ->join('imageVariant.image', 'image')
            ->join('image.author', 'person')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('person.username', ':username'),
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('image.title', ':title'),
                $queryBuilder->expr()->eq('imageVariant.description', ':description')
                ))
            )
            ->setParameter('username', $username)
            ->setParameter('title', $title)
            ->setParameter('description', $description)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->orderBy('image.title', 'ASC');
        $queryBuilder->addOrderBy('imageVariant.description', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, ImageVariant>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return Query
     */
    public function findImageTitleLikeQuery(?string $title): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('image.title', ':title'));
        $query = $queryBuilder->getQuery();
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return array<int, ImageVariant>
     */
    public function findImageTitleLike(?string $title): array
    {
        return $this->findImageTitleLikeQuery($title)->getResult();
    }

    /**
     *
     * @param string $username
     * @param string $title
     *
     * @return Query
     */
    public function findVariantsOfImageQuery($username, $title): Query
    {
        $queryBuilder = $this->createQueryBuilder('imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('person.username', ':username'),
            $queryBuilder->expr()->eq('image.title', ':title')
        ));
        $queryBuilder->orderBy('imageVariant.description', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('username', $username);
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $username
     * @param string $title
     *
     * @return array<int, ImageVariant>
     */
    public function findVariantsOfImage($username, $title): array
    {
        return $this->findVariantsOfImageQuery($username, $title)->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findMyVariantsQuery(string $title): Query
    {
        $queryBuilder = $this->createQueryBuilder('imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->eq('image.title', ':title')
        ));
        $query = $queryBuilder->getQuery();
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return array<int, ImageVariant>
     */
    public function findMyVariants(string $title): array
    {
        return $this->findMyVariantsQuery($title)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findClubShowcaseQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('imageVariant');
        $queryBuilder->where($queryBuilder->expr()->eq('imageVariant.clubShowcase', 1));
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, ImageVariant>
     */
    public function findClubShowcase(): array
    {
        return $this->findClubShowcaseQuery()->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findWithoutSizeQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('imageVariant');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->neq('imageVariant.filePath', $queryBuilder->expr()->literal('')),
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('imageVariant.width', 0),
                $queryBuilder->expr()->eq('imageVariant.height', 0)
            )
        ));
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, ImageVariant>
     */
    public function findWithoutSize(): array
    {
        return $this->findWithoutSizeQuery()->getResult();
    }

    /**
     *
     * @param ImageVariant $entity
     * @param bool $flush
     */
    public function remove(ImageVariant $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param ImageVariant $entity
     * @param bool $flush
     */
    public function save(ImageVariant $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
