<?php
namespace App\Repository;

use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<Image>
 */
class ImagesRepository extends ServiceEntityRepository
{
    /**
     * @var PeopleRepository $peopleRepository
     */
    private PeopleRepository $peopleRepository;

    public function __construct(ManagerRegistry $managerRegistry, PeopleRepository $peopleRepository)
    {
        parent::__construct($managerRegistry, Image::class);
        $this->peopleRepository = $peopleRepository;
    }

    /**
     *
     * @param string $title
     * @param bool $flush
     *
     * @return Image
     */
    public function create(string $title, $flush = false): Image
    {
        $new = new Image();
        $new->setTitle($title);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $username
     * @param string $title
     *
     * @return Image|null
     */
    public function findOne(string $username, string $title): ?Image
    {
        $queryBuilder = $this->createQueryBuilder('image');
        return $queryBuilder
            ->join('image.author', 'person')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('person.username', ':username'),
                $queryBuilder->expr()->eq('image.title', ':title')
                )
            )
            ->setParameter('username', $username)
            ->setParameter('title', $title)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('image');
        $queryBuilder->join('image.variants', 'imageVariant');
        $queryBuilder->join('imageVariant.entries', 'sectionEntry');
        $queryBuilder->orderBy('image.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Image>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return Query
     */
    public function findWithTitleQuery(?string $title): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->orX(
            $queryBuilder->expr()->like('image.title', ':title'),
            $queryBuilder->expr()->like('sectionEntry.title', ':title')
        ));
        $query = $queryBuilder->getQuery();
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return array<int, Image>
     */
    public function findWithTitle(?string $title): array
    {
        return $this->findWithTitleQuery($title)->getResult();
    }

    /**
     *
     * @param string $username
     *
     * @return QueryBuilder
     */
    private function byPersonBuilder($username): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('image');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->where($queryBuilder->expr()->eq('person.username', ':username'));
        $queryBuilder->orderBy('image.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     * @param string $username
     */
    private function byPersonParameters(Query $query, $username) : void
    {
        $query->setParameter('username', $username);
    }
    
    /**
     *
     * @param string $username
     *
     * @return Query
     */
    public function findByPersonQuery($username): Query
    {
        $query = $this->byPersonBuilder($username)->getQuery();
        $this->byPersonParameters($query, $username);
    
        return $query;
    }
    
    /**
     *
     * @param string $username
     *
     * @return array<int, Image>
     */
    public function findByPerson($username): array
    {
        return $this->findByPersonQuery($username)->getResult();
    }
    
    /**
     *
     * @param string $username
     * @param string|null $title
     *
     * @return Query
     */
    public function findByPersonWithTitleQuery($username, ?string $title): Query
    {
        $queryBuilder = $this->byPersonBuilder($username);
        $queryBuilder->andWhere($queryBuilder->expr()->like('image.title', ':title'));
        $query = $queryBuilder->getQuery();
        $this->byPersonParameters($query, $username);
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string $username
     * @param string|null $title
     *
     * @return array<int, Image>
     */
    public function findByPersonWithTitle($username, ?string $title): array
    {
        return $this->findByPersonWithTitleQuery($username, $title)->getResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function myImagesBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('image');
        $queryBuilder->where($queryBuilder->expr()->eq('image.author', ':user'));
        $queryBuilder->orderBy('image.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     */
    private function myImagesParameters(Query $query) : void
    {
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findMyImagesQuery(): Query
    {
        $query = $this->myImagesBuilder()->getQuery();
        $this->myImagesParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Image>
     */
    public function findMyImages(): array
    {
        return $this->findMyImagesQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return Query
     */
    public function findMineWithTitleQuery(?string $title): Query
    {
        $queryBuilder = $this->myImagesBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->like('image.title', ':title'));
        $query = $queryBuilder->getQuery();
        $this->myImagesParameters($query);
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return array<int, Image>
     */
    public function findMineWithTitle(?string $title): array
    {
        return $this->findMineWithTitleQuery($title)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findMyImagesWithoutPicturesQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('image');
        $queryBuilder->join('image.variants', 'imageVariant');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->isNull('imageVariant.filePath'),
                $queryBuilder->expr()->eq('imageVariant.photo', 1)
            )
        ));
        $queryBuilder->orderBy('image.title', 'DESC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, Image>
     */
    public function findMyImagesWithoutPictures(): array
    {
        return $this->findMyImagesWithoutPicturesQuery()->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findMyImageQuery(string $title): Query
    {
        $queryBuilder = $this->createQueryBuilder('image');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->eq('image.title', ':title')
        ));
        $query = $queryBuilder->getQuery();
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return Image|null
     */
    public function findMyImage(string $title): ?Image
    {
        return $this->findMyImageQuery($title)->getOneOrNullResult();
    }

    /**
     *
     * @param string $username
     *
     * @return Query
     */
    public function findPersonalShowcaseQuery($username): Query
    {
        $queryBuilder = $this->createQueryBuilder('image');
        $queryBuilder->select('image.title');
        $queryBuilder->addSelect('imageVariant.filePath');
        $queryBuilder->join('image.variants', 'imageVariant');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->where($queryBuilder->expr()->eq('person.username', ':username'));
        $queryBuilder->andWhere($queryBuilder->expr()->eq('imageVariant.personalShowcase', 1));
        $queryBuilder->orderBy('image.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('username', $username);
    
        return $query;
    }
    
    /**
     *
     * @param string $username
     *
     * @return array<int, Image>
     */
    public function findPersonalShowcase($username): array
    {
        return $this->findPersonalShowcaseQuery($username)->getResult();
    }

    /**
     *
     * @param Image $entity
     * @param bool $flush
     */
    public function remove(Image $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Image $entity
     * @param bool $flush
     */
    public function save(Image $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
