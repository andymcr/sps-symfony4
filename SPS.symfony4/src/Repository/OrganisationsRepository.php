<?php
namespace App\Repository;

use App\Entity\Organisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<Organisation>
 */
class OrganisationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Organisation::class);
    }

    /**
     *
     * @param string $title
     * @param string|null $shortTitle
     * @param string|null $website
     * @param bool $flush
     *
     * @return Organisation
     */
    public function create(string $title, string $shortTitle = null, string $website = null, $flush = false): Organisation
    {
        $new = new Organisation();
        $new->setTitle($title);
        $new->setShortTitle($shortTitle);
        $new->setWebsite($website);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $title
     *
     * @return Organisation|null
     */
    public function findOne(string $title): ?Organisation
    {
        $queryBuilder = $this->createQueryBuilder('organisation');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('organisation.title', ':title'))
            ->setParameter('title', $title)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('organisation');
        $queryBuilder->orderBy('organisation.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Organisation>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $name
     *
     * @return Query
     */
    public function findWithNameQuery(?string $name): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('organisation.title', ':name'));
        $query = $queryBuilder->getQuery();
        if (is_null($name) || strcmp(trim($name), '') === 0) {
            $query->setParameter('name', '%');
        } else {
            $query->setParameter('name', strpos($name, '%') ? $name : '%' . $name . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $name
     *
     * @return array<int, Organisation>
     */
    public function findWithName(?string $name): array
    {
        return $this->findWithNameQuery($name)->getResult();
    }

    /**
     *
     * @param Organisation $entity
     * @param bool $flush
     */
    public function remove(Organisation $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Organisation $entity
     * @param bool $flush
     */
    public function save(Organisation $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
