<?php
namespace App\Repository;

use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;


/**
 * @extends ServiceEntityRepository<Person>
 */
class PeopleRepository extends ServiceEntityRepository
{
    /**
     * @var Security $security
     */
    private Security $security;

    public function __construct(ManagerRegistry $managerRegistry, Security $security)
    {
        parent::__construct($managerRegistry, Person::class);
        $this->security = $security;
    }

    /**
     * @return Person
     *
     */
    public function getCurrentUser(): ?Person
    {
        $user = $this->security->getUser();
        if (is_null($user)) {
            return null;
        }
    
        $queryBuilder = $this->createQueryBuilder('person');
        $peopleRepository = $queryBuilder
            ->where($queryBuilder->expr()->eq('person.username', ':username'))
            ->setParameter('username', $user->getUsername())
            ->getQuery()->getOneOrNullResult();
        if(is_null($peopleRepository)) {
            throw new \Exception();
        }
        return $peopleRepository;
    }

    /**
     *
     * @param string $username
     * @param string $familyName
     * @param string $forenames
     * @param string|null $salutation
     * @param string|null $website
     * @param bool $flush
     *
     * @return Person
     */
    public function create(string $username, string $familyName, string $forenames, string $salutation = null, string $website = null, $flush = false): Person
    {
        $new = new Person();
        $new->setUsername($username);
        $new->setFamilyName($familyName);
        $new->setForenames($forenames);
        $new->setSalutation($salutation);
        $new->setWebsite($website);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $username
     *
     * @return Person|null
     */
    public function findOne(string $username): ?Person
    {
        $queryBuilder = $this->createQueryBuilder('person');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('person.username', ':username'))
            ->setParameter('username', $username)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('person');
        $queryBuilder->orderBy('person.familyName', 'ASC');
        $queryBuilder->addOrderBy('person.forenames', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Person>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $forenames
     * @param string|null $familyName
     *
     * @return Query
     */
    public function findWithNameQuery(?string $forenames = '%', ?string $familyName = '%'): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->like('person.forenames', ':forenames'),
            $queryBuilder->expr()->like('person.familyName', ':familyName')
        ));
        $query = $queryBuilder->getQuery();
        if (is_null($forenames) || strcmp(trim($forenames), '') === 0) {
            $query->setParameter('forenames', '%');
        } else {
            $query->setParameter('forenames', strpos($forenames, '%') ? $forenames : '%' . $forenames . '%');
        }
        if (is_null($familyName) || strcmp(trim($familyName), '') === 0) {
            $query->setParameter('familyName', '%');
        } else {
            $query->setParameter('familyName', strpos($familyName, '%') ? $familyName : '%' . $familyName . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $forenames
     * @param string|null $familyName
     *
     * @return array<int, Person>
     */
    public function findWithName(?string $forenames = '%', ?string $familyName = '%'): array
    {
        return $this->findWithNameQuery($forenames, $familyName)->getResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function judgesBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('person');
        $queryBuilder->join('person.hasJudged', 'competition');
        $queryBuilder->join('competition.category', 'competitionCategory');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->join('event.organisedBy', 'organisation');
        $queryBuilder->where($queryBuilder->expr()->eq('organisation.title', $queryBuilder->expr()->literal('Sale Photographic Society')));
        $queryBuilder->orderBy('person.familyName', 'ASC');
        $queryBuilder->addOrderBy('person.forenames', 'ASC');
        $queryBuilder->distinct();
        return $queryBuilder;
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findJudgesQuery(): Query
    {
        return $this->judgesBuilder()->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, Person>
     */
    public function findJudges(): array
    {
        return $this->findJudgesQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $forenames
     * @param string|null $familyName
     *
     * @return Query
     */
    public function findJudgeWithNameQuery(?string $forenames = '%', ?string $familyName = '%'): Query
    {
        $queryBuilder = $this->judgesBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->andX(
            $queryBuilder->expr()->like('person.forenames', ':forenames'),
            $queryBuilder->expr()->like('person.familyName', ':familyName')
        ));
        $query = $queryBuilder->getQuery();
        if (is_null($forenames) || strcmp(trim($forenames), '') === 0) {
            $query->setParameter('forenames', '%');
        } else {
            $query->setParameter('forenames', strpos($forenames, '%') ? $forenames : '%' . $forenames . '%');
        }
        if (is_null($familyName) || strcmp(trim($familyName), '') === 0) {
            $query->setParameter('familyName', '%');
        } else {
            $query->setParameter('familyName', strpos($familyName, '%') ? $familyName : '%' . $familyName . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $forenames
     * @param string|null $familyName
     *
     * @return array<int, Person>
     */
    public function findJudgeWithName(?string $forenames = '%', ?string $familyName = '%'): array
    {
        return $this->findJudgeWithNameQuery($forenames, $familyName)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findWithGalleryQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('person');
        $queryBuilder->where($queryBuilder->expr()->isNotNull('person.showcaseImage'));
        $queryBuilder->orderBy('person.familyName', 'ASC');
        $queryBuilder->addOrderBy('person.forenames', 'ASC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, Person>
     */
    public function findWithGallery(): array
    {
        return $this->findWithGalleryQuery()->getResult();
    }

    /**
     *
     * @param Person $entity
     * @param bool $flush
     */
    public function remove(Person $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Person $entity
     * @param bool $flush
     */
    public function save(Person $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
