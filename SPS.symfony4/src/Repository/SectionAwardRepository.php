<?php
namespace App\Repository;

use App\Entity\SectionAward;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<SectionAward>
 */
class SectionAwardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, SectionAward::class);
    }

    /**
     *
     * @param string|null $title
     * @param int|null $rank
     * @param bool $flush
     *
     * @return SectionAward
     */
    public function create(string $title = null, int $rank = null, $flush = false): SectionAward
    {
        $new = new SectionAward();
        $new->setTitle($title);
        $new->setRank($rank);
        $this->save($new, $flush);
        return $new;
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('sectionAward')->getQuery();
	}

    /**
     *
     * @param SectionAward $entity
     * @param bool $flush
     */
    public function remove(SectionAward $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param SectionAward $entity
     * @param bool $flush
     */
    public function save(SectionAward $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
