<?php
namespace App\Repository;

use App\Entity\SectionEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<SectionEntry>
 */
class SectionEntriesRepository extends ServiceEntityRepository
{
    /**
     * @var PeopleRepository $peopleRepository
     */
    private PeopleRepository $peopleRepository;

    public function __construct(ManagerRegistry $managerRegistry, PeopleRepository $peopleRepository)
    {
        parent::__construct($managerRegistry, SectionEntry::class);
        $this->peopleRepository = $peopleRepository;
    }

    /**
     *
     * @param string|null $title
     * @param int|null $entryNumber
     * @param int|null $removalOrder
     * @param float|null $score
     * @param int|null $points
     * @param bool|null $removed
     * @param bool|null $disqualified
     * @param bool|null $tagged
     * @param bool $flush
     *
     * @return SectionEntry
     */
    public function create(string $title = null, int $entryNumber = null, int $removalOrder = null, float $score = null, int $points = null, bool $removed = null, bool $disqualified = null, bool $tagged = null, $flush = false): SectionEntry
    {
        $new = new SectionEntry();
        $new->setTitle($title);
        $new->setEntryNumber($entryNumber);
        $new->setRemovalOrder($removalOrder);
        $new->setScore($score);
        $new->setPoints($points);
        $new->setRemoved($removed);
        $new->setDisqualified($disqualified);
        $new->setTagged($tagged);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $eventTitle
     * @param string $competitionSectionTitle
     * @param string $sectionEntryTitle
     *
     * @return SectionEntry|null
     */
    public function findOne(string $eventTitle, string $competitionSectionTitle, string $sectionEntryTitle): ?SectionEntry
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        return $queryBuilder
            ->join('sectionEntry.enteredIn', 'competitionSection')
            ->join('competitionSection.partOf', 'competition')
            ->join('competition.event', 'event')
            ->where(
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('event.title', ':eventTitle'),
                $queryBuilder->expr()->andX($queryBuilder->expr()->eq('competitionSection.title', ':competitionSectionTitle'),
                $queryBuilder->expr()->eq('sectionEntry.title', ':sectionEntryTitle')
                ))
            )
            ->setParameter('eventTitle', $eventTitle)
            ->setParameter('competitionSectionTitle', $competitionSectionTitle)
            ->setParameter('sectionEntryTitle', $sectionEntryTitle)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->orderBy('sectionEntry.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, SectionEntry>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return Query
     */
    public function findTitleLikeQuery(?string $title): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('sectionEntry.title', ':title'));
        $query = $queryBuilder->getQuery();
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return array<int, SectionEntry>
     */
    public function findTitleLike(?string $title): array
    {
        return $this->findTitleLikeQuery($title)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findAwardsDecendingQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.award', 'sectionAward');
        $queryBuilder->where($queryBuilder->expr()->gt('sectionAward.rank', 0));
        $queryBuilder->orderBy('sectionAward.rank', 'DESC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, SectionEntry>
     */
    public function findAwardsDecending(): array
    {
        return $this->findAwardsDecendingQuery()->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findScorersDecendingQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->where($queryBuilder->expr()->gt('sectionEntry.points', 0));
        $queryBuilder->orderBy('sectionEntry.points', 'DESC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, SectionEntry>
     */
    public function findScorersDecending(): array
    {
        return $this->findScorersDecendingQuery()->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findOfCompetitionQuery($title): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->eq('event.title', ':title'));
        $queryBuilder->andWhere($queryBuilder->expr()->andX(
            $queryBuilder->expr()->neq('sectionEntry.disqualified', 1),
            $queryBuilder->expr()->neq('sectionEntry.removed', 1)
        ));
        $queryBuilder->orderBy('competitionSection.order', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.entryNumber', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return array<int, SectionEntry>
     */
    public function findOfCompetition($title): array
    {
        return $this->findOfCompetitionQuery($title)->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findAuthorsOfCompetitionQuery($title): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->eq('event.title', ':title'));
        $queryBuilder->andWhere($queryBuilder->expr()->andX(
            $queryBuilder->expr()->neq('sectionEntry.disqualified', 1),
            $queryBuilder->expr()->neq('sectionEntry.removed', 1)
        ));
        $queryBuilder->orderBy('person.familyName', 'ASC');
        $queryBuilder->addOrderBy('person.forenames', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return array<int, SectionEntry>
     */
    public function findAuthorsOfCompetition($title): array
    {
        return $this->findAuthorsOfCompetitionQuery($title)->getResult();
    }

    /**
     *
     * @param string $eventTitle
     * @param string $competitionSectionTitle
     *
     * @return Query
     */
    public function findOfSectionQuery($eventTitle, $competitionSectionTitle): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('event.title', ':eventTitle'),
            $queryBuilder->expr()->eq('competitionSection.title', ':competitionSectionTitle')
        ));
        $queryBuilder->orderBy('sectionEntry.entryNumber', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('eventTitle', $eventTitle);
        $query->setParameter('competitionSectionTitle', $competitionSectionTitle);
    
        return $query;
    }
    
    /**
     *
     * @param string $eventTitle
     * @param string $competitionSectionTitle
     *
     * @return array<int, SectionEntry>
     */
    public function findOfSection($eventTitle, $competitionSectionTitle): array
    {
        return $this->findOfSectionQuery($eventTitle, $competitionSectionTitle)->getResult();
    }

    /**
     *
     * @param string $username
     *
     * @return Query
     */
    public function findOfPersonQuery($username): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->where($queryBuilder->expr()->eq('person.username', ':username'));
        $queryBuilder->andWhere($queryBuilder->expr()->andX(
            $queryBuilder->expr()->neq('sectionEntry.disqualified', 1),
            $queryBuilder->expr()->neq('sectionEntry.removed', 1)
        ));
        $queryBuilder->orderBy('sectionEntry.score', 'DESC');
        $queryBuilder->addOrderBy('event.start', 'DESC');
        $queryBuilder->addOrderBy('image.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('username', $username);
    
        return $query;
    }
    
    /**
     *
     * @param string $username
     *
     * @return array<int, SectionEntry>
     */
    public function findOfPerson($username): array
    {
        return $this->findOfPersonQuery($username)->getResult();
    }

    /**
     *
     * @param string $username
     * @param string $title
     *
     * @return Query
     */
    public function findOfImageQuery($username, $title): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('person.username', ':username'),
            $queryBuilder->expr()->eq('image.title', ':title')
        ));
        $queryBuilder->orderBy('competition.submissionDeadline', 'DESC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('username', $username);
        $query->setParameter('title', $title);
    
        return $query;
    }
    
    /**
     *
     * @param string $username
     * @param string $title
     *
     * @return array<int, SectionEntry>
     */
    public function findOfImage($username, $title): array
    {
        return $this->findOfImageQuery($username, $title)->getResult();
    }

    /**
     *
     * @param string $title
     *
     * @return Query
     */
    public function findMyImageEntriesQuery($title): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->join('image.author', 'person');
        $queryBuilder->where($queryBuilder->expr()->eq('image.title', ':title'));
        $queryBuilder->andWhere($queryBuilder->expr()->eq('image.author', ':user'));
        $query = $queryBuilder->getQuery();
        $query->setParameter('title', $title);
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    
        return $query;
    }
    
    /**
     *
     * @param string $title
     *
     * @return array<int, SectionEntry>
     */
    public function findMyImageEntries($title): array
    {
        return $this->findMyImageEntriesQuery($title)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findMyBpeEntriesQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.category', 'competitionCategory');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->eq('competitionCategory.shortName', $queryBuilder->expr()->literal('BPE'))
        ));
        $queryBuilder->orderBy('organisation.title', 'ASC');
        $queryBuilder->addOrderBy('event.start', 'ASC');
        $queryBuilder->addOrderBy('competitionSection.title', 'ASC');
        $queryBuilder->addOrderBy('image.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, SectionEntry>
     */
    public function findMyBpeEntries(): array
    {
        return $this->findMyBpeEntriesQuery()->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findMyBpeEntriesByExhibitionQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.category', 'competitionCategory');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->join('event.organisedBy', 'organisation');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->eq('competitionCategory.shortName', $queryBuilder->expr()->literal('BPE'))
        ));
        $queryBuilder->orderBy('organisation.title', 'ASC');
        $queryBuilder->addOrderBy('event.start', 'ASC');
        $queryBuilder->addOrderBy('competitionSection.title', 'ASC');
        $queryBuilder->addOrderBy('image.title', 'ASC');
        $query = $queryBuilder->getQuery();
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, SectionEntry>
     */
    public function findMyBpeEntriesByExhibition(): array
    {
        return $this->findMyBpeEntriesByExhibitionQuery()->getResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function mySpsEntriesBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->join('event.organisedBy', 'organisation');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->eq('organisation.shortTitle', $queryBuilder->expr()->literal('SPS'))
        ));
        $queryBuilder->orderBy('event.start', 'DESC');
        $queryBuilder->addOrderBy('competitionSection.order', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.entryNumber', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     */
    private function mySpsEntriesParameters(Query $query) : void
    {
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findMySpsEntriesQuery(): Query
    {
        $query = $this->mySpsEntriesBuilder()->getQuery();
        $this->mySpsEntriesParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, SectionEntry>
     */
    public function findMySpsEntries(): array
    {
        return $this->findMySpsEntriesQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $entryTitle
     *
     * @return Query
     */
    public function findEntryTitleLikeSPSQuery(?string $entryTitle): Query
    {
        $queryBuilder = $this->mySpsEntriesBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->like('sectionEntry.title', ':entryTitle'));
        $query = $queryBuilder->getQuery();
        $this->mySpsEntriesParameters($query);
        if (is_null($entryTitle) || strcmp(trim($entryTitle), '') === 0) {
            $query->setParameter('entryTitle', '%');
        } else {
            $query->setParameter('entryTitle', strpos($entryTitle, '%') ? $entryTitle : '%' . $entryTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $entryTitle
     *
     * @return array<int, SectionEntry>
     */
    public function findEntryTitleLikeSPS(?string $entryTitle): array
    {
        return $this->findEntryTitleLikeSPSQuery($entryTitle)->getResult();
    }
    
    /**
     *
     * @param string|null $imageTitle
     *
     * @return Query
     */
    public function findImageTitleLikeSPSQuery(?string $imageTitle): Query
    {
        $queryBuilder = $this->mySpsEntriesBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->like('image.title', ':imageTitle'));
        $query = $queryBuilder->getQuery();
        $this->mySpsEntriesParameters($query);
        if (is_null($imageTitle) || strcmp(trim($imageTitle), '') === 0) {
            $query->setParameter('imageTitle', '%');
        } else {
            $query->setParameter('imageTitle', strpos($imageTitle, '%') ? $imageTitle : '%' . $imageTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $imageTitle
     *
     * @return array<int, SectionEntry>
     */
    public function findImageTitleLikeSPS(?string $imageTitle): array
    {
        return $this->findImageTitleLikeSPSQuery($imageTitle)->getResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function myLCPUEntriesBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('sectionEntry');
        $queryBuilder->join('sectionEntry.enteredIn', 'competitionSection');
        $queryBuilder->join('competitionSection.partOf', 'competition');
        $queryBuilder->join('competition.event', 'event');
        $queryBuilder->join('event.organisedBy', 'organisation');
        $queryBuilder->join('sectionEntry.imageVariant', 'imageVariant');
        $queryBuilder->join('imageVariant.image', 'image');
        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('image.author', ':user'),
            $queryBuilder->expr()->eq('organisation.shortTitle', $queryBuilder->expr()->literal('LCPU'))
        ));
        $queryBuilder->orderBy('event.start', 'DESC');
        $queryBuilder->addOrderBy('competitionSection.order', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.entryNumber', 'ASC');
        $queryBuilder->addOrderBy('sectionEntry.title', 'ASC');
        return $queryBuilder;
    }
    
    /**
     *
     * @param Query $query
     */
    private function myLCPUEntriesParameters(Query $query) : void
    {
        $query->setParameter('user', $this->peopleRepository->getCurrentUser());
    }
    
    /**
     *
     
     *
     * @return Query
     */
    public function findMyLCPUEntriesQuery(): Query
    {
        $query = $this->myLCPUEntriesBuilder()->getQuery();
        $this->myLCPUEntriesParameters($query);
    
        return $query;
    }
    
    /**
     *
     
     *
     * @return array<int, SectionEntry>
     */
    public function findMyLCPUEntries(): array
    {
        return $this->findMyLCPUEntriesQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $entryTitle
     *
     * @return Query
     */
    public function findEntryTitleLikeLCPUQuery(?string $entryTitle): Query
    {
        $queryBuilder = $this->myLCPUEntriesBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->like('sectionEntry.title', ':entryTitle'));
        $query = $queryBuilder->getQuery();
        $this->myLCPUEntriesParameters($query);
        if (is_null($entryTitle) || strcmp(trim($entryTitle), '') === 0) {
            $query->setParameter('entryTitle', '%');
        } else {
            $query->setParameter('entryTitle', strpos($entryTitle, '%') ? $entryTitle : '%' . $entryTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $entryTitle
     *
     * @return array<int, SectionEntry>
     */
    public function findEntryTitleLikeLCPU(?string $entryTitle): array
    {
        return $this->findEntryTitleLikeLCPUQuery($entryTitle)->getResult();
    }
    
    /**
     *
     * @param string|null $imageTitle
     *
     * @return Query
     */
    public function findImageTitleLikeLCPUQuery(?string $imageTitle): Query
    {
        $queryBuilder = $this->myLCPUEntriesBuilder();
        $queryBuilder->andWhere($queryBuilder->expr()->like('image.title', ':imageTitle'));
        $query = $queryBuilder->getQuery();
        $this->myLCPUEntriesParameters($query);
        if (is_null($imageTitle) || strcmp(trim($imageTitle), '') === 0) {
            $query->setParameter('imageTitle', '%');
        } else {
            $query->setParameter('imageTitle', strpos($imageTitle, '%') ? $imageTitle : '%' . $imageTitle . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $imageTitle
     *
     * @return array<int, SectionEntry>
     */
    public function findImageTitleLikeLCPU(?string $imageTitle): array
    {
        return $this->findImageTitleLikeLCPUQuery($imageTitle)->getResult();
    }

    /**
     *
     * @param SectionEntry $entity
     * @param bool $flush
     */
    public function remove(SectionEntry $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param SectionEntry $entity
     * @param bool $flush
     */
    public function save(SectionEntry $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
