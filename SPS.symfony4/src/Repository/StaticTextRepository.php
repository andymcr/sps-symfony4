<?php
namespace App\Repository;

use App\Entity\StaticText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<StaticText>
 */
class StaticTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, StaticText::class);
    }

    /**
     *
     * @param string $name
     * @param string $content
     * @param bool $flush
     *
     * @return StaticText
     */
    public function create(string $name, string $content, $flush = false): StaticText
    {
        $new = new StaticText();
        $new->setName($name);
        $new->setContent($content);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $name
     *
     * @return StaticText|null
     */
    public function findOne(string $name): ?StaticText
    {
        $queryBuilder = $this->createQueryBuilder('staticText');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('staticText.name', ':name'))
            ->setParameter('name', $name)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('staticText')->getQuery();
	}

    /**
     *
     
     *
     * @return Query
     */
    public function findAboutUsTextQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('staticText');
        $queryBuilder->where($queryBuilder->expr()->eq('staticText.name', $queryBuilder->expr()->literal('aboutus')));
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return StaticText|null
     */
    public function findAboutUsText(): ?StaticText
    {
        return $this->findAboutUsTextQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findMentorsTextQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('staticText');
        $queryBuilder->where($queryBuilder->expr()->eq('staticText.name', $queryBuilder->expr()->literal('mentors')));
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return StaticText|null
     */
    public function findMentorsText(): ?StaticText
    {
        return $this->findMentorsTextQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findCommitteeTextQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('staticText');
        $queryBuilder->where($queryBuilder->expr()->eq('staticText.name', $queryBuilder->expr()->literal('committee')));
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return StaticText|null
     */
    public function findCommitteeText(): ?StaticText
    {
        return $this->findCommitteeTextQuery()->getOneOrNullResult();
    }

    /**
     *
     * @param StaticText $entity
     * @param bool $flush
     */
    public function remove(StaticText $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param StaticText $entity
     * @param bool $flush
     */
    public function save(StaticText $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
