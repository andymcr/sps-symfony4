<?php
namespace App\Repository;

use App\Entity\Talk;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;


/**
 * @extends ServiceEntityRepository<Talk>
 */
class TalksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Talk::class);
    }

    /**
     *
     * @param string|null $synopsis
     * @param bool $flush
     *
     * @return Talk
     */
    public function create(string $synopsis = null, $flush = false): Talk
    {
        $new = new Talk();
        $new->setSynopsis($synopsis);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $title
     *
     * @return Talk|null
     */
    public function findOne(string $title): ?Talk
    {
        $queryBuilder = $this->createQueryBuilder('talk');
        return $queryBuilder
            ->join('talk.event', 'event')
            ->where($queryBuilder->expr()->eq('event.title', ':title'))
            ->setParameter('title', $title)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     *
     
     *
     * @return QueryBuilder
     */
    private function allBuilder(): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('talk');
        $queryBuilder->join('talk.event', 'event');
        $queryBuilder->orderBy('event.start', 'DESC');
        return $queryBuilder;
    }
    
    /**
     *
     *
     * @return Query
     */
    public function findAllQuery(): Query
    {
        return $this->allBuilder()->getQuery();
    }
    
    /**
     *
     *
     * @return array<int, Talk>
     */
    public function findAll(): array
    {
        return $this->findAllQuery()->getResult();
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return Query
     */
    public function findWithTitleQuery(?string $title): Query
    {
        $queryBuilder = $this->allBuilder();
        $queryBuilder->where($queryBuilder->expr()->like('event.title', ':title'));
        $query = $queryBuilder->getQuery();
        if (is_null($title) || strcmp(trim($title), '') === 0) {
            $query->setParameter('title', '%');
        } else {
            $query->setParameter('title', strpos($title, '%') ? $title : '%' . $title . '%');
        }
    
        return $query;
    }
    
    /**
     *
     * @param string|null $title
     *
     * @return array<int, Talk>
     */
    public function findWithTitle(?string $title): array
    {
        return $this->findWithTitleQuery($title)->getResult();
    }

    /**
     *
     
     *
     * @return Query
     */
    public function findDateAscendingQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('talk');
        $queryBuilder->join('talk.event', 'event');
        $queryBuilder->orderBy('event.start', 'ASC');
    
        return $queryBuilder->getQuery();
    }
    
    /**
     *
     
     *
     * @return array<int, Talk>
     */
    public function findDateAscending(): array
    {
        return $this->findDateAscendingQuery()->getResult();
    }

    /**
     *
     * @param Talk $entity
     * @param bool $flush
     */
    public function remove(Talk $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Talk $entity
     * @param bool $flush
     */
    public function save(Talk $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
