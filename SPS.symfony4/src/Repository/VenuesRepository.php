<?php
namespace App\Repository;

use App\Entity\Venue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @extends ServiceEntityRepository<Venue>
 */
class VenuesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Venue::class);
    }

    /**
     *
     * @param string $name
     * @param bool $flush
     *
     * @return Venue
     */
    public function create(string $name, $flush = false): Venue
    {
        $new = new Venue();
        $new->setName($name);
        $this->save($new, $flush);
        return $new;
    }

    /**
     *
     * @param string $name
     *
     * @return Venue|null
     */
    public function findOne(string $name): ?Venue
    {
        $queryBuilder = $this->createQueryBuilder('venue');
        return $queryBuilder
            ->where($queryBuilder->expr()->eq('venue.name', ':name'))
            ->setParameter('name', $name)
            ->getQuery()->getOneOrNullResult();
    }

	/**
	 *
	 *
	 * @return Query
	 */
	public function findAllQuery(): Query
	{
	    return $this->createQueryBuilder('venue')->getQuery();
	}

    /**
     *
     * @param Venue $entity
     * @param bool $flush
     */
    public function remove(Venue $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     * @param Venue $entity
     * @param bool $flush
     */
    public function save(Venue $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush)
        {
            $this->flush();
        }
    }

    /**
     *
     */
    public function flush(): void
    {
        $this->_em->flush();
    }

}
