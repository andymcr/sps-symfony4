<?php
namespace App\Service;

use App\Entity\Competition;
// Start of user code use
use App\Entity\CompetitionAward;
use App\Entity\CompetitionSection;
use App\Entity\Person;
use App\Entity\SectionEntry;
use Doctrine\Common\Collections\Criteria;
use Uk\Me\AndyCarpenter\Writer\SpreadsheetWriter;
use Uk\Me\AndyCarpenter\Writer\WordWriter;
use Uk\Me\AndyCarpenter\Writer\Writer;
// End of user code

class CompetitionReportService
{
// Start of user code constants
    const FIELD_AUTHOR = "author";
    const FIELD_AWARD = "award";
    const FIELD_CLUB = "club";
    const FIELD_CLUB_SHORT = "clubShort";
    const FIELD_COMPETITION = "competition";
    const FIELD_DATE = "date";
    const FIELD_DISQUALIFIED = "disqualified";
    const FIELD_ENTRYNUMBER = "entryNumber";
    const FIELD_ENUMERATION = "enumeration";
    const FIELD_JUDGE = "judge";
    const FIELD_ORDER = "order";
    const FIELD_REMOVED = "removed";
    const FIELD_SECTION = "section";
    const FIELD_SECTION_SHORT = "sectionShort";
    const FIELD_SCORE = "score";
    const FIELD_TITLE = "title";
// End of user code

// Start of user code variables
    /**
     * @var Writer $writer
     */
    private Writer $writer;

    /**
     * @var array<string> $entryFields
     */
    private array $entryFields = [];

    /**
     * @var string $judge
     */
    private string $judge = '';
// End of user code

    /**
     *
     * @param Competition $competition
     * @param bool $insertHeader
     * @return String
     */
    public function entriesDetailed(Competition $competition, bool $insertHeader = true): String
    {
        // Start of user code operation.entriesDetailed
        $this->writer = new WordWriter();

        $this->setEntryFields([
            self::FIELD_ENTRYNUMBER,
            self::FIELD_TITLE,
            self::FIELD_AUTHOR,
            self::FIELD_SCORE,
            self::FIELD_AWARD
        ]);

        $this->start($competition, $insertHeader);
        if (!$competition->getAwards()->isEmpty()) {
            $this->writeAwards($competition);
        }
        $this->writeSections($competition);

        return $this->writer->save();
        // End of user code
    }

    /**
     *
     * @param Competition $competition
     * @param bool $insertHeader
     * @return String
     */
    public function entriesAnonymous(Competition $competition, bool $insertHeader = true): String
    {
        // Start of user code operation.entriesAnonymous
        $this->writer = new WordWriter();
        
        $this->setEntryFields([
            self::FIELD_ENTRYNUMBER,
            self::FIELD_TITLE,
            self::FIELD_SCORE,
            self::FIELD_AWARD
        ]);
        
        $this->start($competition, $insertHeader);
        $this->writeSections($competition);
        
        return $this->writer->save();
        // End of user code
    }

    /**
     *
     * @param Competition $competition
     * @return String
     */
    public function full(Competition $competition): String
    {
        // Start of user code operation.full
        $this->writer = new SpreadsheetWriter();

        $this->setEntryFields([
            self::FIELD_CLUB,
            self::FIELD_COMPETITION,
            self::FIELD_DATE,
            self::FIELD_JUDGE,
            self::FIELD_SECTION,
            self::FIELD_TITLE,
            self::FIELD_AUTHOR,
            self::FIELD_AWARD
        ]);

        $this->start($competition, false);
        $this->writer->setSectionTitle('All', 2);
        $this->writeSections($competition, true);

        $this->writer->addSection('Placed', 2);
        $this->writeSections($competition, true, 'Placed');

        foreach ($this->determineEntryAwardsGained($competition) as $award) {
            $this->writer->addSection($award, 2);
            $this->writeSections($competition, true, $award);
        }

        return $this->writer->save();
        // End of user code
    }

    /**
     *
     * @param Competition $competition
     * @return String
     */
    public function entryLabels(Competition $competition): String
    {
        // Start of user code operation.entryLabels
        $this->writer = new SpreadsheetWriter();

        $this->setEntryFields([
            self::FIELD_CLUB_SHORT,
            self::FIELD_COMPETITION,
            self::FIELD_DATE,
            self::FIELD_SECTION_SHORT,
            self::FIELD_ENTRYNUMBER
        ]);

        $this->start($competition, false);
        $this->writer->setSectionTitle('Entries', 2);
        $this->writeSections($competition, true);
        
        return $this->writer->save();
        // End of user code
    }

    /**
     *
     * @param Competition $competition
     * @return String
     */
    public function placeLabels(Competition $competition): String
    {
        // Start of user code operation.placeLabels
        $this->writer = new SpreadsheetWriter();

        $this->setEntryFields([
            self::FIELD_CLUB,
            self::FIELD_COMPETITION,
            self::FIELD_DATE,
            self::FIELD_SECTION,
            self::FIELD_ENUMERATION
        ]);

        $this->start($competition, false);
        $this->writer->setSectionTitle('Places', 2);
        $this->writer->startTable($this->writer->getStyleTable());
        $this->writeEntryHeader();
        $criteria = Criteria::create()
            ->orderBy([SELF::FIELD_ORDER => Criteria::ASC]);
        foreach ($competition->getSections()->matching($criteria) AS $section) {
            $this->writeSectionEnumerations($section, array('Third', 'Second', 'First'));
        }
        $this->writer->endTable();
        
        return $this->writer->save();
        // End of user code
    }

/*
 * Start of user code support
 */
    /**
     *
     * @param array<string> $entryFields
     */
    private function setEntryFields(array $entryFields): void
    {
        $this->entryFields = $entryFields;
    }
    
    /**
     *
     * @param Competition $competition
     * @param bool $insertHeader
     */
    private function start(Competition $competition, bool $insertHeader = true): void
    {
        $this->writer->setDocumentTitle($competition->getEvent()->getDefaultLabel());
        if ($insertHeader) {
            $this->competitionHeader($competition);
        } else {
            $this->writer->setSectionTitle($competition->getEvent()->getDefaultLabel());
        }
        
        if (in_array(self::FIELD_JUDGE, $this->entryFields)) {
            $this->judge = '';
            foreach ($competition->getJudges() as $judge) {
                $this->judge = $this->judge . ' ' . $judge->getDefaultLabel() . ' ' . $this->distinctions($judge);
                $this->judge = trim($this->judge);
            }
        }
    }
    
    /**
     *
     * @param Competition $competition
     */
    private function competitionHeader(Competition $competition): void
    {
        $this->writer->setTitle($competition->getEvent()->getOrganisedBy()->getDefaultLabel());
        $this->writer->writeTitle($competition->getEvent()->getDefaultLabel()
            . ', ' . $competition->getEvent()->getStart()->format('jS F Y'),
            1, $this->writer->getStyleHeader1());
        
        if ($competition->getJudges()->count() == 1) {
            foreach ($competition->getJudges() AS $judge) {
                $this->writer->writeTitle('Judge: ' . $judge->getDefaultLabel() . ' ' . $this->distinctions($judge),
                    2, $this->writer->getStyleHeader2());
            }
        } else {
            $first = true;
            foreach ($competition->getJudges() AS $judge) {
                $this->writer->writeText(($first ? 'Judges: ' : '    ') . $judge->getDefaultLabel() . ' ' . $this->distinctions($judge));
                $first = false;
            }
        }
    }
    
    /**
     *
     * @param Person $person
     */
    private function distinctions(Person $person): String
    {
        $distinctions = '';
        foreach ($person->getDistinctions() AS $distinction) {
            $distinctions = $distinctions . $distinction->getTitle() . ' ';
        }
        return trim($distinctions);
    }
    
    /**
     *
     * @param Competition $competition
     * 
     * @return array<int, string>
     */
    private function determineEntryAwardsGained(Competition $competition): array
    {
        $awards = array();
        foreach ($competition->getSections() AS $section) {
            foreach ($section->getEntries() AS $entry) {
                if (!is_null($entry->getAward()) && !is_null($entry->getAward()->getTitle())) {
                    $award = str_replace('Equal ', '', $entry->getAward()->getTitle());
                    if (!empty($award) && !in_array($award, $awards)) {
                        $awards[] = $award;
                    }
                }
            }
        }
        
        return $awards;
    }
    
    /**
     *
     * @param Competition $competition
     */
    private function writeAwards(Competition $competition): void
    {
        $this->writer->startTable($this->writer->getStyleTable());
        $criteria = Criteria::create()
        ->orderBy([SELF::FIELD_AWARD => Criteria::ASC]);
        foreach ($competition->getAwards()->matching($criteria) AS $award) {
            $this->writeAward($award);
        }
        $this->writer->endTable();
    }
    
    /**
     *
     * @param CompetitionAward $award
     */
    private function writeAward(CompetitionAward $award): void
    {
        //        $this->section_header(Model_AwardsCompetition::option($award->award));
        //        $this->entry($award->entry);
    }
    
    /**
     *
     * @param Competition $competition
     * @param bool $merged
     * @param string $entryFilter
     */
    private function writeSections(Competition $competition, bool $merged = false, string $entryFilter = null): void
    {
        if ($merged) {
            $this->writer->startTable($this->writer->getStyleTable());
            $this->writeEntryHeader();
        }
        
        $criteria = Criteria::create()
            ->orderBy([SELF::FIELD_ORDER => Criteria::ASC]);
        foreach ($competition->getSections()->matching($criteria) AS $section) {
            if (!$merged) {
                $this->writer->writeTitle($section->getDefaultLabel(),
                    2, $this->writer->getStyleHeader2());
                $this->writer->startTable($this->writer->getStyleTable());
                $this->writeEntryHeader();
            }
            $this->writeEntries($section, $entryFilter);
            if (!$merged) {
                $this->writer->endTable();
            }
        }
        
        if ($merged)
            $this->writer->endTable();
    }
    
    /**
     *
     * @param CompetitionSection $section
     * @param array<string> $enumerations
     */
    private function writeSectionEnumerations(CompetitionSection $section, array $enumerations): void
    {
        foreach ($enumerations as $enumeration) {
            $this->writeEnumeration($section, $enumeration);
        }
    }
    
    /**
     *
     */
    private function writeEntryHeader(): void
    {
        foreach ($this->entryFields AS $field) {
            if ($field == SELF::FIELD_ENTRYNUMBER) {
                $this->writeCell('Num', true);
            } else {
                $this->writeCell(ucfirst($field), true);
            }
        }
    }

    /**
     *
     * @param CompetitionSection $section
     * @param string $entryFilter
     */
    private function writeEntries(CompetitionSection $section, string $entryFilter = null): void
    {
        $criteria = Criteria::create()
//             ->where(Criteria::expr()->andX(
//                 Criteria::expr()->eq(SELF::FIELD_DISQUALIFIED, 0),
//                 Criteria::expr()->eq(SELF::FIELD_REMOVED, 0)
//             ))
            ->orderBy([
                SELF::FIELD_ENTRYNUMBER => Criteria::ASC,
                SELF::FIELD_TITLE => Criteria::ASC,
            ]);
        foreach ($section->getEntries()->matching($criteria) AS $entry) {
            if (!$entry->isRemoved() && !$entry->isDisqualified()) {
                if (is_null($entryFilter)
                    || !is_null($entry->getAward()) && !is_null($entry->getAward()->getTitle())
                        && (
                            str_replace('Equal ', '', $entry->getAward()->getTitle()) == $entryFilter
                                || $entryFilter == 'Placed'
                           )
                ) {
                    $this->writeEntry($section, $entry);
                }
            }
        }
    }
    
    /**
     *
     * @param CompetitionSection $section
     * @param SectionEntry $entry
     */
    private function writeEntry(CompetitionSection $section, SectionEntry $entry): void
    {
        $this->writer->nextRow();
        foreach ($this->entryFields AS $field) {
            $value = '';
            if ($field == self::FIELD_CLUB
                || $field == self::FIELD_CLUB_SHORT
                || $field == self::FIELD_COMPETITION
                || $field == self::FIELD_DATE
                || $field == self::FIELD_JUDGE
                || $field == self::FIELD_SECTION || $field == self::FIELD_SECTION_SHORT) {
                    $value = $this->getValue($field, $section);
            } else if ($field == self::FIELD_ENTRYNUMBER) {
                $value = $entry->getEntryNumber();
            } else if ($field == self::FIELD_AUTHOR) {
                $value = $entry->getImageVariant()->getImage()->getAuthor()->getDefaultLabel();
            } else if ($field == self::FIELD_TITLE) {
                $value = $entry->getTitle();
            } else if ($field == self::FIELD_AWARD) {
                if (!is_null($entry->getAward())) {
                    $value = $entry->getAward()->getTitle();
                }
            } else if ($field == self::FIELD_SCORE) {
                $value = $entry->getScore();
                if ($value == 0) {
                    $value = null;
                }
            } else {
                $value = 'Unknown field ' . $field;
            }
            
            $this->writeCell($value);
        }
    }
    
    /**
     *
     * @param CompetitionSection $section
     * @param string $enumeration
     */
    private function writeEnumeration(CompetitionSection $section, string $enumeration): void
    {
        $this->writer->nextRow();
        foreach ($this->entryFields AS $field) {
            $value = '';
            if ($field == self::FIELD_CLUB
                    || $field == self::FIELD_CLUB_SHORT
                    || $field == self::FIELD_COMPETITION
                    || $field == self::FIELD_DATE
                    || $field == self::FIELD_JUDGE
                    || $field == self::FIELD_SECTION || $field == self::FIELD_SECTION_SHORT) {
                $value = $this->getValue($field, $section);
            } else if ($field == self::FIELD_ENUMERATION) {
                $value = $enumeration;
            } else {
                $value = 'Unknown field ' . $field;
            }
            
            $this->writeCell($value);
        }
    }
    
    /**
     *
     * @param string $field
     * @param CompetitionSection $section
     */
    private function getValue(string $field, CompetitionSection $section): ?string
    {
        $value = '';
        if ($field == self::FIELD_CLUB) {
            $value = $section->getPartOf()->getEvent()->getOrganisedBy()->getDefaultLabel();
        } else if ($field == self::FIELD_CLUB_SHORT) {
            $value = $section->getPartOf()->getEvent()->getOrganisedBy()->getShortTitle();
        } else if ($field == self::FIELD_COMPETITION) {
            $value = preg_replace(
                '/ \([\w\h]+\)/', '',
                $section->getPartOf()->getEvent()->getDefaultLabel());
        } else if ($field == self::FIELD_DATE) {
            $value = $section->getPartOf()->getEvent()->getStart()->format('jS F Y');
        } else if ($field == self::FIELD_JUDGE) {
            $value = $this->judge;
        } else if ($field == self::FIELD_SECTION || $field == self::FIELD_SECTION_SHORT) {
            $value = preg_replace(
                '/ \([\w\h]+\)/', '',
                $section->getDefaultLabel());
            if (!is_null($value) && $field == self::FIELD_SECTION_SHORT)
                $value = preg_replace(
                    [
                        '/Monochrome/',
                        '/\w+: /',
                    ],
                    [
                         'Mono'
                    ],
                    $value);
        } else {
            $value = 'Unknown field ' . $field;
        }

        return $value;
    }
    
    /**
     *
     * @param mixed $content|null
     * @param bool $headerCell
     */
    private function writeCell($content, bool $headerCell = false): void
    {
        if ($headerCell) {
            $this->writer->writeCell($content,
                $this->writer->getStyleHeaderCellBorder(),
                $this->writer->getStyleHeaderCellFont(),
                $this->writer->getStyleHeaderCellParagraph());
        } else {
            $this->writer->writeCell($content,
                $this->writer->getStyleCellBorder(),
                $this->writer->getStyleCellFont(),
                $this->writer->getStyleCellParagraph());
        }
    }

// End of user code
}
