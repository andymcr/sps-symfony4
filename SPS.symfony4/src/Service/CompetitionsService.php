<?php
namespace App\Service;

use App\Entity\Competition;
// Start of user code use
// End of user code

class CompetitionsService
{
// Start of user code constants
// End of user code

// Start of user code variables
// End of user code

    /**
     *
     * @param Competition $competition
     */
    public function addMeritsSections(Competition $competition): void
    {
        // Start of user code operation.addMeritsSections
        // End of user code
    }

    /**
     *
     * @param Competition $competition
     */
    public function addQuarterlySections(Competition $competition): void
    {
        // Start of user code operation.addQuarterlySections
        // End of user code
    }

/*
 * Start of user code support
 */
// End of user code
}
