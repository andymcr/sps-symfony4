<?php
namespace App\Service;

use App\Entity\CumulativeGroup;
use App\Repository\CumulativeEntriesRepository;
use App\Repository\CumulativeCompetitionsRepository;
// Start of user code use
use App\Entity\CompetitionSection;
use App\Entity\CumulativeCompetition;
use App\Entity\CumulativeEntry;
use Doctrine\Common\Collections\Criteria;
use Uk\Me\AndyCarpenter\Writer\SpreadsheetWriter;
// End of user code

class CumulativeService
{
// Start of user code constants
// End of user code

// Start of user code variables
    /**
     * @var SpreadsheetWriter $writer
     */
    private SpreadsheetWriter $writer;
// End of user code

    /**
     * @var CumulativeEntriesRepository $cumulativeEntriesRepository
     */
    private CumulativeEntriesRepository $cumulativeEntriesRepository;

    /**
     * @var CumulativeCompetitionsRepository $cumulativeCompetitionsRepository
     */
    private CumulativeCompetitionsRepository $cumulativeCompetitionsRepository;

    public function __construct(CumulativeEntriesRepository $cumulativeEntriesRepository, CumulativeCompetitionsRepository $cumulativeCompetitionsRepository)
    {
        $this->cumulativeEntriesRepository = $cumulativeEntriesRepository;
        $this->cumulativeCompetitionsRepository = $cumulativeCompetitionsRepository;
    }

    /**
     *
     * @param CumulativeGroup $cumulativeGroup
     */
    public function tally(CumulativeGroup $cumulativeGroup): void
    {
        // Start of user code operation.tally
        foreach ($cumulativeGroup->getCompetitions() AS $cumulativeCompetition) {
            $this->tallyCompetition($cumulativeCompetition);
            $this->cumulativeCompetitionsRepository->flush();
        }
        // End of user code
    }

    /**
     *
     * @param CumulativeGroup $cumulativeGroup
     * @return String
     */
    public function summary(CumulativeGroup $cumulativeGroup): String
    {
        // Start of user code operation.summary
        $this->writer = new SpreadsheetWriter();
        $this->writer->setDocumentTitle($cumulativeGroup->getName());
        foreach ($cumulativeGroup->getCompetitions() AS $competition) {
            $this->writeCompetitionSummary($competition);
        }
        return $this->writer->save();
        // End of user code
    }

    /**
     *
     * @param CumulativeGroup $cumulativeGroup
     * @return String
     */
    public function details(CumulativeGroup $cumulativeGroup): String
    {
        // Start of user code operation.details
        $this->writer = new SpreadsheetWriter();
        $this->writer->setDocumentTitle($cumulativeGroup->getName());
        foreach ($cumulativeGroup->getCompetitions() AS $cumulativeCompetition) {
            $this->writeCompetitionDetails($cumulativeCompetition,
                $this->build($cumulativeCompetition));
        }
        return $this->writer->save();
        // End of user code
    }

/*
 * Start of user code support
 */
    /**
     * @param CumulativeCompetition $cumulativeCompetition
     */
    private function tallyCompetition(CumulativeCompetition $cumulativeCompetition): void
    {
        $authors = array();
        $cumulativeScores = array();
        $sectionEntries = array();
        foreach ($cumulativeCompetition->getFromSections() AS $section) {
            $criteria = Criteria::create()
                ->where(Criteria::expr()->neq('points', 0));
            foreach ($section->getEntries()->matching($criteria) AS $sectionEntry) {
                $author = $sectionEntry->getImageVariant()->getImage()->getAuthor();
                $authorId = $author->getId();
                if (!array_key_exists($authorId, $authors)) {
                    $authors[$authorId] = $author;
                    $cumulativeScores[$authorId] = 0;
                    $sectionEntries[$authorId] = array();
                }
                $cumulativeScores[$authorId] += $sectionEntry->getPoints();
                $sectionEntries[$authorId][] = $sectionEntry;
            }
        }

        $removedEntries = array();
        foreach ($cumulativeCompetition->getScores() AS $cumulativeEntry) {
            $authorId = $cumulativeEntry->getAuthor()->getId();
            if (array_key_exists($authorId, $authors)) {
                $cumulativeEntry->setScore($cumulativeScores[$authorId]);
                $cumulativeEntry->getEntries()->clear();
                foreach ($sectionEntries[$authorId] AS $sectionEntry) {
                    $cumulativeEntry->addEntry($sectionEntry);
                }
                $this->cumulativeEntriesRepository->save($cumulativeEntry);
                unset($authors[$authorId]);
            } else {
                $removedEntries[] = $cumulativeEntry;
            }
        }

        foreach ($authors as $author) {
            $authorId = $author->getId();
            $cumulativeEntry = new CumulativeEntry();
            $cumulativeEntry->setAuthor($author);
            $cumulativeEntry->setScore($cumulativeScores[$authorId]);
            foreach ($sectionEntries[$authorId] AS $sectionEntry) {
                $cumulativeEntry->addEntry($sectionEntry);
            }
            $this->cumulativeEntriesRepository->save($cumulativeEntry);
            $cumulativeCompetition->addScore($cumulativeEntry);
        }
        $this->cumulativeEntriesRepository->flush();

        foreach ($removedEntries AS $cumulativeEntry) {
            $cumulativeCompetition->removeScore($cumulativeEntry);
        }

        $this->cumulativeCompetitionsRepository->save($cumulativeCompetition, true);
    }

    /**
     * @param CumulativeCompetition $cumulativeCompetition
     * 
     * @return array<string, array<int|string, CompetitionSection>>
     */
    private function build(CumulativeCompetition $cumulativeCompetition): array
    {
        $sectionsByCompetition = [];
        foreach ($cumulativeCompetition->getFromSections() AS $section) {
            if (!array_key_exists($section->getPartOf()->getEvent()->getTitle(), $sectionsByCompetition)) {
                $sectionsByCompetition[$section->getPartOf()->getEvent()->getTitle()] = [];
            }
            $sectionsByCompetition[$section->getPartOf()->getEvent()->getTitle()][$section->getOrder()] = $section;
        }
        
        ksort($sectionsByCompetition);
        foreach ($sectionsByCompetition AS $title => $sections) {
            ksort($sections);
            $sectionsByCompetition[$title] = $sections;
        }
        
        return $sectionsByCompetition;
    }
    
    /**
     * @param CumulativeCompetition $cumulativeCompetition
     */
    private function writeCompetitionSummary(CumulativeCompetition $cumulativeCompetition): void
    {
        $this->writer->addSection($cumulativeCompetition->getName(), 2);
        $this->writeCell('Author', true);
        $this->writeCell('Score', true);
        $criteria = Criteria::create()->orderBy(array(
                'points' => Criteria::DESC,
            ));
        foreach ($cumulativeCompetition->getScores()->matching($criteria) AS $cumulativeEntry) {
            $this->writer->nextRow();
            $this->writeCell($cumulativeEntry->getAuthor()->getDefaultLabel());
            $this->writeCell($cumulativeEntry->getScore());
        }
    }
    
    /**
     * @param CumulativeCompetition $cumulativeCompetition
     * @param array<string, array<int|string, CompetitionSection>> $sectionsByCompetition
     */
    private function writeCompetitionDetails(CumulativeCompetition $cumulativeCompetition, $sectionsByCompetition): void
    {
        $this->writer->addSection($cumulativeCompetition->getName(), 2);
        $this->writeDetailHeader($sectionsByCompetition);
        $criteria = Criteria::create()->orderBy(array(
                'points' => Criteria::DESC,
            ));
        foreach ($cumulativeCompetition->getScores()->matching($criteria) AS $cumulativeEntry) {
            $this->writer->nextRow();
            $this->writeAuthorDetail($cumulativeEntry, $sectionsByCompetition);
        }
    }
    
    /**
     * @param array<string, array<int|string, CompetitionSection>> $sectionsByCompetition
     */
    private function writeDetailHeader($sectionsByCompetition): void
    {
        $this->writeCell('', true);
        foreach ($sectionsByCompetition AS $title => $sections) {
            $this->writeCell($title, true);
            for ($i = 1; $i < count($sections) && $i < 5; $i++) {
                $this->writeCell('', true);
            }
        }
        $this->writeCell('Total', true);
        
        $this->writer->nextRow();
        $this->writeCell('', true);
        foreach ($sectionsByCompetition AS $sections) {
            foreach ($sections AS $section) {
                $this->writeCell($section->getTitle(), true);
            }
        }
    }
    
    /**
     * @param CumulativeEntry $cumulativeEntry
     * @param array<string, array<int|string, CompetitionSection>> $sectionsByCompetition
     */
    private function writeAuthorDetail(CumulativeEntry $cumulativeEntry, $sectionsByCompetition): void
    {
        $this->writeCell($cumulativeEntry->getAuthor()->getDefaultLabel());
        foreach ($sectionsByCompetition AS $sections) {
            foreach ($sections AS $section) {
                $sectionScore = $this->sectionScore($cumulativeEntry, $section);
                if ($sectionScore > 0) {
                    $this->writeCell($sectionScore);
                } else {
                    $this->writeCell('');
                }
            }
        }
        $this->writeCell($cumulativeEntry->getScore());
    }
    
    /**
     * @param CumulativeEntry $cumulativeEntry
     * @param CompetitionSection $section
     */
    private function sectionScore(CumulativeEntry $cumulativeEntry, CompetitionSection $section): int
    {
        $score = 0;
        foreach ($cumulativeEntry->getEntries() AS $sectionEntry) {
            if ($sectionEntry->getEnteredIn()->getId() == $section->getId()) {
                $score += $sectionEntry->getPoints();
            }
        }
        return $score;
    }
    
    /**
     * @param mixed $content
     * @param bool $headerCell
     */
    private function writeCell($content, bool $headerCell = false): void
    {
        if ($headerCell) {
            $this->writer->writeCell($content,
                $this->writer->getStyleHeaderCellBorder(),
                $this->writer->getStyleHeaderCellFont(),
                $this->writer->getStyleHeaderCellParagraph());
        } else {
            $this->writer->writeCell($content,
                $this->writer->getStyleCellBorder(),
                $this->writer->getStyleCellFont(),
                $this->writer->getStyleCellParagraph());
        }
    }
// End of user code
}
