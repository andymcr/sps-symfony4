<?php
namespace App\Service;

// Start of user code use
use Uk\Me\AndyCarpenter\Writer\SpreadsheetWriter;
use Uk\Me\AndyCarpenter\Writer\Writer;
// End of user code

class EventsService
{
// Start of user code constants
// End of user code

// Start of user code variables
    /**
     * @var Writer $writer
     */
    private Writer $writer;
// End of user code

    /**
     *
     * @return String
     */
    public function syllabus(): String
    {
        // Start of user code operation.syllabus
        $this->writer = new SpreadsheetWriter();
        
        return $this->writer->save();
        // End of user code
    }

    /**
     *
     * @return String
     */
    public function syllabusNext(): String
    {
        // Start of user code operation.syllabusNext
        $this->writer = new SpreadsheetWriter();
        
        return $this->writer->save();
        // End of user code
    }

/*
 * Start of user code support
 */
// End of user code
}
