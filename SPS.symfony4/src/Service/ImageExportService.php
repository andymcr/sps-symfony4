<?php
namespace App\Service;

use App\Entity\Competition;
// Start of user code use
use App\Entity\CompetitionSection;
use App\Entity\SectionEntry;
// End of user code

class ImageExportService
{
// Start of user code constants
// End of user code

// Start of user code variables
    /**
     * @var \ZipArchive $zip
     */
    var $zip;
    
    /**
     * @var bool $anonymous
     */
    var $anonymous = true;
// End of user code

    /**
     *
     * @param Competition $competition
     * @return String
     */
    public function judge(Competition $competition): String
    {
        // Start of user code operation.judge
        return $this->zipCompetition($competition);
        // End of user code
    }

    /**
     *
     * @param Competition $competition
     * @return String
     */
    public function dicentra(Competition $competition): String
    {
        // Start of user code operation.dicentra
        $this->anonymous = false;
        return $this->zipCompetition($competition);
        // End of user code
    }

/*
 * Start of user code support
 */
    /**
     *
     * @param Competition $competition
     */
    private function zipCompetition(Competition $competition): string
    {
        $zipPath = tempnam('', '');
        if (!$zipPath) {
            throw new \Exception("Unable to create temporary zip file");
        }
        $this->zip = new \ZipArchive();
        $this->zip->open($zipPath, \ZipArchive::CREATE);
        
        $orderSize = 1;
        if (count($competition->getSections()) > 9) {
            $orderSize = 2;
        }
        foreach ($competition->getSections() AS $section) {
            $this->zipSection($section, $orderSize);
        }
        
        $this->zip->close();
        return $zipPath;
    }
    
    /**
     *
     * @param CompetitionSection $section
     * @param int $orderSize The number of digits  needed to show the order number
     */
    private function zipSection(CompetitionSection $section, int $orderSize): void
    {
        $sectionName = str_pad((string) $section->getOrder(), $orderSize, '0', STR_PAD_LEFT)
        . ' ' . $this->sectionTitle($section);
        
        $entrySize = 1;
        if (count($section->getEntries()) > 9) {
            $entrySize = 2;
        }
        foreach ($section->getEntries() AS $entry) {
            if (!empty($entry->getImageVariant()->getFilePath())) {
                $this->zipEntry($sectionName, $entry, $entrySize);
            }
        }
        
    }
    
    /**
     *
     * @param CompetitionSection $section
     */
    private function sectionTitle(CompetitionSection $section): string
    {
        return str_replace(
            array(':'),
            array(' -'),
            $section->getTitle()
            );
    }
    
    /**
     *
     * @param string $sectionName
     * @param SectionEntry $entry
     * @param int $entrySize
     */
    private function zipEntry(string $sectionName, SectionEntry $entry, int $entrySize): void
    {
        $imagePath = 'http://images.salephoto.org.uk/'
            . $entry->getImageVariant()->getFilePath();
        $imageFilename = tempnam('', '');
        if ($imageFilename) {
            copy ($imagePath, $imageFilename);
             
            $this->setMetadata($entry, $imageFilename);
            
            $entryPath = $sectionName . DIRECTORY_SEPARATOR
                . $this->entryName($entry, $entrySize);
            $this->zip->addFile($imageFilename, $entryPath);
       }
    }
    
    /**
     *
     * @param SectionEntry $entry
     * @param string $imageFilename
     */
    private function setMetadata(SectionEntry $entry, string $imageFilename): void
    {
        $encodedTags = '';
        
        getimagesize($imageFilename, $info);
        $origIptc = null;
        if(isset($info['APP13'])) {
            $origIptc = iptcparse($info['APP13']);
            if ($origIptc) {
                foreach ($origIptc as $tag => $value) {
                    if (substr($tag, 0, 1) == '2'
                            && strcmp($tag, '2#080') !== 0
                            && strcmp($tag, '2#116') !== 0
                            && strcmp($tag, '2#120') !== 0
                            && is_string($value[0])) {
                            $tag = substr($tag, 2);
                        foreach ($value as $row ) {
                            $encodedTags .= $this->iptc_make_tag(2, (int) $tag, $row);
                        }
                    }
                }
            }
        }
        
        $rawTags = array(
            '2#105' => $entry->getTitle(),
            '2#120' => $entry->getTitle(),
        );
        foreach($rawTags as $tag => $value) {
            if (!is_null($value)) {
                $tag = substr($tag, 2);
                $encodedTags .= $this->iptc_make_tag(2, (int) $tag, $value);
            }
        }
        $taggedImage = iptcembed($encodedTags, $imageFilename);
        if (is_string($taggedImage)) {
            $fp = fopen($imageFilename, "wb");
            if ($fp) {
                fwrite($fp, $taggedImage);
                fclose($fp);
            }
        }
    }
    
    /**
     * iptc_make_tag() function by Thies C. Arntzen
     *
     * @param int $rec
     * @param int $data
     * @param string $value
     */
    function iptc_make_tag(int $rec, int $data, string $value): string
    {
        $length = strlen($value);
        $retval = chr(0x1C) . chr($rec) . chr($data);
        
        if($length < 0x8000)
        {
            $retval .= chr($length >> 8) .  chr($length & 0xFF);
        }
        else
        {
            $retval .= chr(0x80) .
            chr(0x04) .
            chr(($length >> 24) & 0xFF) .
            chr(($length >> 16) & 0xFF) .
            chr(($length >> 8) & 0xFF) .
            chr($length & 0xFF);
        }
        
        return $retval . $value;
    }
    
    /**
     *
     * @param SectionEntry $entry
     * @param int $entrySize
     */
    private function entryName(SectionEntry $entry, int $entrySize): string
    {
        $entryNumber = str_pad((string) $entry->getEntryNumber(), $entrySize, '0', STR_PAD_LEFT);
        $author = $entry->getImageVariant()->getImage()->getAuthor()->getDefaultLabel();
        if ($this->anonymous)
        {
            return $entryNumber . ' ' . $this->entryTitle($entry) . '.jpg';
        } else {
            return $entryNumber . ' ' . $this->entryTitle($entry)
            . '_' . $author . '.jpg';
        }
    }
    
    /**
     *
     * @param SectionEntry $entry
     */
    private function entryTitle(SectionEntry $entry): string
    {
        return str_replace(
            array('"', // '', '', '', '',
                '�', '�',
                '?'),
            array('\'', // '\'', '\'', '\'', '-',
                'e', 'o'),
            (string) $entry->getTitle()
            );
    }
// End of user code
}
