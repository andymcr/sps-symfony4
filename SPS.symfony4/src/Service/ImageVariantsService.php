<?php
namespace App\Service;

use App\Entity\ImageVariant;
use App\Entity\SectionEntry;
use App\Repository\ImageVariantsRepository;
// Start of user code use
use Symfony\Component\Filesystem\Exception\RuntimeException;
// End of user code

class ImageVariantsService
{
    private const IMAGE_SERVER = 'https://images.salephoto.org.uk/';

    private const MAX_DOWNLOAD_WIDTH = 800;

    private const MAX_DOWNLOAD_HEIGHT = 600;

// Start of user code constants
// End of user code

// Start of user code variables
// End of user code

    /**
     * @var ImageVariantsRepository $imageVariantsRepository
     */
    private ImageVariantsRepository $imageVariantsRepository;

    public function __construct(ImageVariantsRepository $imageVariantsRepository)
    {
        $this->imageVariantsRepository = $imageVariantsRepository;
    }

    /**
     *
     * @param ImageVariant $imageVariant
     */
    public function toggleClubShowcase(ImageVariant $imageVariant): void
    {
        // Start of user code operation.toggleClubShowcase
        $imageVariant->setClubShowcase(!$imageVariant->isClubShowcase());
        $this->imageVariantsRepository->save($imageVariant, true);
        // End of user code
    }

    /**
     *
     * @param ImageVariant $imageVariant
     */
    public function togglePersonalShowcase(ImageVariant $imageVariant): void
    {
        // Start of user code operation.togglePersonalShowcase
        $imageVariant->setPersonalShowcase(!$imageVariant->isPersonalShowcase());
        $this->imageVariantsRepository->save($imageVariant, true);
        // End of user code
    }

    /**
     *
     * @param SectionEntry $entry
     * @return String
     */
    public function exportImage(SectionEntry $entry): String
    {
        // Start of user code operation.exportImage
        $resizedImage = $this->resizeImage(
            $this->filename($entry->getImageVariant()),
            self::MAX_DOWNLOAD_WIDTH, self::MAX_DOWNLOAD_HEIGHT);

        $filename = tempnam('', '');
        if ($filename === FALSE) {
            throw new RuntimeException("Problem getting temporary image filename");
        }
        $result = imagejpeg($resizedImage, $filename);
        if ($result === FALSE) {
            throw new RuntimeException("Problem saving image file");
        }

        return $filename;
        // End of user code
    }

    /**
     *
     */
    public function setImageSizes(): void
    {
        // Start of user code operation.setImageSizes
        $i = 0;
        foreach ($this->imageVariantsRepository->findWithoutSize() as $variant) {
            try {
                $info = $this->getImageInfo($this->filename($variant));
                $variant->setWidth($info[0]);
                $variant->setHeight($info[1]);
                $this->imageVariantsRepository->save($variant);
                $i++;
                if ($i > 100) {
                    $this->imageVariantsRepository->flush();
                    $i = 0;
                }
            } catch (\Exception $e) {
                print $this->filename($variant);
            }
        }
        
        $this->imageVariantsRepository->flush();
        // End of user code
    }

/*
 * Start of user code support
 */
    /**
     * 
     * @param ImageVariant $imageVariant
     * @return String
     */
    private function filename(ImageVariant $imageVariant): string
    {
        return self::IMAGE_SERVER . $imageVariant->getFilePath();
    }

    /**
     * @param String $filename
     * @return array<int|string, mixed>
     */
    private function getImageInfo(String $filename): array
    {
        $info = getimagesize($filename);
        if ($info === FALSE) {
             throw new RuntimeException($filename);
        }

        return $info;
    }

    /**
     * 
     * @param String $filename
     * @param int $maxWidth
     * @param int $maxHeight
     * @return \GdImage
     */
    private function resizeImage(String $filename, int $maxWidth, int $maxHeight): \GdImage
    {
        $info = $this->getImageInfo($filename);
        $currentWidth = $info[0];
        $currentHeight = $info[1];
        $ratio = $currentWidth / $currentHeight;
        if ($maxWidth/$maxHeight > $ratio) {
            $newWidth = $maxHeight*$ratio;
            $newHeight = $maxHeight;
        } else {
            $newHeight = (int) round($maxWidth/$ratio);
            $newWidth = $maxWidth;
        }

        $sourceImage = imagecreatefromjpeg($filename);
        if ($sourceImage === FALSE) {
            throw new RuntimeException("Problem loading image file");
        }

        $resizedImage = imagescale($sourceImage, $newWidth, $newHeight);
        if ($resizedImage === FALSE) {
            throw new RuntimeException("Problem creating image");
        }

        return $resizedImage;
    }
// End of user code
}
