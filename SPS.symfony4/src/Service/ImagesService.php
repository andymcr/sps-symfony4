<?php
namespace App\Service;

use App\Entity\Image;
use App\Service\ImageVariantsService;
// Start of user code use
// End of user code

class ImagesService
{
// Start of user code constants
// End of user code

// Start of user code variables
// End of user code

    /**
     * @var ImageVariantsService $imageVariantsService
     */
    private $imageVariantsService;

    public function __construct(ImageVariantsService $imageVariantsService)
    {
        $this->imageVariantsService = $imageVariantsService;
    }

    /**
     *
     * @param Image $image
     */
    public function togglePersonalShowcase(Image $image): void
    {
        // Start of user code operation.togglePersonalShowcase
        foreach ($image->getVariants() as $variant) {
            $this->imageVariantsService->togglePersonalShowcase($variant);
        }
        // End of user code
    }

/*
 * Start of user code support
 */
// End of user code
}
