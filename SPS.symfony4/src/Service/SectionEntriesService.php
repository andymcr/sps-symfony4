<?php
namespace App\Service;

use App\Entity\SectionEntry;
use App\Service\ImageVariantsService;
// Start of user code use
// End of user code

class SectionEntriesService
{
// Start of user code constants
// End of user code

// Start of user code variables
// End of user code

    /**
     * @var ImageVariantsService $imageVariantsService
     */
    private $imageVariantsService;

    public function __construct(ImageVariantsService $imageVariantsService)
    {
        $this->imageVariantsService = $imageVariantsService;
    }

    /**
     *
     * @param SectionEntry $entry
     */
    public function toggleClubShowcase(SectionEntry $entry): void
    {
        // Start of user code operation.toggleClubShowcase
        $this->imageVariantsService->toggleClubShowcase($entry->getImageVariant());
        // End of user code
    }

/*
 * Start of user code support
 */
// End of user code
}
