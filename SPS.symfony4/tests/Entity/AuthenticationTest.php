<?php
namespace App\Tests\Entity;

use App\Entity\Authentication;
use PHPUnit\Framework\TestCase;


class AuthenticationTest extends TestCase
{
    public function testUsernameDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals('', $authentication->getUsername());
    }

    public function testEnabledDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertTrue($authentication->isEnabled());
    }

    public function testEmailDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals('', $authentication->getEmail());
    }

    public function testEmailVerifiedDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertFalse($authentication->isEmailVerified());
    }

    public function testPasswordDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals('', $authentication->getPassword());
    }

    public function testLocaledDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertFalse($authentication->isLocaled());
    }

    public function testExpiredDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertFalse($authentication->isExpired());
    }

    public function testExpiresAtDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals(new \DateTimeImmutable('00:00:00'), $authentication->getExpiresAt());
    }

    public function testPasswordChangedAtDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals(new \DateTimeImmutable('00:00:00'), $authentication->getPasswordChangedAt());
    }

    public function testCredentialsExpiredDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertFalse($authentication->isCredentialsExpired());
    }

    public function testCredentialsExpireAtDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals(new \DateTimeImmutable('00:00:00'), $authentication->getCredentialsExpireAt());
    }

    public function testRolesDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertCount(1, $authentication->getRoles());
    }

    public function testLastLoginDefault(): void
    {
    	$authentication = new Authentication();
        $this->assertEquals(new \DateTime('0000-00-00 00:00:00'), $authentication->getLastLogin());
    }

}
