<?php
namespace App\Tests\Entity;

use App\Entity\AuthorScore;
use PHPUnit\Framework\TestCase;


class AuthorScoreTest extends TestCase
{
    public function testPointsDefault(): void
    {
    	$authorScore = new AuthorScore();
        $this->assertEquals(0, $authorScore->getPoints());
    }

    public function testAuthorDefault(): void
    {
    	$authorScore = new AuthorScore();
        $this->assertNull($authorScore->getAuthor());
    }

    public function testEntriesDefault(): void
    {
    	$authorScore = new AuthorScore();
        $this->assertCount(0, $authorScore->getEntries());
    }

    public function testPointsForDefault(): void
    {
    	$authorScore = new AuthorScore();
        $this->expectError();
        $this->assertNull($authorScore->getPointsFor());
    }

}
