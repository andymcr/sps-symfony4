<?php
namespace App\Tests\Entity;

use App\Entity\CompetitionAward;
use PHPUnit\Framework\TestCase;


class CompetitionAwardTest extends TestCase
{
    public function testAwardDefault(): void
    {
    	$competitionAward = new CompetitionAward();
        $this->assertEquals(0, $competitionAward->getAward());
    }

    public function testEntryDefault(): void
    {
    	$competitionAward = new CompetitionAward();
        $this->assertNull($competitionAward->getEntry());
    }

    public function testAwardForDefault(): void
    {
    	$competitionAward = new CompetitionAward();
        $this->expectError();
        $this->assertNull($competitionAward->getAwardFor());
    }

}
