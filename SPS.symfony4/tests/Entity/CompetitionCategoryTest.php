<?php
namespace App\Tests\Entity;

use App\Entity\CompetitionCategory;
use PHPUnit\Framework\TestCase;


class CompetitionCategoryTest extends TestCase
{
    public function testShortNameDefault(): void
    {
    	$competitionCategory = new CompetitionCategory();
        $this->assertEquals('', $competitionCategory->getShortName());
    }

    public function testFullNameDefault(): void
    {
    	$competitionCategory = new CompetitionCategory();
        $this->assertEquals('', $competitionCategory->getFullName());
    }

    public function testRankDefault(): void
    {
    	$competitionCategory = new CompetitionCategory();
        $this->assertEquals(0, $competitionCategory->getRank());
    }

    public function testParentDefault(): void
    {
    	$competitionCategory = new CompetitionCategory();
        $this->assertNull($competitionCategory->getParent());
    }

    public function testSubcategoriesDefault(): void
    {
    	$competitionCategory = new CompetitionCategory();
        $this->assertCount(0, $competitionCategory->getSubcategories());
    }

}
