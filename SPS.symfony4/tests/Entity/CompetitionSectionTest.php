<?php
namespace App\Tests\Entity;

use App\Entity\CompetitionSection;
use PHPUnit\Framework\TestCase;


class CompetitionSectionTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->assertEquals('', $competitionSection->getTitle());
    }

    public function testOrderDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->assertEquals(0, $competitionSection->getOrder());
    }

    public function testMaximumEntriesDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->assertEquals(0, $competitionSection->getMaximumEntries());
    }

    public function testAcceptanceScoreDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->assertEquals(0, $competitionSection->getAcceptanceScore());
    }

    public function testContributesToDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->assertCount(0, $competitionSection->getContributesTo());
    }

    public function testPartOfDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->expectError();
        $this->assertNull($competitionSection->getPartOf());
    }

    public function testEntriesDefault(): void
    {
    	$competitionSection = new CompetitionSection();
        $this->assertCount(0, $competitionSection->getEntries());
    }

}
