<?php
namespace App\Tests\Entity;

use App\Entity\Competition;
use PHPUnit\Framework\TestCase;


class CompetitionTest extends TestCase
{
    public function testWebsiteDefault(): void
    {
    	$competition = new Competition();
        $this->assertEquals('', $competition->getWebsite());
    }

    public function testMaximumScoreDefault(): void
    {
    	$competition = new Competition();
        $this->assertEquals(0, $competition->getMaximumScore());
    }

    public function testPublicAuthorsDefault(): void
    {
    	$competition = new Competition();
        $this->assertFalse($competition->isPublicAuthors());
    }

    public function testPublicResultsDefault(): void
    {
    	$competition = new Competition();
        $this->assertFalse($competition->isPublicResults());
    }

    public function testSubmissionDeadlineDefault(): void
    {
    	$competition = new Competition();
        $this->assertEquals(new \DateTimeImmutable('00:00:00'), $competition->getSubmissionDeadline());
    }

    public function testSecretaryDefault(): void
    {
    	$competition = new Competition();
        $this->assertNull($competition->getSecretary());
    }

    public function testCategoryDefault(): void
    {
    	$competition = new Competition();
        $this->assertNull($competition->getCategory());
    }

    public function testImageDefault(): void
    {
    	$competition = new Competition();
        $this->assertNull($competition->getImage());
    }

    public function testJudgesDefault(): void
    {
    	$competition = new Competition();
        $this->assertCount(0, $competition->getJudges());
    }

    public function testEventDefault(): void
    {
    	$competition = new Competition();
        $this->expectError();
        $this->assertNull($competition->getEvent());
    }

    public function testSectionsDefault(): void
    {
    	$competition = new Competition();
        $this->assertCount(0, $competition->getSections());
    }

    public function testAwardsDefault(): void
    {
    	$competition = new Competition();
        $this->assertCount(0, $competition->getAwards());
    }

}
