<?php
namespace App\Tests\Entity;

use App\Entity\CumulativeCompetition;
use PHPUnit\Framework\TestCase;


class CumulativeCompetitionTest extends TestCase
{
    public function testNameDefault(): void
    {
    	$cumulativeCompetition = new CumulativeCompetition();
        $this->assertEquals('', $cumulativeCompetition->getName());
    }

    public function testPrivateDefault(): void
    {
    	$cumulativeCompetition = new CumulativeCompetition();
        $this->assertFalse($cumulativeCompetition->isPrivate());
    }

    public function testFromSectionsDefault(): void
    {
    	$cumulativeCompetition = new CumulativeCompetition();
        $this->assertCount(0, $cumulativeCompetition->getFromSections());
    }

    public function testPartOfDefault(): void
    {
    	$cumulativeCompetition = new CumulativeCompetition();
        $this->expectError();
        $this->assertNull($cumulativeCompetition->getPartOf());
    }

    public function testScoresDefault(): void
    {
    	$cumulativeCompetition = new CumulativeCompetition();
        $this->assertCount(0, $cumulativeCompetition->getScores());
    }

}
