<?php
namespace App\Tests\Entity;

use App\Entity\CumulativeEntry;
use PHPUnit\Framework\TestCase;


class CumulativeEntryTest extends TestCase
{
    public function testScoreDefault(): void
    {
    	$cumulativeEntry = new CumulativeEntry();
        $this->assertEquals(0, $cumulativeEntry->getScore());
    }

    public function testAuthorDefault(): void
    {
    	$cumulativeEntry = new CumulativeEntry();
        $this->assertNull($cumulativeEntry->getAuthor());
    }

    public function testEntriesDefault(): void
    {
    	$cumulativeEntry = new CumulativeEntry();
        $this->assertCount(0, $cumulativeEntry->getEntries());
    }

    public function testPartOfDefault(): void
    {
    	$cumulativeEntry = new CumulativeEntry();
        $this->expectError();
        $this->assertNull($cumulativeEntry->getPartOf());
    }

}
