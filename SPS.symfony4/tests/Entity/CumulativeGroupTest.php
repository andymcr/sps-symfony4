<?php
namespace App\Tests\Entity;

use App\Entity\CumulativeGroup;
use PHPUnit\Framework\TestCase;


class CumulativeGroupTest extends TestCase
{
    public function testNameDefault(): void
    {
    	$cumulativeGroup = new CumulativeGroup();
        $this->assertEquals('', $cumulativeGroup->getName());
    }

    public function testCompetitionsDefault(): void
    {
    	$cumulativeGroup = new CumulativeGroup();
        $this->assertCount(0, $cumulativeGroup->getCompetitions());
    }

}
