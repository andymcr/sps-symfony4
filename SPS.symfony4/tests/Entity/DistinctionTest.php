<?php
namespace App\Tests\Entity;

use App\Entity\Distinction;
use PHPUnit\Framework\TestCase;


class DistinctionTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$distinction = new Distinction();
        $this->assertEquals('', $distinction->getTitle());
    }

    public function testRankDefault(): void
    {
    	$distinction = new Distinction();
        $this->assertEquals(0, $distinction->getRank());
    }

}
