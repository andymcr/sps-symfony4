<?php
namespace App\Tests\Entity;

use App\Entity\Event;
use PHPUnit\Framework\TestCase;


class EventTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$event = new Event();
        $this->assertEquals('', $event->getTitle());
    }

    public function testCategoryDefault(): void
    {
    	$event = new Event();
        $this->assertEquals(0, $event->getCategory());
    }

    public function testDraftDefault(): void
    {
    	$event = new Event();
        $this->assertFalse($event->isDraft());
    }

    public function testCancelledDefault(): void
    {
    	$event = new Event();
        $this->assertFalse($event->isCancelled());
    }

    public function testPostponedDefault(): void
    {
    	$event = new Event();
        $this->assertFalse($event->isPostponed());
    }

    public function testPubliciseDefault(): void
    {
    	$event = new Event();
        $this->assertTrue($event->isPublicise());
    }

    public function testStartDefault(): void
    {
    	$event = new Event();
        $this->assertEquals(new \DateTimeImmutable('00:00:00'), $event->getStart());
    }

    public function testDurationDefault(): void
    {
    	$event = new Event();
        $this->assertEquals(new \DateTime('02:00:00'), $event->getDuration());
    }

    public function testOrganisedByDefault(): void
    {
    	$event = new Event();
        $this->assertNull($event->getOrganisedBy());
    }

    public function testVenueDefault(): void
    {
    	$event = new Event();
        $this->assertNull($event->getVenue());
    }

}
