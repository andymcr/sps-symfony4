<?php
namespace App\Tests\Entity;

use App\Entity\Image;
use PHPUnit\Framework\TestCase;


class ImageTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$image = new Image();
        $this->assertEquals('', $image->getTitle());
    }

    public function testAuthorDefault(): void
    {
    	$image = new Image();
        $this->expectError();
        $this->assertNull($image->getAuthor());
    }

    public function testVariantsDefault(): void
    {
    	$image = new Image();
        $this->assertCount(0, $image->getVariants());
    }

}
