<?php
namespace App\Tests\Entity;

use App\Entity\ImageVariant;
use PHPUnit\Framework\TestCase;


class ImageVariantTest extends TestCase
{
    public function testDescriptionDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertEquals('', $imageVariant->getDescription());
    }

    public function testFilePathDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertEquals('', $imageVariant->getFilePath());
    }

    public function testClubShowcaseDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertFalse($imageVariant->isClubShowcase());
    }

    public function testPersonalShowcaseDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertFalse($imageVariant->isPersonalShowcase());
    }

    public function testPhotoDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertFalse($imageVariant->isPhoto());
    }

    public function testWidthDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertEquals(0, $imageVariant->getWidth());
    }

    public function testHeightDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertEquals(0, $imageVariant->getHeight());
    }

    public function testEntriesDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->assertCount(0, $imageVariant->getEntries());
    }

    public function testImageDefault(): void
    {
    	$imageVariant = new ImageVariant();
        $this->expectError();
        $this->assertNull($imageVariant->getImage());
    }

}
