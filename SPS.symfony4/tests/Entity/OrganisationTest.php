<?php
namespace App\Tests\Entity;

use App\Entity\Organisation;
use PHPUnit\Framework\TestCase;


class OrganisationTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$organisation = new Organisation();
        $this->assertEquals('', $organisation->getTitle());
    }

    public function testShortTitleDefault(): void
    {
    	$organisation = new Organisation();
        $this->assertEquals('', $organisation->getShortTitle());
    }

    public function testWebsiteDefault(): void
    {
    	$organisation = new Organisation();
        $this->assertEquals('', $organisation->getWebsite());
    }

    public function testMembersDefault(): void
    {
    	$organisation = new Organisation();
        $this->assertCount(0, $organisation->getMembers());
    }

    public function testEventsDefault(): void
    {
    	$organisation = new Organisation();
        $this->assertCount(0, $organisation->getEvents());
    }

}
