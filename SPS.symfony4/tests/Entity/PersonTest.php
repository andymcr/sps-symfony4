<?php
namespace App\Tests\Entity;

use App\Entity\Person;
use PHPUnit\Framework\TestCase;


class PersonTest extends TestCase
{
    public function testUsernameDefault(): void
    {
    	$person = new Person();
        $this->assertEquals('', $person->getUsername());
    }

    public function testFamilyNameDefault(): void
    {
    	$person = new Person();
        $this->assertEquals('', $person->getFamilyName());
    }

    public function testForenamesDefault(): void
    {
    	$person = new Person();
        $this->assertEquals('', $person->getForenames());
    }

    public function testSalutationDefault(): void
    {
    	$person = new Person();
        $this->assertEquals('', $person->getSalutation());
    }

    public function testWebsiteDefault(): void
    {
    	$person = new Person();
        $this->assertEquals('', $person->getWebsite());
    }

    public function testShowcaseImageDefault(): void
    {
    	$person = new Person();
        $this->assertNull($person->getShowcaseImage());
    }

    public function testMemberOfDefault(): void
    {
    	$person = new Person();
        $this->assertNull($person->getMemberOf());
    }

    public function testDistinctionsDefault(): void
    {
    	$person = new Person();
        $this->assertCount(0, $person->getDistinctions());
    }

    public function testHasJudgedDefault(): void
    {
    	$person = new Person();
        $this->assertCount(0, $person->getHasJudged());
    }

    public function testHasPresentedDefault(): void
    {
    	$person = new Person();
        $this->assertCount(0, $person->getHasPresented());
    }

    public function testImagesDefault(): void
    {
    	$person = new Person();
        $this->assertCount(0, $person->getImages());
    }

}
