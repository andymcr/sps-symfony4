<?php
namespace App\Tests\Entity;

use App\Entity\SectionAward;
use PHPUnit\Framework\TestCase;


class SectionAwardTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$sectionAward = new SectionAward();
        $this->assertEquals('', $sectionAward->getTitle());
    }

    public function testRankDefault(): void
    {
    	$sectionAward = new SectionAward();
        $this->assertEquals(0, $sectionAward->getRank());
    }

}
