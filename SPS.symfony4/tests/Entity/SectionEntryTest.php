<?php
namespace App\Tests\Entity;

use App\Entity\SectionEntry;
use PHPUnit\Framework\TestCase;


class SectionEntryTest extends TestCase
{
    public function testTitleDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertEquals('', $sectionEntry->getTitle());
    }

    public function testEntryNumberDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertEquals(0, $sectionEntry->getEntryNumber());
    }

    public function testRemovalOrderDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertEquals(0, $sectionEntry->getRemovalOrder());
    }

    public function testScoreDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertEquals(0.0, $sectionEntry->getScore());
    }

    public function testPointsDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertEquals(0, $sectionEntry->getPoints());
    }

    public function testRemovedDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertFalse($sectionEntry->isRemoved());
    }

    public function testDisqualifiedDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertFalse($sectionEntry->isDisqualified());
    }

    public function testTaggedDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertFalse($sectionEntry->isTagged());
    }

    public function testAwardDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertNull($sectionEntry->getAward());
    }

    public function testImageVariantDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->assertNull($sectionEntry->getImageVariant());
    }

    public function testEnteredInDefault(): void
    {
    	$sectionEntry = new SectionEntry();
        $this->expectError();
        $this->assertNull($sectionEntry->getEnteredIn());
    }

}
