<?php
namespace App\Tests\Entity;

use App\Entity\StaticText;
use PHPUnit\Framework\TestCase;


class StaticTextTest extends TestCase
{
    public function testNameDefault(): void
    {
    	$staticText = new StaticText();
        $this->assertEquals('', $staticText->getName());
    }

    public function testContentDefault(): void
    {
    	$staticText = new StaticText();
        $this->assertEquals('', $staticText->getContent());
    }

}
