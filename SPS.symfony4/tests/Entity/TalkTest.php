<?php
namespace App\Tests\Entity;

use App\Entity\Talk;
use PHPUnit\Framework\TestCase;


class TalkTest extends TestCase
{
    public function testSynopsisDefault(): void
    {
    	$talk = new Talk();
        $this->assertEquals('', $talk->getSynopsis());
    }

    public function testPresentersDefault(): void
    {
    	$talk = new Talk();
        $this->assertCount(0, $talk->getPresenters());
    }

    public function testEventDefault(): void
    {
    	$talk = new Talk();
        $this->expectError();
        $this->assertNull($talk->getEvent());
    }

}
