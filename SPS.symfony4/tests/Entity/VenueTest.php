<?php
namespace App\Tests\Entity;

use App\Entity\Venue;
use PHPUnit\Framework\TestCase;


class VenueTest extends TestCase
{
    public function testNameDefault(): void
    {
    	$venue = new Venue();
        $this->assertEquals('', $venue->getName());
    }

    public function testLocationDefault(): void
    {
    	$venue = new Venue();
        $this->assertEquals(, $venue->getLocation());
    }

}
